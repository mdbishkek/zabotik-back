FROM node:18.13.0-alpine AS build

WORKDIR /app

COPY package*.json ./

RUN npm install

COPY . .

RUN ["npm", "run", "build"]

ENV PORT=8000

EXPOSE 8000

CMD ["node", "dist/index.js"]