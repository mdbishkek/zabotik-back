"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.AllergiesController = void 0;
const express_1 = __importDefault(require("express"));
const ERoles_1 = require("../enums/ERoles");
const permission_1 = require("../middleware/permission");
const allergiesDb_1 = require("../repository/subDb/allergiesDb");
class AllergiesController {
    constructor() {
        this.getRouter = () => {
            return this.router;
        };
        this.getAllergies = async (expressReq, res) => {
            const req = expressReq;
            const user = req.dataFromToken;
            const response = await this.repository.getAllergies(user.id, req.params.id);
            res.status(response.status).send(response.result);
        };
        this.createAllergy = async (expressReq, res) => {
            const req = expressReq;
            const user = req.dataFromToken;
            const response = await this.repository.createAllergy(user.id, req.body);
            res.status(response.status).send(response.result);
        };
        this.deleteAllergy = async (expressReq, res) => {
            const req = expressReq;
            const user = req.dataFromToken;
            const response = await this.repository.deleteAllergy(user.id, req.params.id);
            res.status(response.status).send(response.result);
        };
        this.repository = allergiesDb_1.allergiesDb;
        this.router = express_1.default.Router();
        this.router.get("/:id", (0, permission_1.permission)([ERoles_1.ERoles.ADMIN, ERoles_1.ERoles.SUPERADMIN, ERoles_1.ERoles.DOCTOR, ERoles_1.ERoles.PARENT]), this.getAllergies);
        this.router.post("/", (0, permission_1.permission)([ERoles_1.ERoles.DOCTOR, ERoles_1.ERoles.SUPERADMIN, ERoles_1.ERoles.PARENT]), this.createAllergy);
        this.router.delete("/:id", (0, permission_1.permission)([ERoles_1.ERoles.DOCTOR, ERoles_1.ERoles.SUPERADMIN, ERoles_1.ERoles.PARENT]), this.deleteAllergy);
    }
}
exports.AllergiesController = AllergiesController;
