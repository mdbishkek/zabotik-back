"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ChatMessagesController = void 0;
const express_1 = __importDefault(require("express"));
const multer_1 = __importDefault(require("multer"));
const index_config_1 = require("../index.config");
const shortid_1 = __importDefault(require("shortid"));
const chatMessagesDb_1 = require("../repository/subDb/chatMessagesDb");
const morganMiddleware_1 = __importDefault(require("../config/morganMiddleware"));
const permission_1 = require("../middleware/permission");
const ERoles_1 = require("../enums/ERoles");
const storage = multer_1.default.diskStorage({
    destination(req, file, callback) {
        callback(null, index_config_1.config.messagesFiles);
    },
    filename(req, file, callback) {
        callback(null, (0, shortid_1.default)());
    },
});
const upload = (0, multer_1.default)({ storage });
class ChatMessagesController {
    constructor() {
        this.getRouter = () => {
            return this.router;
        };
        this.getMessagesByQuestion = async (expressReq, res) => {
            const req = expressReq;
            const user = req.dataFromToken;
            const response = await this.repository.getMessagesByQuestion(user.id, req.params.id);
            res.status(response.status).send(response.result);
        };
        this.createMessage = async (expressReq, res) => {
            const req = expressReq;
            const user = req.dataFromToken;
            const message = req.body;
            message.url = req.file ? req.file.filename : "";
            const response = await this.repository.createMessage(user.id, message);
            res.status(response.status).send(response.result);
        };
        this.deleteMessage = async (expressReq, res) => {
            const req = expressReq;
            const user = req.dataFromToken;
            const response = await this.repository.deleteMessage(user.id, req.params.id);
            res.status(response.status).send(response.result);
        };
        this.router = express_1.default.Router();
        this.router.use(morganMiddleware_1.default);
        this.router.get("/:id", (0, permission_1.permission)([ERoles_1.ERoles.ADMIN, ERoles_1.ERoles.SUPERADMIN, ERoles_1.ERoles.DOCTOR, ERoles_1.ERoles.PARENT]), this.getMessagesByQuestion);
        this.router.post("/", [(0, permission_1.permission)([ERoles_1.ERoles.DOCTOR, ERoles_1.ERoles.PARENT]), upload.single("url")], this.createMessage);
        this.router.delete("/:id", (0, permission_1.permission)([ERoles_1.ERoles.DOCTOR, ERoles_1.ERoles.PARENT]), this.deleteMessage);
        this.repository = chatMessagesDb_1.chatMessagesDb;
    }
}
exports.ChatMessagesController = ChatMessagesController;
