"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.childrenController = void 0;
const express_1 = __importDefault(require("express"));
const morganMiddleware_1 = __importDefault(require("../config/morganMiddleware"));
const permission_1 = require("../middleware/permission");
const childrenDb_1 = require("../repository/subDb/childrenDb");
const ERoles_1 = require("../enums/ERoles");
const multer_1 = __importDefault(require("multer"));
const shortid_1 = __importDefault(require("shortid"));
const index_config_1 = require("../index.config");
const storage = multer_1.default.diskStorage({
    destination(req, file, callback) {
        callback(null, index_config_1.config.childrenImgs);
    },
    filename(req, file, callback) {
        callback(null, (0, shortid_1.default)());
    },
});
const upload = (0, multer_1.default)({ storage });
class childrenController {
    constructor() {
        this.getRouter = () => {
            return this.router;
        };
        this.getChildrenByParentId = async (expressReq, res) => {
            const req = expressReq;
            const user = req.dataFromToken;
            const response = await this.repository.getChildrenByParentId(req.params.id, user.id);
            res.status(response.status).send(response.result);
        };
        this.getChildrenByDoctorId = async (expressReq, res) => {
            const req = expressReq;
            const user = req.dataFromToken;
            const response = await this.repository.getChildrenByDoctorId(user.id, String(req.query.offset), String(req.query.limit), req.params.id);
            res.status(response.status).send(response.result);
        };
        this.getChildrenForDoctor = async (expressReq, res) => {
            const req = expressReq;
            const user = req.dataFromToken;
            const response = await this.repository.getChildrenForDoctor(req.params.id, user.id);
            res.status(response.status).send(response.result);
        };
        this.getChildById = async (expressReq, res) => {
            const req = expressReq;
            const user = req.dataFromToken;
            const response = await this.repository.getChildById(req.params.id, user.id);
            res.status(response.status).send(response.result);
        };
        this.createChild = async (expressReq, res) => {
            const req = expressReq;
            const user = req.dataFromToken;
            const child = req.body;
            child.photo = req.file ? req.file.filename : "";
            const response = await this.repository.createChild(child, user.id);
            res.status(response.status).send(response.result);
        };
        this.editChild = async (expressReq, res) => {
            const req = expressReq;
            const user = req.dataFromToken;
            const child = req.body;
            if (req.file && req.file.filename) {
                child.photo = req.file.filename;
            }
            const response = await this.repository.editChildById(req.params.id, child, user.id);
            res.status(response.status).send(response.result);
        };
        this.router = express_1.default.Router();
        this.router.use(morganMiddleware_1.default);
        this.router.get("/parent/:id", (0, permission_1.permission)([ERoles_1.ERoles.ADMIN, ERoles_1.ERoles.DOCTOR, ERoles_1.ERoles.PARENT, ERoles_1.ERoles.SUPERADMIN]), this.getChildrenByParentId);
        this.router.get("/doctor/:id", (0, permission_1.permission)([ERoles_1.ERoles.ADMIN, ERoles_1.ERoles.SUPERADMIN, ERoles_1.ERoles.DOCTOR,]), this.getChildrenByDoctorId);
        this.router.get("/for-doctor/:id", (0, permission_1.permission)([ERoles_1.ERoles.DOCTOR]), this.getChildrenForDoctor);
        this.router.post("/", [(0, permission_1.permission)([ERoles_1.ERoles.ADMIN, ERoles_1.ERoles.DOCTOR, ERoles_1.ERoles.PARENT, ERoles_1.ERoles.SUPERADMIN]), upload.single("photo")], this.createChild);
        this.router.get("/:id", (0, permission_1.permission)([ERoles_1.ERoles.ADMIN, ERoles_1.ERoles.DOCTOR, ERoles_1.ERoles.PARENT, ERoles_1.ERoles.SUPERADMIN]), this.getChildById);
        this.router.patch("/:id", [(0, permission_1.permission)([ERoles_1.ERoles.ADMIN, ERoles_1.ERoles.DOCTOR, ERoles_1.ERoles.PARENT, ERoles_1.ERoles.SUPERADMIN]), upload.single("photo")], this.editChild);
        this.repository = childrenDb_1.childrenDb;
    }
}
exports.childrenController = childrenController;
