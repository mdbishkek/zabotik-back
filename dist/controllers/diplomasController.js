"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.DiplomasController = void 0;
const express_1 = __importDefault(require("express"));
const permission_1 = require("../middleware/permission");
const ERoles_1 = require("../enums/ERoles");
const index_config_1 = require("../index.config");
const multer_1 = __importDefault(require("multer"));
const shortid_1 = __importDefault(require("shortid"));
const diplomasDb_1 = require("../repository/subDb/diplomasDb");
const storage = multer_1.default.diskStorage({
    destination(req, file, callback) {
        callback(null, index_config_1.config.doctorsDiplomas);
    },
    filename(req, file, callback) {
        callback(null, (0, shortid_1.default)());
    },
});
const upload = (0, multer_1.default)({ storage });
class DiplomasController {
    constructor() {
        this.getRouter = () => {
            return this.router;
        };
        this.getDiplomasByDoctor = async (expressReq, res) => {
            const req = expressReq;
            const user = req.dataFromToken;
            const response = await this.repository.getDiplomasByDoctor(user.id, req.params.id);
            res.status(response.status).send(response.result);
        };
        this.createDiploma = async (expressReq, res) => {
            const req = expressReq;
            const user = req.dataFromToken;
            const diploma = req.body;
            diploma.url = req.file ? req.file.filename : "";
            const response = await this.repository.createDiploma(user.id, diploma);
            res.status(response.status).send(response.result);
        };
        this.deleteDiploma = async (expressReq, res) => {
            const req = expressReq;
            const user = req.dataFromToken;
            const response = await this.repository.deleteDiploma(user.id, req.params.id);
            res.status(response.status).send(response.result);
        };
        this.repository = diplomasDb_1.diplomasDb;
        this.router = express_1.default.Router();
        this.router.get("/:id", (0, permission_1.permission)([ERoles_1.ERoles.DOCTOR, ERoles_1.ERoles.ADMIN, ERoles_1.ERoles.PARENT, ERoles_1.ERoles.SUPERADMIN]), this.getDiplomasByDoctor);
        this.router.post("/", [(0, permission_1.permission)([ERoles_1.ERoles.DOCTOR, ERoles_1.ERoles.ADMIN, ERoles_1.ERoles.SUPERADMIN]), upload.single("url")], this.createDiploma);
        this.router.delete("/:id", (0, permission_1.permission)([ERoles_1.ERoles.DOCTOR, ERoles_1.ERoles.ADMIN, ERoles_1.ERoles.SUPERADMIN]), this.deleteDiploma);
    }
}
exports.DiplomasController = DiplomasController;
