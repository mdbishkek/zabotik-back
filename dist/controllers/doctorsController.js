"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.DoctorsController = void 0;
const express_1 = __importDefault(require("express"));
const multer_1 = __importDefault(require("multer"));
const shortid_1 = __importDefault(require("shortid"));
const index_config_1 = require("../index.config");
const morganMiddleware_1 = __importDefault(require("../config/morganMiddleware"));
const permission_1 = require("../middleware/permission");
const ERoles_1 = require("../enums/ERoles");
const doctorsDb_1 = require("../repository/subDb/doctorsDb");
const storage = multer_1.default.diskStorage({
    destination(req, file, callback) {
        callback(null, index_config_1.config.doctorsImgs);
    },
    filename(req, file, callback) {
        callback(null, (0, shortid_1.default)());
    },
});
const upload = (0, multer_1.default)({ storage });
class DoctorsController {
    constructor() {
        this.getRouter = () => {
            return this.router;
        };
        this.getDoctors = async (expressReq, res) => {
            const req = expressReq;
            const user = req.dataFromToken;
            const response = await this.repository.getDoctors(user.id, req.query.offset ? String(req.query.offset) : undefined, req.query.limit ? String(req.query.limit) : undefined);
            res.status(response.status).send(response.result);
        };
        this.getDoctorByUserId = async (expressReq, res) => {
            const req = expressReq;
            const user = req.dataFromToken;
            const response = await this.repository.getDoctorByUserId(user.id, req.params.id);
            res.status(response.status).send(response.result);
        };
        this.getDoctorById = async (expressReq, res) => {
            const req = expressReq;
            const user = req.dataFromToken;
            const response = await this.repository.getDoctorById(user.id, req.params.id);
            res.status(response.status).send(response.result);
        };
        this.editDoctor = async (expressReq, res) => {
            const req = expressReq;
            const user = req.dataFromToken;
            const doctor = req.body;
            if (req.file && req.file.filename) {
                doctor.photo = req.file.filename;
            }
            const response = await this.repository.editDoctor(user.id, req.params.id, doctor);
            res.status(response.status).send(response);
        };
        this.activateDoctor = async (expressReq, res) => {
            const req = expressReq;
            const user = req.dataFromToken;
            const response = await this.repository.activateDoctor(user.id, req.params.id);
            res.status(response.status).send(response.result);
        };
        this.changeDoctorPrice = async (expressReq, res) => {
            const req = expressReq;
            const user = req.dataFromToken;
            const response = await this.repository.changeDoctorPrice(user.id, req.params.id, req.body);
            res.status(response.status).send(response.result);
        };
        this.router = express_1.default.Router();
        this.router.use(morganMiddleware_1.default);
        this.router.get("/", (0, permission_1.permission)([ERoles_1.ERoles.ADMIN, ERoles_1.ERoles.SUPERADMIN, ERoles_1.ERoles.DOCTOR]), this.getDoctors);
        this.router.get("/userid/:id", (0, permission_1.permission)([ERoles_1.ERoles.ADMIN, ERoles_1.ERoles.SUPERADMIN, ERoles_1.ERoles.DOCTOR, ERoles_1.ERoles.PARENT]), this.getDoctorByUserId);
        this.router.get("/:id", (0, permission_1.permission)([ERoles_1.ERoles.ADMIN, ERoles_1.ERoles.SUPERADMIN, ERoles_1.ERoles.DOCTOR]), this.getDoctorById);
        this.router.put("/:id", [(0, permission_1.permission)([ERoles_1.ERoles.ADMIN, ERoles_1.ERoles.SUPERADMIN, ERoles_1.ERoles.DOCTOR]), upload.single("photo")], this.editDoctor);
        this.router.patch("/:id", (0, permission_1.permission)([ERoles_1.ERoles.ADMIN, ERoles_1.ERoles.SUPERADMIN]), this.activateDoctor);
        this.router.patch("/price/:id", (0, permission_1.permission)([ERoles_1.ERoles.ADMIN, ERoles_1.ERoles.SUPERADMIN]), this.changeDoctorPrice);
        this.repository = doctorsDb_1.doctorsDb;
    }
}
exports.DoctorsController = DoctorsController;
