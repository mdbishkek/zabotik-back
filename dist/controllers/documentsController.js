"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.DocumentsController = void 0;
const express_1 = __importDefault(require("express"));
const multer_1 = __importDefault(require("multer"));
const index_config_1 = require("../index.config");
const shortid_1 = __importDefault(require("shortid"));
const documentsDb_1 = require("../repository/subDb/documentsDb");
const permission_1 = require("../middleware/permission");
const ERoles_1 = require("../enums/ERoles");
const storage = multer_1.default.diskStorage({
    destination(req, file, callback) {
        callback(null, index_config_1.config.childrenDocuments);
    },
    filename(req, file, callback) {
        callback(null, (0, shortid_1.default)());
    },
});
const upload = (0, multer_1.default)({ storage });
class DocumentsController {
    constructor() {
        this.getRouter = () => {
            return this.router;
        };
        this.createDocument = async (expressReq, res) => {
            const req = expressReq;
            const user = req.dataFromToken;
            const document = req.body;
            document.url = req.file ? req.file.filename : "";
            const response = await this.repository.createDocument(user.id, document);
            res.status(response.status).send(response.result);
        };
        this.deleteDocument = async (expressReq, res) => {
            const req = expressReq;
            const user = req.dataFromToken;
            const response = await this.repository.deleteDocument(user.id, req.params.id);
            res.status(response.status).send(response.result);
        };
        this.getDocumentsByChildId = async (expressReq, res) => {
            const req = expressReq;
            const user = req.dataFromToken;
            const response = await this.repository.getDocumentsByChildId(user.id, req.params.id);
            res.status(response.status).send(response.result);
        };
        this.repository = documentsDb_1.documentsDb;
        this.router = express_1.default.Router();
        this.router.post("/", [(0, permission_1.permission)([ERoles_1.ERoles.DOCTOR, ERoles_1.ERoles.ADMIN, ERoles_1.ERoles.SUPERADMIN, ERoles_1.ERoles.PARENT]), upload.single("url")], this.createDocument);
        this.router.delete("/:id", (0, permission_1.permission)([ERoles_1.ERoles.DOCTOR, ERoles_1.ERoles.ADMIN, ERoles_1.ERoles.PARENT, ERoles_1.ERoles.SUPERADMIN]), this.deleteDocument);
        this.router.get("/:id", (0, permission_1.permission)([ERoles_1.ERoles.DOCTOR, ERoles_1.ERoles.SUPERADMIN, ERoles_1.ERoles.ADMIN, ERoles_1.ERoles.PARENT]), this.getDocumentsByChildId);
    }
}
exports.DocumentsController = DocumentsController;
