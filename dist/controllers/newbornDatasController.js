"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.NewbornDatasController = void 0;
const express_1 = __importDefault(require("express"));
const newbornDatasDb_1 = require("../repository/subDb/newbornDatasDb");
const permission_1 = require("../middleware/permission");
const ERoles_1 = require("../enums/ERoles");
class NewbornDatasController {
    constructor() {
        this.getRouter = () => {
            return this.router;
        };
        this.getNewbornDataByChildId = async (expressReq, res) => {
            const req = expressReq;
            const user = req.dataFromToken;
            const response = await this.repository.getNewbornDataByChildId(user.id, req.params.id);
            res.status(response.status).send(response.result);
        };
        this.updateNewbornDataByChildId = async (expressReq, res) => {
            const req = expressReq;
            const user = req.dataFromToken;
            const response = await this.repository.updateNewbornDataByChildId(user.id, req.params.id, req.body);
            res.status(response.status).send(response);
        };
        this.repository = newbornDatasDb_1.newbornDatasDb;
        this.router = express_1.default.Router();
        this.router.get("/:id", (0, permission_1.permission)([ERoles_1.ERoles.DOCTOR, ERoles_1.ERoles.SUPERADMIN, ERoles_1.ERoles.ADMIN, ERoles_1.ERoles.PARENT]), this.getNewbornDataByChildId);
        this.router.put("/:id", (0, permission_1.permission)([ERoles_1.ERoles.SUPERADMIN, ERoles_1.ERoles.ADMIN, ERoles_1.ERoles.DOCTOR]), this.updateNewbornDataByChildId);
    }
}
exports.NewbornDatasController = NewbornDatasController;
