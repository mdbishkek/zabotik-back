"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ParentsController = void 0;
const express_1 = __importDefault(require("express"));
const morganMiddleware_1 = __importDefault(require("../config/morganMiddleware"));
const permission_1 = require("../middleware/permission");
const ERoles_1 = require("../enums/ERoles");
const parentsDb_1 = require("../repository/subDb/parentsDb");
class ParentsController {
    constructor() {
        this.getRouter = () => {
            return this.router;
        };
        this.getParentsByDoctorId = async (expressReq, res) => {
            const req = expressReq;
            const user = req.dataFromToken;
            const response = await this.repository.getParentsByDoctorId(user.id, String(req.query.offset), String(req.query.limit), req.params.id);
            res.status(response.status).send(response.result);
        };
        this.getParentByUserId = async (expressReq, res) => {
            const req = expressReq;
            const user = req.dataFromToken;
            const response = await this.repository.getParentByUserId(user.id, req.params.id);
            res.status(response.status).send(response.result);
        };
        this.activateParent = async (expressReq, res) => {
            const req = expressReq;
            const user = req.dataFromToken;
            const response = await this.repository.activateParent(user.id, req.params.id);
            res.status(response.status).send(response.result);
        };
        this.router = express_1.default.Router();
        this.router.use(morganMiddleware_1.default);
        this.router.get("/doctor/:id", (0, permission_1.permission)([ERoles_1.ERoles.ADMIN, ERoles_1.ERoles.SUPERADMIN, ERoles_1.ERoles.DOCTOR]), this.getParentsByDoctorId);
        this.router.get("/:id", (0, permission_1.permission)([ERoles_1.ERoles.ADMIN, ERoles_1.ERoles.SUPERADMIN, ERoles_1.ERoles.DOCTOR, ERoles_1.ERoles.PARENT]), this.getParentByUserId);
        this.router.patch("/:id", (0, permission_1.permission)([ERoles_1.ERoles.ADMIN, ERoles_1.ERoles.SUPERADMIN, ERoles_1.ERoles.PARENT]), this.activateParent);
        this.repository = parentsDb_1.parentsDb;
    }
}
exports.ParentsController = ParentsController;
