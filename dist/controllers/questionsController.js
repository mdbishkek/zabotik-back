"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.QuestionsController = void 0;
const express_1 = __importDefault(require("express"));
const permission_1 = require("../middleware/permission");
const ERoles_1 = require("../enums/ERoles");
const questionsDb_1 = require("../repository/subDb/questionsDb");
class QuestionsController {
    constructor() {
        this.getRouter = () => {
            return this.router;
        };
        this.getQuestionsByChildId = async (expressReq, res) => {
            const req = expressReq;
            const user = req.dataFromToken;
            const response = await this.repository.getQuestionsByChildId(user.id, req.params.id);
            res.status(response.status).send(response.result);
        };
        this.getQuestionsByDoctorId = async (expressReq, res) => {
            const req = expressReq;
            const user = req.dataFromToken;
            const response = await this.repository.getQuestionsByDoctorId(user.id, req.params.id);
            res.status(response.status).send(response.result);
        };
        this.createQuestion = async (expressReq, res) => {
            const req = expressReq;
            const user = req.dataFromToken;
            const response = await this.repository.createQuestion(user.id, req.body);
            res.status(response.status).send(response.result);
        };
        this.repository = questionsDb_1.questionsDb;
        this.router = express_1.default.Router();
        this.router.get("/child/:id", (0, permission_1.permission)([ERoles_1.ERoles.PARENT, ERoles_1.ERoles.DOCTOR, ERoles_1.ERoles.ADMIN, ERoles_1.ERoles.SUPERADMIN]), this.getQuestionsByChildId);
        this.router.get("/doctor/:id", (0, permission_1.permission)([ERoles_1.ERoles.DOCTOR, ERoles_1.ERoles.ADMIN, ERoles_1.ERoles.SUPERADMIN]), this.getQuestionsByDoctorId);
        this.router.post("/", (0, permission_1.permission)([ERoles_1.ERoles.PARENT]), this.createQuestion);
    }
}
exports.QuestionsController = QuestionsController;
