"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.RecommendationsController = void 0;
const express_1 = __importDefault(require("express"));
const permission_1 = require("../middleware/permission");
const ERoles_1 = require("../enums/ERoles");
const index_config_1 = require("../index.config");
const multer_1 = __importDefault(require("multer"));
const shortid_1 = __importDefault(require("shortid"));
const recommendationsDb_1 = require("../repository/subDb/recommendationsDb");
const storage = multer_1.default.diskStorage({
    destination(req, file, callback) {
        callback(null, index_config_1.config.docRecommends);
    },
    filename(req, file, callback) {
        callback(null, (0, shortid_1.default)());
    },
});
const upload = (0, multer_1.default)({ storage });
class RecommendationsController {
    constructor() {
        this.getRouter = () => {
            return this.router;
        };
        this.getRecommendationsByDoctorId = async (expressReq, res) => {
            const req = expressReq;
            const response = await this.repository.getRecommendationsByDoctor(req.params.id);
            res.status(response.status).send(response.result);
        };
        this.createRecommendation = async (expressReq, res) => {
            const req = expressReq;
            const user = req.dataFromToken;
            const recommendation = req.body;
            recommendation.url = req.file ? req.file.filename : "";
            const response = await this.repository.createRecommendation(user.id, recommendation);
            res.status(response.status).send(response.result);
        };
        this.deleteRecommendation = async (expressReq, res) => {
            const req = expressReq;
            const user = req.dataFromToken;
            const response = await this.repository.deleteRecommendation(user.id, req.params.id);
            res.status(response.status).send(response.result);
        };
        this.editRecommendation = async (expressReq, res) => {
            const req = expressReq;
            const user = req.dataFromToken;
            const upgradedRecommendation = req.body;
            if (req.file && req.file.filename) {
                upgradedRecommendation.url = req.file.filename;
            }
            const response = await this.repository.editRecommendation(user.id, req.params.id, upgradedRecommendation);
            res.status(response.status).send(response.result);
        };
        this.repository = recommendationsDb_1.recommendationDb;
        this.router = express_1.default.Router();
        this.router.get("/:id", (0, permission_1.permission)(), this.getRecommendationsByDoctorId);
        this.router.post("/", [(0, permission_1.permission)([ERoles_1.ERoles.DOCTOR]), upload.single("url")], this.createRecommendation);
        this.router.delete("/:id", (0, permission_1.permission)([ERoles_1.ERoles.DOCTOR, ERoles_1.ERoles.ADMIN, ERoles_1.ERoles.SUPERADMIN]), this.deleteRecommendation);
        this.router.put("/:id", [(0, permission_1.permission)([ERoles_1.ERoles.DOCTOR, ERoles_1.ERoles.ADMIN, ERoles_1.ERoles.SUPERADMIN]), upload.single("url")], this.editRecommendation);
    }
}
exports.RecommendationsController = RecommendationsController;
