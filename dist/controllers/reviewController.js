"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ReviewController = void 0;
const express_1 = __importDefault(require("express"));
const reviewsDb_1 = require("../repository/subDb/reviewsDb");
const permission_1 = require("../middleware/permission");
const ERoles_1 = require("../enums/ERoles");
class ReviewController {
    constructor() {
        this.getRouter = () => {
            return this.router;
        };
        this.getReviews = async (expressReq, res) => {
            const req = expressReq;
            const user = req.dataFromToken;
            const response = await this.repository.getReviews(user.id, String(req.query.offset), String(req.query.limit));
            res.status(response.status).send(response.result);
        };
        this.createReview = async (expressReq, res) => {
            const req = expressReq;
            const user = req.dataFromToken;
            const response = await this.repository.createReview(user.id, req.body);
            res.status(response.status).send(response.result);
        };
        this.deleteReview = async (expressReq, res) => {
            const req = expressReq;
            const user = req.dataFromToken;
            const response = await this.repository.deleteReview(user.id, req.params.id);
            res.status(response.status).send(response.result);
        };
        this.repository = reviewsDb_1.reviewsDb;
        this.router = express_1.default.Router();
        this.router.get("/", (0, permission_1.permission)([ERoles_1.ERoles.ADMIN, ERoles_1.ERoles.SUPERADMIN]), this.getReviews);
        this.router.post("/", (0, permission_1.permission)([ERoles_1.ERoles.PARENT]), this.createReview);
        this.router.delete("/:id", (0, permission_1.permission)([ERoles_1.ERoles.SUPERADMIN]), this.deleteReview);
    }
}
exports.ReviewController = ReviewController;
