"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.SpecExamsController = void 0;
const express_1 = __importDefault(require("express"));
const ERoles_1 = require("../enums/ERoles");
const permission_1 = require("../middleware/permission");
const specialistExamsDb_1 = require("../repository/subDb/specialistExamsDb");
class SpecExamsController {
    constructor() {
        this.getRouter = () => {
            return this.router;
        };
        this.getSpecialistExamsByChildId = async (expressReq, res) => {
            const req = expressReq;
            const user = req.dataFromToken;
            const response = await this.repository.getSpecialistExamsByChildId(user.id, req.params.id);
            res.status(response.status).send(response.result);
        };
        this.createSpecialistExam = async (expressReq, res) => {
            const req = expressReq;
            const user = req.dataFromToken;
            const response = await this.repository.createSpecialistExam(user.id, req.body);
            res.status(response.status).send(response.result);
        };
        this.deleteSpecialistExam = async (expressReq, res) => {
            const req = expressReq;
            const user = req.dataFromToken;
            const response = await this.repository.deleteSpecialistExam(user.id, req.params.id);
            res.status(response.status).send(response.result);
        };
        this.repository = specialistExamsDb_1.specialistExamsDb;
        this.router = express_1.default.Router();
        this.router.get("/:id", (0, permission_1.permission)([ERoles_1.ERoles.SUPERADMIN, ERoles_1.ERoles.ADMIN, ERoles_1.ERoles.DOCTOR, ERoles_1.ERoles.PARENT]), this.getSpecialistExamsByChildId);
        this.router.post("/", (0, permission_1.permission)([ERoles_1.ERoles.DOCTOR, ERoles_1.ERoles.PARENT]), this.createSpecialistExam);
        this.router.delete("/:id", (0, permission_1.permission)([ERoles_1.ERoles.DOCTOR, ERoles_1.ERoles.PARENT]), this.deleteSpecialistExam);
    }
}
exports.SpecExamsController = SpecExamsController;
