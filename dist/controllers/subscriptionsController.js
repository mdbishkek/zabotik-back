"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.SubscriptionsController = void 0;
const subscriptionsDb_1 = require("../repository/subDb/subscriptionsDb");
const express_1 = __importDefault(require("express"));
const permission_1 = require("../middleware/permission");
const ERoles_1 = require("../enums/ERoles");
class SubscriptionsController {
    constructor() {
        this.getRouter = () => {
            return this.router;
        };
        this.renewSubscription = async (expressReq, res) => {
            const req = expressReq;
            const user = req.dataFromToken;
            const response = await this.repository.renewSubscription(user.id, req.body.sub);
            res.status(response.status).send(response.result);
        };
        this.repository = subscriptionsDb_1.subscriptionsDb;
        this.router = express_1.default.Router();
        this.router.post("/", (0, permission_1.permission)([ERoles_1.ERoles.DOCTOR, ERoles_1.ERoles.PARENT]), this.renewSubscription);
    }
}
exports.SubscriptionsController = SubscriptionsController;
