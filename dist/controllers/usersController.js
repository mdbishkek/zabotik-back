"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.UsersController = void 0;
const express_1 = __importDefault(require("express"));
const morganMiddleware_1 = __importDefault(require("../config/morganMiddleware"));
const permission_1 = require("../middleware/permission");
const ERoles_1 = require("../enums/ERoles");
const usersDb_1 = require("../repository/subDb/usersDb");
class UsersController {
    constructor() {
        this.getRouter = () => {
            return this.router;
        };
        this.getUsers = async (expressReq, res) => {
            const req = expressReq;
            const user = req.dataFromToken;
            const response = await this.repository.getUsers(user.id, String(req.query.offset), String(req.query.limit), String(req.query.filter));
            res.status(response.status).send(response.result);
        };
        this.register = async (expressReq, res) => {
            const req = expressReq;
            const user = req.dataFromToken;
            const response = await this.repository.register(user.id, req.body);
            res.status(response.status).send(response.result);
        };
        this.registerParent = async (expressReq, res) => {
            const req = expressReq;
            const user = req.dataFromToken;
            const response = await this.repository.registerParent(req.body, user.id);
            res.status(response.status).send(response.result);
        };
        this.login = async (req, res) => {
            const response = await this.repository.login(req.body);
            res.status(response.status).send(response.result);
        };
        this.editUser = async (expressReq, res) => {
            const req = expressReq;
            const user = req.dataFromToken;
            const response = await this.repository.editUser(user.id, req.params.id, req.body);
            res.status(response.status).send(response.result);
        };
        this.setPassword = async (req, res) => {
            const response = await this.repository.setPassword(req.body);
            res.status(response.status).send(response.result);
        };
        this.blockUser = async (expressReq, res) => {
            const req = expressReq;
            const user = req.dataFromToken;
            const response = await this.repository.blockUser(user.id, req.params.id);
            res.status(response.status).send(response.result);
        };
        this.checkToken = async (expressReq, res) => {
            const req = expressReq;
            const user = req.dataFromToken;
            const response = await this.repository.checkToken(user.id);
            res.status(response.status).send(response.result);
        };
        this.router = express_1.default.Router();
        this.router.use(morganMiddleware_1.default);
        this.router.post("/", (0, permission_1.permission)([ERoles_1.ERoles.ADMIN, ERoles_1.ERoles.SUPERADMIN]), this.register);
        this.router.post("/parent", (0, permission_1.permission)([ERoles_1.ERoles.ADMIN, ERoles_1.ERoles.DOCTOR, ERoles_1.ERoles.SUPERADMIN]), this.registerParent);
        this.router.post("/login", this.login);
        this.router.get("/", (0, permission_1.permission)([ERoles_1.ERoles.ADMIN, ERoles_1.ERoles.SUPERADMIN]), this.getUsers);
        this.router.patch("/:id", (0, permission_1.permission)([ERoles_1.ERoles.SUPERADMIN, ERoles_1.ERoles.ADMIN, ERoles_1.ERoles.DOCTOR, ERoles_1.ERoles.PARENT]), this.editUser);
        this.router.post("/set-password", this.setPassword);
        this.router.patch("/block/:id", (0, permission_1.permission)([ERoles_1.ERoles.ADMIN, ERoles_1.ERoles.SUPERADMIN]), this.blockUser);
        this.router.get("/token", (0, permission_1.permission)([ERoles_1.ERoles.SUPERADMIN, ERoles_1.ERoles.ADMIN, ERoles_1.ERoles.DOCTOR, ERoles_1.ERoles.PARENT]), this.checkToken);
        this.repository = usersDb_1.usersDb;
    }
}
exports.UsersController = UsersController;
