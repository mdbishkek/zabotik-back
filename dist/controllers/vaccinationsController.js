"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.VaccinationsController = void 0;
const express_1 = __importDefault(require("express"));
const ERoles_1 = require("../enums/ERoles");
const permission_1 = require("../middleware/permission");
const vaccinationsDb_1 = require("../repository/subDb/vaccinationsDb");
class VaccinationsController {
    constructor() {
        this.getRouter = () => {
            return this.router;
        };
        this.getVaccinations = async (expressReq, res) => {
            const req = expressReq;
            const user = req.dataFromToken;
            const response = await this.repository.getVaccinations(user.id, req.params.id);
            res.status(response.status).send(response.result);
        };
        this.createVaccination = async (expressReq, res) => {
            const req = expressReq;
            const user = req.dataFromToken;
            const response = await this.repository.createVaccination(user.id, req.body);
            res.status(response.status).send(response.result);
        };
        this.deleteVaccination = async (expressReq, res) => {
            const req = expressReq;
            const user = req.dataFromToken;
            const response = await this.repository.deleteVaccination(user.id, req.params.id);
            res.status(response.status).send(response.result);
        };
        this.repository = vaccinationsDb_1.vaccinationsDb;
        this.router = express_1.default.Router();
        this.router.get("/:id", (0, permission_1.permission)([ERoles_1.ERoles.ADMIN, ERoles_1.ERoles.SUPERADMIN, ERoles_1.ERoles.DOCTOR, ERoles_1.ERoles.PARENT]), this.getVaccinations);
        this.router.post("/", (0, permission_1.permission)([ERoles_1.ERoles.DOCTOR, ERoles_1.ERoles.SUPERADMIN, ERoles_1.ERoles.PARENT]), this.createVaccination);
        this.router.delete("/:id", (0, permission_1.permission)([ERoles_1.ERoles.DOCTOR, ERoles_1.ERoles.SUPERADMIN, ERoles_1.ERoles.PARENT]), this.deleteVaccination);
    }
}
exports.VaccinationsController = VaccinationsController;
