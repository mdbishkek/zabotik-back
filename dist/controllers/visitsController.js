"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.VisitsController = void 0;
const express_1 = __importDefault(require("express"));
const permission_1 = require("../middleware/permission");
const ERoles_1 = require("../enums/ERoles");
const visitsDb_1 = require("../repository/subDb/visitsDb");
class VisitsController {
    constructor() {
        this.getRouter = () => {
            return this.router;
        };
        this.getVisitsByChildId = async (expressReq, res) => {
            const req = expressReq;
            const user = req.dataFromToken;
            const response = await this.repository.getVisitsByChildId(user.id, req.params.id);
            res.status(response.status).send(response.result);
        };
        this.createVisit = async (expressReq, res) => {
            const req = expressReq;
            const user = req.dataFromToken;
            const visit = req.body;
            const response = await this.repository.createVisit(user.id, visit);
            res.status(response.status).send(response.result);
        };
        this.deleteVisit = async (expressReq, res) => {
            const req = expressReq;
            const user = req.dataFromToken;
            const response = await this.repository.deleteVisit(user.id, req.params.id);
            res.status(response.status).send(response.result);
        };
        this.repository = visitsDb_1.visitDb;
        this.router = express_1.default.Router();
        this.router.get("/:id", (0, permission_1.permission)([ERoles_1.ERoles.DOCTOR, ERoles_1.ERoles.ADMIN, ERoles_1.ERoles.SUPERADMIN, ERoles_1.ERoles.PARENT]), this.getVisitsByChildId);
        this.router.post("/", (0, permission_1.permission)([ERoles_1.ERoles.DOCTOR]), this.createVisit);
        this.router.delete("/:id", (0, permission_1.permission)([ERoles_1.ERoles.DOCTOR]), this.deleteVisit);
    }
}
exports.VisitsController = VisitsController;
