"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.EDoctorLevel = void 0;
var EDoctorLevel;
(function (EDoctorLevel) {
    EDoctorLevel[EDoctorLevel["JUNIOR"] = 5000] = "JUNIOR";
    EDoctorLevel[EDoctorLevel["MIDLLE"] = 12000] = "MIDLLE";
    EDoctorLevel[EDoctorLevel["SENIOR"] = 21000] = "SENIOR";
})(EDoctorLevel = exports.EDoctorLevel || (exports.EDoctorLevel = {}));
