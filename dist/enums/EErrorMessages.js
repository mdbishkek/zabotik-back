"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.EErrorMessages = void 0;
var EErrorMessages;
(function (EErrorMessages) {
    EErrorMessages["NO_ACCESS"] = "\u0423 \u0412\u0430\u0441 \u043D\u0435\u0442 \u043F\u0440\u0430\u0432 \u0434\u043E\u0441\u0442\u0443\u043F\u0430.";
    EErrorMessages["NOT_AUTHORIZED"] = "\u0412\u044B \u043D\u0435 \u0438\u0434\u0435\u043D\u0442\u0438\u0444\u0438\u0446\u0438\u0440\u043E\u0432\u0430\u043D\u044B.";
    EErrorMessages["DIPLOMA_NOT_FOUND"] = "\u0414\u0438\u043F\u043B\u043E\u043C \u043D\u0435 \u043D\u0430\u0439\u0434\u0435\u043D.";
    EErrorMessages["PARENT_NOT_FOUND"] = "\u0420\u043E\u0434\u0438\u0442\u0435\u043B\u044C \u043F\u0430\u0446\u0438\u0435\u043D\u0442\u0430 \u043D\u0435 \u043D\u0430\u0439\u0434\u0435\u043D.";
    EErrorMessages["PARENT_TABLE_ALREADY_EXISTS"] = "\u0422\u0430\u0431\u043B\u0438\u0446\u0430 \u00AB\u0420\u043E\u0434\u0438\u0442\u0435\u043B\u044C\u00BB \u0434\u043B\u044F \u044D\u0442\u043E\u0433\u043E \u043F\u043E\u043B\u044C\u0437\u043E\u0432\u0430\u0442\u0435\u043B\u044F \u0443\u0436\u0435 \u0441\u043E\u0437\u0434\u0430\u043D\u0430.";
    EErrorMessages["USER_NOT_FOUND"] = "\u041F\u043E\u043B\u044C\u0437\u043E\u0432\u0430\u0442\u0435\u043B\u044C \u043D\u0435 \u043D\u0430\u0439\u0434\u0435\u043D.";
    EErrorMessages["DOCTOR_NOT_FOUND"] = "\u0412\u0440\u0430\u0447 \u043D\u0435 \u043D\u0430\u0439\u0434\u0435\u043D.";
    EErrorMessages["QUESTION_NOT_FOUND"] = "\u0412\u043E\u043F\u0440\u043E\u0441 \u043D\u0435 \u043D\u0430\u0439\u0434\u0435\u043D";
    EErrorMessages["SPECIALIST_EXAM_NOT_FOUND"] = "\u0417\u0430\u043F\u0438\u0441\u044C \u043E\u0431 \u043E\u0441\u043C\u043E\u0442\u0440\u0435 \u0432\u0440\u0430\u0447\u0430 \u043D\u0435 \u043D\u0430\u0439\u0434\u0435\u043D\u0430";
    EErrorMessages["DOCTOR_TABLE_ALREADY_EXISTS"] = "\u0422\u0430\u0431\u043B\u0438\u0446\u0430 \u0432\u0440\u0430\u0447 \u0434\u043B\u044F \u044D\u0442\u043E\u0433\u043E \u043F\u043E\u043B\u044C\u0437\u043E\u0432\u0430\u0442\u0435\u043B\u044F \u0443\u0436\u0435 \u0441\u043E\u0437\u0434\u0430\u043D\u0430.";
    EErrorMessages["WRONG_PASSWORD"] = "\u041F\u0430\u0440\u043E\u043B\u044C \u0443\u043A\u0430\u0437\u0430\u043D \u043D\u0435 \u0432\u0435\u0440\u043D\u043E!";
    EErrorMessages["NO_PASSWORD"] = "\u041F\u0430\u0440\u043E\u043B\u044C \u043D\u0435 \u0443\u043A\u0430\u0437\u0430\u043D.";
    EErrorMessages["PASSWORD_LENGTH_FAILED"] = "\u041F\u0430\u0440\u043E\u043B\u044C \u0434\u043E\u043B\u0436\u0435\u043D \u0431\u044B\u0442\u044C \u043E\u0442 6 \u0434\u043E 8 \u0441\u0438\u043C\u0432\u043E\u043B\u043E\u0432 \u0432 \u0434\u043B\u0438\u043D\u0443.";
    EErrorMessages["PASSWORD_CAPITAL_LETTER_FAILED"] = "\u041F\u0430\u0440\u043E\u043B\u044C \u0434\u043E\u043B\u0436\u0435\u043D \u0441\u043E\u0434\u0435\u0440\u0436\u0430\u0442\u044C \u043C\u0438\u043D\u0438\u043C\u0443\u043C 1 \u0437\u0430\u0433\u043B\u0430\u0432\u043D\u0443\u044E \u0431\u0443\u043A\u0432\u0443.";
    EErrorMessages["PASSWORD_SMALL_LETTER_FAILED"] = "\u041F\u0430\u0440\u043E\u043B\u044C \u0434\u043E\u043B\u0436\u0435\u043D \u0441\u043E\u0434\u0435\u0440\u0436\u0430\u0442\u044C \u043C\u0438\u043D\u0438\u043C\u0443\u043C 1 \u0441\u0442\u0440\u043E\u0447\u043D\u0443\u044E \u0431\u0443\u043A\u0432\u0443.";
    EErrorMessages["PASSWORD_NUMBER_FAILED"] = "\u041F\u0430\u0440\u043E\u043B\u044C \u0434\u043E\u043B\u0436\u0435\u043D \u0441\u043E\u0434\u0435\u0440\u0436\u0430\u0442\u044C \u043C\u0438\u043D\u0438\u043C\u0443\u043C 1 \u0446\u0438\u0444\u0440\u0443.";
    EErrorMessages["USER_NOT_FOUND_BY_ID"] = "\u041F\u043E\u043B\u044C\u0437\u043E\u0432\u0430\u0442\u0435\u043B\u044C \u0441 \u0442\u0430\u043A\u0438\u043C ID \u043D\u0435 \u043D\u0430\u0439\u0434\u0435\u043D.";
    EErrorMessages["SUPERADMIN_CANT_BE_BLOCKED"] = "\u0421\u0443\u043F\u0435\u0440\u0430\u0434\u043C\u0438\u043D\u0438\u0441\u0442\u0440\u0430\u0442\u043E\u0440 \u043D\u0435 \u043C\u043E\u0436\u0435\u0442 \u0431\u044B\u0442\u044C \u0431\u043B\u043E\u043A\u0438\u0440\u043E\u0432\u0430\u043D.";
    EErrorMessages["USER_ALREADY_EXISTS"] = "\u041F\u043E\u043B\u044C\u0437\u043E\u0432\u0430\u0442\u0435\u043B\u044C \u0441 \u0442\u0430\u043A\u0438\u043C email \u0443\u0436\u0435 \u0437\u0430\u0440\u0435\u0433\u0438\u0441\u0442\u0440\u0438\u0440\u043E\u0432\u0430\u043D.";
    EErrorMessages["WRONG_MAIL_FORMAT"] = "\u041D\u0435\u043F\u0440\u0430\u0432\u0438\u043B\u044C\u043D\u044B\u0439 \u0444\u043E\u0440\u043C\u0430\u0442 email-\u0430\u0434\u0440\u0435\u0441\u0430";
    EErrorMessages["DOCTOR_DIPLOMA_NOT_FOUND"] = "\u0412\u0440\u0430\u0447, \u0447\u044C\u0438 \u0434\u0438\u043F\u043B\u043E\u043C\u044B \u0412\u044B \u0437\u0430\u043F\u0440\u0430\u0448\u0438\u0432\u0430\u0435\u0442\u0435 \u043D\u0435 \u043D\u0430\u0439\u0434\u0435\u043D.";
    EErrorMessages["IMAGE_REQUIRED"] = "\u0418\u0437\u043E\u0431\u0440\u0430\u0436\u0435\u043D\u0438\u0435 \u043E\u0431\u044F\u0437\u0430\u0442\u0435\u043B\u044C\u043D\u043E.";
    EErrorMessages["NO_RECOMMENDATIONS_FOUND"] = "\u0420\u0435\u043A\u043E\u043C\u0435\u043D\u0434\u0430\u0446\u0438\u0438 \u043D\u0435 \u043D\u0430\u0439\u0434\u0435\u043D\u044B.";
    EErrorMessages["NO_RECOMMENDATION_FOUND"] = "\u0420\u0435\u043A\u043E\u043C\u0435\u043D\u0434\u0430\u0446\u0438\u044F \u043D\u0435 \u043D\u0430\u0439\u0434\u0435\u043D\u0430.";
    EErrorMessages["WRONG_PASS_OR_EMAIL"] = "\u041D\u0435\u0432\u0435\u0440\u043D\u043E \u0443\u043A\u0430\u0437\u0430\u043D \u0435\u043C\u0430\u0439\u043B \u0438\u043B\u0438 \u043F\u0430\u0440\u043E\u043B\u044C.";
    EErrorMessages["CHILD_NOT_FOUND"] = "\u0420\u0435\u0431\u0435\u043D\u043E\u043A \u043D\u0435 \u043D\u0430\u0439\u0434\u0435\u043D.";
    EErrorMessages["NO_REVIEW_FOUND"] = "\u041E\u0442\u0437\u044B\u0432 \u043D\u0435 \u043D\u0430\u0439\u0434\u0435\u043D.";
    EErrorMessages["DOCUMENT_NOT_FOUND"] = "\u0414\u043E\u043A\u0443\u043C\u0435\u043D\u0442 \u043D\u0435 \u043D\u0430\u0439\u0434\u0435\u043D.";
    EErrorMessages["VACCINATION_NOT_FOUND"] = "\u0417\u0430\u043F\u0438\u0441\u044C \u043E \u0432\u0430\u043A\u0446\u0438\u043D\u0430\u0446\u0438\u0438 \u043D\u0435 \u043D\u0430\u0439\u0434\u0435\u043D\u0430.";
    EErrorMessages["ALLERGY_NOT_FOUND"] = "\u0417\u0430\u043F\u0438\u0441\u044C \u043E\u0431 \u0430\u043B\u043B\u0435\u0440\u0433\u0438\u0438 \u043D\u0435 \u043D\u0430\u0439\u0434\u0435\u043D\u0430.";
    EErrorMessages["VISIT_NOT_FOUND"] = "\u0417\u0430\u043F\u0438\u0441\u044C \u043E \u043F\u043E\u0441\u0435\u0449\u0435\u043D\u0438\u0438 \u043D\u0435 \u043D\u0430\u0439\u0434\u0435\u043D\u0430.";
    EErrorMessages["VISITS_NOT_FOUND"] = "\u0417\u0430\u043F\u0438\u0441\u0438 \u043E \u043F\u043E\u0441\u0435\u0449\u0435\u043D\u0438\u044F\u0445 \u043D\u0435 \u043D\u0430\u0439\u0434\u0435\u043D\u044B.";
    EErrorMessages["MESSAGE_NOT_FOUND"] = "\u0421\u043E\u043E\u0431\u0449\u0435\u043D\u0438\u0435 \u043D\u0435 \u043D\u0430\u0439\u0434\u0435\u043D\u043E.";
    EErrorMessages["NO_SUBSCRIPTION"] = "\u041F\u043E\u0434\u043F\u0438\u0441\u043A\u0438 \u043D\u0435\u0442.";
    EErrorMessages["WRONG_PRICE"] = "\u041D\u0435\u043A\u043E\u0440\u0440\u0435\u043A\u0442\u043D\u0430\u044F \u0431\u0430\u0437\u043E\u0432\u0430\u044F \u0446\u0435\u043D\u0430.";
    EErrorMessages["NEWBORN_DATA_NOT_FOUND"] = "\u0421\u0432\u0435\u0434\u0435\u043D\u0438\u0435 \u043E \u043D\u043E\u0432\u043E\u0440\u043E\u0436\u0434\u0435\u043D\u043D\u043E\u043C \u043D\u0435 \u043D\u0430\u0439\u0434\u0435\u043D\u043E.";
    EErrorMessages["WRONG_SUB_TYPE"] = "\u041D\u0435\u043F\u0440\u0430\u0432\u0438\u043B\u044C\u043D\u044B\u0439 \u0442\u0438\u043F \u043F\u043E\u0434\u043F\u0438\u0441\u043A\u0438";
})(EErrorMessages = exports.EErrorMessages || (exports.EErrorMessages = {}));
