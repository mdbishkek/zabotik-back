"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.EPaymentType = void 0;
var EPaymentType;
(function (EPaymentType) {
    EPaymentType["AQUIR"] = "\u043E\u043D\u043B\u0430\u0439\u043D";
    EPaymentType["CASH"] = "\u043D\u0430\u043B\u0438\u0447\u043D\u044B\u0435";
})(EPaymentType = exports.EPaymentType || (exports.EPaymentType = {}));
