"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ERoles = void 0;
var ERoles;
(function (ERoles) {
    ERoles["ADMIN"] = "ADMIN";
    ERoles["DOCTOR"] = "DOCTOR";
    ERoles["PARENT"] = "PARENT";
    ERoles["SUPERADMIN"] = "SUPERADMIN";
})(ERoles = exports.ERoles || (exports.ERoles = {}));
