"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.EVisitReasons = void 0;
var EVisitReasons;
(function (EVisitReasons) {
    EVisitReasons["PROPH"] = "\u043F\u0440\u043E\u0444\u0438\u043B\u0430\u043A\u0442\u0438\u0447\u0435\u0441\u043A\u0438\u0439";
    EVisitReasons["THERAP"] = "\u043B\u0435\u0447\u0435\u0431\u043D\u044B\u0439";
})(EVisitReasons = exports.EVisitReasons || (exports.EVisitReasons = {}));
