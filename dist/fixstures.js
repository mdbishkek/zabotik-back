"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.createUserFixtures = void 0;
const ERoles_1 = require("./enums/ERoles");
const User_1 = require("./models/User");
const logger_1 = __importDefault(require("./lib/logger"));
const react_uuid_1 = __importDefault(require("react-uuid"));
const postgresDb_1 = require("./repository/postgresDb");
const db = postgresDb_1.PostgresDB;
const date = new Date();
const createUserFixtures = async () => {
    try {
        await User_1.User.create({
            id: (0, react_uuid_1.default)(),
            role: ERoles_1.ERoles.SUPERADMIN,
            email: "zabotik.help@yandex.ru",
            phone: "+7(707)177-48-74",
            name: "Алина",
            surname: "Ардамина",
            password: "$2b$08$oG.sWyvFWciU9P8gnjQz3.HhNt5V6v4yoLXaDlSO1a75hKtfXf61i",
            isBlocked: false,
        });
        logger_1.default.info("Фикстуры созданы");
    }
    catch (error) {
        logger_1.default.error(error);
    }
};
exports.createUserFixtures = createUserFixtures;
(0, exports.createUserFixtures)();
