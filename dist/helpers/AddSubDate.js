"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.addMonthesToUTCDate = void 0;
const addMonthesToUTCDate = (utcDate, month) => {
    const dateObject = new Date(utcDate);
    const currentMonth = dateObject.getUTCMonth();
    dateObject.setUTCMonth(currentMonth + month);
    return dateObject;
};
exports.addMonthesToUTCDate = addMonthesToUTCDate;
