"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.deleteFile = void 0;
const fs_1 = __importDefault(require("fs"));
const path_1 = __importDefault(require("path"));
const logger_1 = __importDefault(require("../lib/logger"));
const deleteFile = (fileName, filePath) => {
    const imagePath = path_1.default.resolve("./") + `/public/uploads/${filePath}/`;
    fs_1.default.unlink(imagePath + fileName, (err) => {
        if (err) {
            throw err;
        }
        logger_1.default.info(`Image ${fileName} deleted`);
    });
};
exports.deleteFile = deleteFile;
