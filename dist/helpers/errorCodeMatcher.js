"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.errorCodesMathcher = void 0;
const http_status_codes_1 = require("http-status-codes");
const EErrorMessages_1 = require("../enums/EErrorMessages");
exports.errorCodesMathcher = {
    [EErrorMessages_1.EErrorMessages.DIPLOMA_NOT_FOUND]: http_status_codes_1.StatusCodes.NOT_FOUND,
    [EErrorMessages_1.EErrorMessages.DOCTOR_NOT_FOUND]: http_status_codes_1.StatusCodes.NOT_FOUND,
    [EErrorMessages_1.EErrorMessages.PARENT_NOT_FOUND]: http_status_codes_1.StatusCodes.NOT_FOUND,
    [EErrorMessages_1.EErrorMessages.USER_NOT_FOUND_BY_ID]: http_status_codes_1.StatusCodes.NOT_FOUND,
    [EErrorMessages_1.EErrorMessages.USER_NOT_FOUND]: http_status_codes_1.StatusCodes.NOT_FOUND,
    [EErrorMessages_1.EErrorMessages.NO_ACCESS]: http_status_codes_1.StatusCodes.FORBIDDEN,
    [EErrorMessages_1.EErrorMessages.NOT_AUTHORIZED]: http_status_codes_1.StatusCodes.UNAUTHORIZED,
    [EErrorMessages_1.EErrorMessages.WRONG_PASSWORD]: http_status_codes_1.StatusCodes.BAD_REQUEST,
    [EErrorMessages_1.EErrorMessages.PASSWORD_LENGTH_FAILED]: http_status_codes_1.StatusCodes.BAD_REQUEST,
    [EErrorMessages_1.EErrorMessages.PASSWORD_CAPITAL_LETTER_FAILED]: http_status_codes_1.StatusCodes.BAD_REQUEST,
    [EErrorMessages_1.EErrorMessages.PASSWORD_SMALL_LETTER_FAILED]: http_status_codes_1.StatusCodes.BAD_REQUEST,
    [EErrorMessages_1.EErrorMessages.PASSWORD_NUMBER_FAILED]: http_status_codes_1.StatusCodes.BAD_REQUEST,
    [EErrorMessages_1.EErrorMessages.WRONG_MAIL_FORMAT]: http_status_codes_1.StatusCodes.BAD_REQUEST,
    [EErrorMessages_1.EErrorMessages.DOCTOR_TABLE_ALREADY_EXISTS]: http_status_codes_1.StatusCodes.BAD_REQUEST,
    [EErrorMessages_1.EErrorMessages.PARENT_TABLE_ALREADY_EXISTS]: http_status_codes_1.StatusCodes.BAD_REQUEST,
    [EErrorMessages_1.EErrorMessages.USER_ALREADY_EXISTS]: http_status_codes_1.StatusCodes.BAD_REQUEST,
    [EErrorMessages_1.EErrorMessages.SUPERADMIN_CANT_BE_BLOCKED]: http_status_codes_1.StatusCodes.BAD_REQUEST,
    [EErrorMessages_1.EErrorMessages.NO_PASSWORD]: http_status_codes_1.StatusCodes.BAD_REQUEST,
    [EErrorMessages_1.EErrorMessages.DOCTOR_DIPLOMA_NOT_FOUND]: http_status_codes_1.StatusCodes.NOT_FOUND,
    [EErrorMessages_1.EErrorMessages.IMAGE_REQUIRED]: http_status_codes_1.StatusCodes.BAD_REQUEST,
    [EErrorMessages_1.EErrorMessages.NO_RECOMMENDATIONS_FOUND]: http_status_codes_1.StatusCodes.NOT_FOUND,
    [EErrorMessages_1.EErrorMessages.WRONG_PASS_OR_EMAIL]: http_status_codes_1.StatusCodes.BAD_REQUEST,
    [EErrorMessages_1.EErrorMessages.VISIT_NOT_FOUND]: http_status_codes_1.StatusCodes.NOT_FOUND,
    [EErrorMessages_1.EErrorMessages.VISITS_NOT_FOUND]: http_status_codes_1.StatusCodes.NOT_FOUND,
    [EErrorMessages_1.EErrorMessages.WRONG_SUB_TYPE]: http_status_codes_1.StatusCodes.BAD_REQUEST
};
