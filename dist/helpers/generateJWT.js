"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.generateJWT = void 0;
const jsonwebtoken_1 = __importDefault(require("jsonwebtoken"));
const generateJWT = (payload) => {
    return jsonwebtoken_1.default.sign(payload, `${process.env.SECRET_KEY}`, { expiresIn: "30d" });
};
exports.generateJWT = generateJWT;
