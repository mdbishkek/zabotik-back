"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.config = void 0;
const path_1 = __importDefault(require("path"));
exports.config = {
    doctorsImgs: path_1.default.join(__dirname, "../public/uploads/doctorsImgs"),
    doctorsDiplomas: path_1.default.join(__dirname, "../public/uploads/doctorsDiplomas"),
    docRecommends: path_1.default.join(__dirname, "../public/uploads/docRecommends"),
    childrenImgs: path_1.default.join(__dirname, "../public/uploads/childrenImgs"),
    childrenDocuments: path_1.default.join(__dirname, "../public/uploads/childrenDocuments"),
    messagesFiles: path_1.default.join(__dirname, "../public/uploads/messagesFiles")
};
