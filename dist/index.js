"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.app = void 0;
const express_1 = __importDefault(require("express"));
const dotenv_1 = __importDefault(require("dotenv"));
const cors_1 = __importDefault(require("cors"));
const logger_1 = __importDefault(require("./lib/logger"));
const setPassword_1 = __importDefault(require("./routes/setPassword"));
const postgresDb_1 = require("./repository/postgresDb");
const usersController_1 = require("./controllers/usersController");
const doctorsController_1 = require("./controllers/doctorsController");
const parentsController_1 = require("./controllers/parentsController");
const diplomasController_1 = require("./controllers/diplomasController");
const recommendationsController_1 = require("./controllers/recommendationsController");
const childrenController_1 = require("./controllers/childrenController");
const reviewController_1 = require("./controllers/reviewController");
const documentsController_1 = require("./controllers/documentsController");
const allergiesController_1 = require("./controllers/allergiesController");
const vaccinationsController_1 = require("./controllers/vaccinationsController");
const visitsController_1 = require("./controllers/visitsController");
const questionsController_1 = require("./controllers/questionsController");
const specExamsControllers_1 = require("./controllers/specExamsControllers");
const sheduleSubscription_1 = __importDefault(require("./sheduleSubscripttion/sheduleSubscription"));
const chatMessagesController_1 = require("./controllers/chatMessagesController");
const chatMessagesStatusController_1 = require("./controllers/chatMessagesStatusController");
const subscriptionsController_1 = require("./controllers/subscriptionsController");
const newbornDatasController_1 = require("./controllers/newbornDatasController");
const helmet_1 = __importDefault(require("helmet"));
const express_rate_limit_1 = __importDefault(require("express-rate-limit"));
dotenv_1.default.config();
class App {
    constructor() {
        this.init = async () => {
            try {
                this.app.listen(process.env.APP_PORT, () => {
                    logger_1.default.info(`Server is running on port ${process.env.APP_PORT}`);
                });
                postgresDb_1.postgresDB.init();
                this.app.use("/users", new usersController_1.UsersController().getRouter());
                this.app.use("/doctors", new doctorsController_1.DoctorsController().getRouter());
                this.app.use("/parents", new parentsController_1.ParentsController().getRouter());
                this.app.use("/diplomas", new diplomasController_1.DiplomasController().getRouter());
                this.app.use("/recommendations", new recommendationsController_1.RecommendationsController().getRouter());
                this.app.use("/children", new childrenController_1.childrenController().getRouter());
                this.app.use("/reviews", new reviewController_1.ReviewController().getRouter());
                this.app.use("/documents", new documentsController_1.DocumentsController().getRouter());
                this.app.use("/allergies", new allergiesController_1.AllergiesController().getRouter());
                this.app.use("/vaccinations", new vaccinationsController_1.VaccinationsController().getRouter());
                this.app.use("/visits", new visitsController_1.VisitsController().getRouter());
                this.app.use("/questions", new questionsController_1.QuestionsController().getRouter());
                this.app.use("/examinations", new specExamsControllers_1.SpecExamsController().getRouter());
                this.app.use("/messages", new chatMessagesController_1.ChatMessagesController().getRouter());
                this.app.use("/messages-status", new chatMessagesStatusController_1.ChatMessagesStatusController().getRouter());
                this.app.use("/renew", new subscriptionsController_1.SubscriptionsController().getRouter());
                this.app.use("/newborn-data", new newbornDatasController_1.NewbornDatasController().getRouter());
                sheduleSubscription_1.default;
            }
            catch (err) {
                const error = err;
                logger_1.default.error(`Server error: ${error.message}`);
            }
        };
        this.app = (0, express_1.default)();
        this.app.use(express_1.default.urlencoded({ extended: true }));
        this.app.use(express_1.default.static("public"));
        this.app.use(express_1.default.json());
        this.app.use((0, cors_1.default)());
        this.app.use((0, helmet_1.default)());
        this.app.use((0, express_rate_limit_1.default)({
            windowMs: 15 * 60 * 1000,
            max: 300,
            standardHeaders: true,
            legacyHeaders: false,
            message: { message: "Много запросов повторите позже" }
        }));
        this.app.use(setPassword_1.default);
    }
}
exports.app = new App();
exports.app.init()
    .then(() => {
    logger_1.default.info("Server is OK");
})
    .catch(() => {
    logger_1.default.error("Server is NOT OK");
});
