"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const nodemailer_express_handlebars_1 = __importDefault(require("nodemailer-express-handlebars"));
const nodemailer_1 = __importDefault(require("nodemailer"));
const dotenv_1 = __importDefault(require("dotenv"));
const path_1 = __importDefault(require("path"));
const logger_1 = __importDefault(require("./logger"));
dotenv_1.default.config();
const myEmail = process.env.EMAIL;
const sendMail = async (data) => {
    const transporter = nodemailer_1.default.createTransport({
        host: process.env.MAIL_HOST,
        port: 465,
        debug: true,
        secure: true,
        logger: true,
        auth: {
            user: myEmail,
            pass: process.env.CLIENT_SECRET
        },
    });
    const mailOptions = {
        from: `'Заботик' Мед.Сервис ${myEmail}`,
        to: data.recipient,
        subject: data.theme,
        template: "email",
        context: {
            msg: data.link,
        },
    };
    const handlebarOptions = {
        viewEngine: {
            partialsDir: path_1.default.resolve("./src/views/"),
            defaultLayout: false,
        },
        viewPath: path_1.default.resolve("./src/views/"),
    };
    transporter.use("compile", (0, nodemailer_express_handlebars_1.default)(handlebarOptions));
    transporter.sendMail(mailOptions, async (error, info) => {
        if (error) {
            logger_1.default.info("Сообщение не отправлено: " + error.message);
        }
        else {
            logger_1.default.info("Сообщение отправлено: " + info.response);
        }
    });
};
exports.default = sendMail;
