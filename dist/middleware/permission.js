"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.permission = void 0;
const http_status_codes_1 = require("http-status-codes");
const jsonwebtoken_1 = __importDefault(require("jsonwebtoken"));
const errorCodeMatcher_1 = require("../helpers/errorCodeMatcher");
const EErrorMessages_1 = require("../enums/EErrorMessages");
const permission = (roles) => {
    return (expressReq, res, next) => {
        const req = expressReq;
        if (req.method === "OPTIONS")
            next();
        try {
            const data = jsonwebtoken_1.default.verify(req.get("Authorization") || "", process.env.SECRET_KEY || "");
            if (roles && !roles.includes(data.role))
                throw new Error(EErrorMessages_1.EErrorMessages.NO_ACCESS);
            req.dataFromToken = data;
            next();
        }
        catch (err) {
            const error = err;
            const status = errorCodeMatcher_1.errorCodesMathcher[error.message] || http_status_codes_1.StatusCodes.UNAUTHORIZED;
            res.status(status).send(error.message);
        }
    };
};
exports.permission = permission;
