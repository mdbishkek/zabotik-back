"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.Child = void 0;
const sequelize_typescript_1 = require("sequelize-typescript");
const Parent_1 = require("./Parent");
const ESex_1 = require("../enums/ESex");
const Document_1 = require("./Document");
const Allergy_1 = require("./Allergy");
const SpecialistExam_1 = require("./SpecialistExam");
const Vaccination_1 = require("./Vaccination");
const Visit_1 = require("./Visit");
const NewbornData_1 = require("./NewbornData");
const Question_1 = require("./Question");
let Child = class Child extends sequelize_typescript_1.Model {
};
__decorate([
    (0, sequelize_typescript_1.HasMany)(() => Document_1.Document),
    __metadata("design:type", Array)
], Child.prototype, "documents", void 0);
__decorate([
    (0, sequelize_typescript_1.HasMany)(() => Allergy_1.Allergy),
    __metadata("design:type", Array)
], Child.prototype, "allergies", void 0);
__decorate([
    (0, sequelize_typescript_1.HasMany)(() => SpecialistExam_1.SpecialistExam),
    __metadata("design:type", Array)
], Child.prototype, "specialistExams", void 0);
__decorate([
    (0, sequelize_typescript_1.HasMany)(() => Vaccination_1.Vaccination),
    __metadata("design:type", Array)
], Child.prototype, "vaccinations", void 0);
__decorate([
    (0, sequelize_typescript_1.HasMany)(() => Visit_1.Visit),
    __metadata("design:type", Array)
], Child.prototype, "visits", void 0);
__decorate([
    (0, sequelize_typescript_1.HasMany)(() => Question_1.Question),
    __metadata("design:type", Array)
], Child.prototype, "questions", void 0);
__decorate([
    (0, sequelize_typescript_1.HasOne)(() => NewbornData_1.NewbornData),
    __metadata("design:type", NewbornData_1.NewbornData)
], Child.prototype, "newbornDatas", void 0);
__decorate([
    (0, sequelize_typescript_1.BelongsTo)(() => Parent_1.Parent),
    __metadata("design:type", Parent_1.Parent)
], Child.prototype, "parents", void 0);
__decorate([
    sequelize_typescript_1.PrimaryKey,
    (0, sequelize_typescript_1.Column)({
        type: sequelize_typescript_1.DataType.UUID,
        defaultValue: sequelize_typescript_1.DataType.UUIDV4
    }),
    __metadata("design:type", String)
], Child.prototype, "id", void 0);
__decorate([
    (0, sequelize_typescript_1.ForeignKey)(() => Parent_1.Parent),
    (0, sequelize_typescript_1.Column)({
        type: sequelize_typescript_1.DataType.UUID,
        field: "parent_id",
        allowNull: false
    }),
    __metadata("design:type", String)
], Child.prototype, "parentId", void 0);
__decorate([
    (0, sequelize_typescript_1.Column)({
        type: sequelize_typescript_1.DataType.STRING,
        allowNull: false
    }),
    __metadata("design:type", String)
], Child.prototype, "photo", void 0);
__decorate([
    (0, sequelize_typescript_1.Column)({
        type: sequelize_typescript_1.DataType.STRING(256),
        allowNull: false
    }),
    __metadata("design:type", String)
], Child.prototype, "name", void 0);
__decorate([
    (0, sequelize_typescript_1.Column)({
        type: sequelize_typescript_1.DataType.STRING(256),
        allowNull: false
    }),
    __metadata("design:type", String)
], Child.prototype, "surname", void 0);
__decorate([
    (0, sequelize_typescript_1.Column)({
        field: "date_of_birth",
        type: sequelize_typescript_1.DataType.DATEONLY,
        allowNull: false
    }),
    __metadata("design:type", Date)
], Child.prototype, "dateOfBirth", void 0);
__decorate([
    (0, sequelize_typescript_1.Column)({
        type: sequelize_typescript_1.DataType.ENUM(...Object.values(ESex_1.ESex)),
        allowNull: false
    }),
    __metadata("design:type", String)
], Child.prototype, "sex", void 0);
__decorate([
    (0, sequelize_typescript_1.Column)({
        type: sequelize_typescript_1.DataType.INTEGER,
        defaultValue: 0,
        allowNull: false
    }),
    __metadata("design:type", Number)
], Child.prototype, "height", void 0);
__decorate([
    (0, sequelize_typescript_1.Column)({
        type: sequelize_typescript_1.DataType.DECIMAL(6, 3),
        defaultValue: 0,
        allowNull: false
    }),
    __metadata("design:type", Number)
], Child.prototype, "weight", void 0);
__decorate([
    (0, sequelize_typescript_1.Column)({
        type: sequelize_typescript_1.DataType.STRING(256),
        allowNull: true
    }),
    __metadata("design:type", String)
], Child.prototype, "patronim", void 0);
__decorate([
    (0, sequelize_typescript_1.Column)({
        field: "is_active",
        type: sequelize_typescript_1.DataType.BOOLEAN,
        allowNull: false,
        defaultValue: true
    }),
    __metadata("design:type", Boolean)
], Child.prototype, "isActive", void 0);
Child = __decorate([
    (0, sequelize_typescript_1.Table)({
        tableName: "children",
        timestamps: false
    })
], Child);
exports.Child = Child;
