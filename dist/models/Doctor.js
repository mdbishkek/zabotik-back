"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.Doctor = void 0;
const sequelize_typescript_1 = require("sequelize-typescript");
const User_1 = require("./User");
const Diploma_1 = require("./Diploma");
const Question_1 = require("./Question");
const Parent_1 = require("./Parent");
const Recommendation_1 = require("./Recommendation");
let Doctor = class Doctor extends sequelize_typescript_1.Model {
};
__decorate([
    (0, sequelize_typescript_1.HasMany)(() => Diploma_1.Diploma),
    __metadata("design:type", Array)
], Doctor.prototype, "diplomas", void 0);
__decorate([
    (0, sequelize_typescript_1.HasMany)(() => Question_1.Question),
    __metadata("design:type", Array)
], Doctor.prototype, "questions", void 0);
__decorate([
    (0, sequelize_typescript_1.HasMany)(() => Parent_1.Parent),
    __metadata("design:type", Array)
], Doctor.prototype, "parents", void 0);
__decorate([
    (0, sequelize_typescript_1.HasMany)(() => Recommendation_1.Recommendation),
    __metadata("design:type", Array)
], Doctor.prototype, "recommendations", void 0);
__decorate([
    (0, sequelize_typescript_1.BelongsTo)(() => User_1.User),
    __metadata("design:type", User_1.User)
], Doctor.prototype, "users", void 0);
__decorate([
    sequelize_typescript_1.PrimaryKey,
    (0, sequelize_typescript_1.Column)({
        type: sequelize_typescript_1.DataType.UUID,
        defaultValue: sequelize_typescript_1.DataType.UUIDV4
    }),
    __metadata("design:type", String)
], Doctor.prototype, "id", void 0);
__decorate([
    (0, sequelize_typescript_1.ForeignKey)(() => User_1.User),
    (0, sequelize_typescript_1.Column)({
        type: sequelize_typescript_1.DataType.UUID,
        field: "user_id",
        allowNull: false
    }),
    __metadata("design:type", String)
], Doctor.prototype, "userId", void 0);
__decorate([
    (0, sequelize_typescript_1.Column)({
        type: sequelize_typescript_1.DataType.STRING,
        allowNull: false
    }),
    __metadata("design:type", String)
], Doctor.prototype, "photo", void 0);
__decorate([
    (0, sequelize_typescript_1.Column)({
        type: sequelize_typescript_1.DataType.STRING,
        allowNull: false,
        defaultValue: "-"
    }),
    __metadata("design:type", String)
], Doctor.prototype, "speciality", void 0);
__decorate([
    (0, sequelize_typescript_1.Column)({
        field: "place_of_work",
        type: sequelize_typescript_1.DataType.STRING(256),
        allowNull: false,
        defaultValue: "-"
    }),
    __metadata("design:type", String)
], Doctor.prototype, "placeOfWork", void 0);
__decorate([
    (0, sequelize_typescript_1.Column)({
        type: sequelize_typescript_1.DataType.INTEGER,
        allowNull: false,
        defaultValue: 0,
        validate: {
            max: 80,
            min: 0
        }
    }),
    __metadata("design:type", Number)
], Doctor.prototype, "experience", void 0);
__decorate([
    (0, sequelize_typescript_1.Column)({
        field: "is_active",
        type: sequelize_typescript_1.DataType.BOOLEAN,
        allowNull: false,
        defaultValue: true
    }),
    __metadata("design:type", Boolean)
], Doctor.prototype, "isActive", void 0);
__decorate([
    (0, sequelize_typescript_1.Column)({
        type: sequelize_typescript_1.DataType.DECIMAL(10, 2),
        allowNull: false,
        defaultValue: 0.00
    }),
    __metadata("design:type", Number)
], Doctor.prototype, "price", void 0);
__decorate([
    (0, sequelize_typescript_1.Column)({
        type: sequelize_typescript_1.DataType.STRING,
        allowNull: true
    }),
    __metadata("design:type", String)
], Doctor.prototype, "achievements", void 0);
__decorate([
    (0, sequelize_typescript_1.Column)({
        type: sequelize_typescript_1.DataType.STRING(256),
        allowNull: true
    }),
    __metadata("design:type", String)
], Doctor.prototype, "degree", void 0);
Doctor = __decorate([
    (0, sequelize_typescript_1.Table)({
        tableName: "doctors",
        timestamps: false
    })
], Doctor);
exports.Doctor = Doctor;
