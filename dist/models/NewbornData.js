"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.NewbornData = void 0;
const sequelize_typescript_1 = require("sequelize-typescript");
const Child_1 = require("./Child");
let NewbornData = class NewbornData extends sequelize_typescript_1.Model {
};
__decorate([
    (0, sequelize_typescript_1.BelongsTo)(() => Child_1.Child),
    __metadata("design:type", Child_1.Child)
], NewbornData.prototype, "children", void 0);
__decorate([
    sequelize_typescript_1.PrimaryKey,
    (0, sequelize_typescript_1.Column)({
        type: sequelize_typescript_1.DataType.UUID,
        defaultValue: sequelize_typescript_1.DataType.UUIDV4
    }),
    __metadata("design:type", String)
], NewbornData.prototype, "id", void 0);
__decorate([
    (0, sequelize_typescript_1.ForeignKey)(() => Child_1.Child),
    (0, sequelize_typescript_1.Column)({
        type: sequelize_typescript_1.DataType.UUID,
        field: "child_id",
        allowNull: false
    }),
    __metadata("design:type", String)
], NewbornData.prototype, "childId", void 0);
__decorate([
    (0, sequelize_typescript_1.Column)({
        field: "discharged_date",
        type: sequelize_typescript_1.DataType.DATEONLY,
        allowNull: false,
        defaultValue: sequelize_typescript_1.DataType.NOW
    }),
    __metadata("design:type", Date)
], NewbornData.prototype, "dischargedDate", void 0);
__decorate([
    (0, sequelize_typescript_1.Column)({
        field: "pregnancy_n",
        type: sequelize_typescript_1.DataType.INTEGER,
        defaultValue: 0,
        allowNull: false
    }),
    __metadata("design:type", Number)
], NewbornData.prototype, "pregnancyN", void 0);
__decorate([
    (0, sequelize_typescript_1.Column)({
        field: "pregnancy_descript",
        type: sequelize_typescript_1.DataType.STRING,
        defaultValue: "-",
        allowNull: false
    }),
    __metadata("design:type", String)
], NewbornData.prototype, "pregnancyDescript", void 0);
__decorate([
    (0, sequelize_typescript_1.Column)({
        field: "birth_n",
        type: sequelize_typescript_1.DataType.INTEGER,
        defaultValue: 0,
        allowNull: false
    }),
    __metadata("design:type", Number)
], NewbornData.prototype, "birthN", void 0);
__decorate([
    (0, sequelize_typescript_1.Column)({
        field: "gest_age",
        type: sequelize_typescript_1.DataType.INTEGER,
        defaultValue: 0,
        allowNull: false
    }),
    __metadata("design:type", Number)
], NewbornData.prototype, "gestAge", void 0);
__decorate([
    (0, sequelize_typescript_1.Column)({
        field: "period_1",
        type: sequelize_typescript_1.DataType.INTEGER,
        defaultValue: 0,
        allowNull: false
    }),
    __metadata("design:type", Number)
], NewbornData.prototype, "period1", void 0);
__decorate([
    (0, sequelize_typescript_1.Column)({
        field: "period_2",
        type: sequelize_typescript_1.DataType.INTEGER,
        defaultValue: 0,
        allowNull: false
    }),
    __metadata("design:type", Number)
], NewbornData.prototype, "period2", void 0);
__decorate([
    (0, sequelize_typescript_1.Column)({
        field: "amn_abs_period",
        type: sequelize_typescript_1.DataType.INTEGER,
        defaultValue: 0,
        allowNull: false
    }),
    __metadata("design:type", Number)
], NewbornData.prototype, "amnAbsPeriod", void 0);
__decorate([
    (0, sequelize_typescript_1.Column)({
        field: "amn_descript",
        type: sequelize_typescript_1.DataType.STRING(256),
        defaultValue: "-",
        allowNull: false
    }),
    __metadata("design:type", String)
], NewbornData.prototype, "amnDescript", void 0);
__decorate([
    (0, sequelize_typescript_1.Column)({
        type: sequelize_typescript_1.DataType.STRING(256),
        defaultValue: "-",
        allowNull: false
    }),
    __metadata("design:type", String)
], NewbornData.prototype, "anesthesia", void 0);
__decorate([
    (0, sequelize_typescript_1.Column)({
        field: "post_birth_period",
        type: sequelize_typescript_1.DataType.STRING(256),
        defaultValue: "-",
        allowNull: false
    }),
    __metadata("design:type", String)
], NewbornData.prototype, "postBirthPeriod", void 0);
__decorate([
    (0, sequelize_typescript_1.Column)({
        field: "mother_state",
        type: sequelize_typescript_1.DataType.STRING,
        defaultValue: "-",
        allowNull: false
    }),
    __metadata("design:type", String)
], NewbornData.prototype, "motherState", void 0);
__decorate([
    (0, sequelize_typescript_1.Column)({
        field: "birth_weight",
        type: sequelize_typescript_1.DataType.DECIMAL(4, 3),
        defaultValue: 0,
        allowNull: false
    }),
    __metadata("design:type", Number)
], NewbornData.prototype, "birthWeight", void 0);
__decorate([
    (0, sequelize_typescript_1.Column)({
        field: "birth_height",
        type: sequelize_typescript_1.DataType.INTEGER,
        defaultValue: 0,
        allowNull: false
    }),
    __metadata("design:type", Number)
], NewbornData.prototype, "birthHeight", void 0);
__decorate([
    (0, sequelize_typescript_1.Column)({
        field: "newborn_state",
        type: sequelize_typescript_1.DataType.STRING(256),
        defaultValue: "-",
        allowNull: false
    }),
    __metadata("design:type", String)
], NewbornData.prototype, "newbornState", void 0);
__decorate([
    (0, sequelize_typescript_1.Column)({
        field: "apgar_score",
        type: sequelize_typescript_1.DataType.STRING(10),
        defaultValue: "-",
        allowNull: false
    }),
    __metadata("design:type", String)
], NewbornData.prototype, "apgarScore", void 0);
__decorate([
    (0, sequelize_typescript_1.Column)({
        type: sequelize_typescript_1.DataType.STRING(256),
        defaultValue: "-",
        allowNull: false
    }),
    __metadata("design:type", String)
], NewbornData.prototype, "reanimation", void 0);
__decorate([
    (0, sequelize_typescript_1.Column)({
        field: "breast_try",
        type: sequelize_typescript_1.DataType.BOOLEAN,
        defaultValue: true,
        allowNull: false
    }),
    __metadata("design:type", Boolean)
], NewbornData.prototype, "breastTry", void 0);
__decorate([
    (0, sequelize_typescript_1.Column)({
        type: sequelize_typescript_1.DataType.STRING(256),
        defaultValue: "-",
        allowNull: false
    }),
    __metadata("design:type", String)
], NewbornData.prototype, "feeding", void 0);
__decorate([
    (0, sequelize_typescript_1.Column)({
        type: sequelize_typescript_1.DataType.STRING,
        defaultValue: "-",
        allowNull: false
    }),
    __metadata("design:type", String)
], NewbornData.prototype, "diagnosis", void 0);
__decorate([
    (0, sequelize_typescript_1.Column)({
        type: sequelize_typescript_1.DataType.STRING,
        defaultValue: "-",
        allowNull: false
    }),
    __metadata("design:type", String)
], NewbornData.prototype, "examination", void 0);
__decorate([
    (0, sequelize_typescript_1.Column)({
        type: sequelize_typescript_1.DataType.STRING,
        defaultValue: "-",
        allowNull: false
    }),
    __metadata("design:type", String)
], NewbornData.prototype, "treatment", void 0);
__decorate([
    (0, sequelize_typescript_1.Column)({
        type: sequelize_typescript_1.DataType.STRING(256),
        defaultValue: "-",
        allowNull: false
    }),
    __metadata("design:type", String)
], NewbornData.prototype, "eyes", void 0);
__decorate([
    (0, sequelize_typescript_1.Column)({
        type: sequelize_typescript_1.DataType.STRING(256),
        defaultValue: "-",
        allowNull: false
    }),
    __metadata("design:type", String)
], NewbornData.prototype, "reflexes", void 0);
__decorate([
    (0, sequelize_typescript_1.Column)({
        type: sequelize_typescript_1.DataType.STRING(256),
        defaultValue: "-",
        allowNull: false
    }),
    __metadata("design:type", String)
], NewbornData.prototype, "skin", void 0);
__decorate([
    (0, sequelize_typescript_1.Column)({
        type: sequelize_typescript_1.DataType.STRING(256),
        defaultValue: "-",
        allowNull: false
    }),
    __metadata("design:type", String)
], NewbornData.prototype, "organs", void 0);
__decorate([
    (0, sequelize_typescript_1.Column)({
        type: sequelize_typescript_1.DataType.STRING(256),
        defaultValue: "-",
        allowNull: false
    }),
    __metadata("design:type", String)
], NewbornData.prototype, "stool", void 0);
__decorate([
    (0, sequelize_typescript_1.Column)({
        type: sequelize_typescript_1.DataType.STRING(256),
        defaultValue: "-",
        allowNull: false
    }),
    __metadata("design:type", String)
], NewbornData.prototype, "diuresis", void 0);
__decorate([
    (0, sequelize_typescript_1.Column)({
        field: "umbilical_cord",
        type: sequelize_typescript_1.DataType.STRING(256),
        defaultValue: "-",
        allowNull: false
    }),
    __metadata("design:type", String)
], NewbornData.prototype, "umbilicalCord", void 0);
__decorate([
    (0, sequelize_typescript_1.Column)({
        field: "examed_by",
        type: sequelize_typescript_1.DataType.STRING(256),
        defaultValue: "-",
        allowNull: false
    }),
    __metadata("design:type", String)
], NewbornData.prototype, "examedBy", void 0);
__decorate([
    (0, sequelize_typescript_1.Column)({
        type: sequelize_typescript_1.DataType.STRING(256),
        defaultValue: "-",
        allowNull: true
    }),
    __metadata("design:type", String)
], NewbornData.prototype, "notes", void 0);
__decorate([
    (0, sequelize_typescript_1.Column)({
        field: "feeding_reason",
        defaultValue: "-",
        type: sequelize_typescript_1.DataType.STRING(256),
        allowNull: true
    }),
    __metadata("design:type", String)
], NewbornData.prototype, "feedingReason", void 0);
NewbornData = __decorate([
    (0, sequelize_typescript_1.Table)({
        tableName: "newbornDatas",
        timestamps: false
    })
], NewbornData);
exports.NewbornData = NewbornData;
