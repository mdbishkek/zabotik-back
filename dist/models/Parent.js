"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.Parent = void 0;
const sequelize_typescript_1 = require("sequelize-typescript");
const User_1 = require("./User");
const Doctor_1 = require("./Doctor");
const Child_1 = require("./Child");
const Question_1 = require("./Question");
let Parent = class Parent extends sequelize_typescript_1.Model {
};
__decorate([
    (0, sequelize_typescript_1.HasMany)(() => Child_1.Child),
    __metadata("design:type", Array)
], Parent.prototype, "children", void 0);
__decorate([
    (0, sequelize_typescript_1.HasMany)(() => Question_1.Question),
    __metadata("design:type", Array)
], Parent.prototype, "question", void 0);
__decorate([
    (0, sequelize_typescript_1.BelongsTo)(() => User_1.User),
    __metadata("design:type", User_1.User)
], Parent.prototype, "users", void 0);
__decorate([
    (0, sequelize_typescript_1.BelongsTo)(() => Doctor_1.Doctor),
    __metadata("design:type", Doctor_1.Doctor)
], Parent.prototype, "doctors", void 0);
__decorate([
    sequelize_typescript_1.PrimaryKey,
    (0, sequelize_typescript_1.Column)({
        type: sequelize_typescript_1.DataType.UUID,
        defaultValue: sequelize_typescript_1.DataType.UUIDV4
    }),
    __metadata("design:type", String)
], Parent.prototype, "id", void 0);
__decorate([
    (0, sequelize_typescript_1.ForeignKey)(() => User_1.User),
    (0, sequelize_typescript_1.Column)({
        type: sequelize_typescript_1.DataType.UUID,
        field: "user_id",
        allowNull: false
    }),
    __metadata("design:type", String)
], Parent.prototype, "userId", void 0);
__decorate([
    (0, sequelize_typescript_1.ForeignKey)(() => Doctor_1.Doctor),
    (0, sequelize_typescript_1.Column)({
        type: sequelize_typescript_1.DataType.UUID,
        field: "doctor_id",
        allowNull: false
    }),
    __metadata("design:type", String)
], Parent.prototype, "doctorId", void 0);
__decorate([
    (0, sequelize_typescript_1.Column)({
        field: "register_date",
        type: sequelize_typescript_1.DataType.DATE,
        allowNull: false,
        defaultValue: sequelize_typescript_1.DataType.NOW
    }),
    __metadata("design:type", Date)
], Parent.prototype, "registerDate", void 0);
__decorate([
    (0, sequelize_typescript_1.Column)({
        field: "is_active",
        type: sequelize_typescript_1.DataType.BOOLEAN,
        allowNull: false,
        defaultValue: false
    }),
    __metadata("design:type", Boolean)
], Parent.prototype, "isActive", void 0);
__decorate([
    (0, sequelize_typescript_1.Column)({
        type: sequelize_typescript_1.DataType.DATE,
        allowNull: false,
        field: "subscription_end_date"
    }),
    __metadata("design:type", Date)
], Parent.prototype, "subscriptionEndDate", void 0);
Parent = __decorate([
    (0, sequelize_typescript_1.Table)({
        tableName: "parents",
        timestamps: false
    })
], Parent);
exports.Parent = Parent;
