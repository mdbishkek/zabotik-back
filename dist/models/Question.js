"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.Question = void 0;
const sequelize_typescript_1 = require("sequelize-typescript");
const Doctor_1 = require("./Doctor");
const Child_1 = require("./Child");
const Parent_1 = require("./Parent");
const Message_1 = require("./Message");
let Question = class Question extends sequelize_typescript_1.Model {
};
__decorate([
    (0, sequelize_typescript_1.HasMany)(() => Message_1.Message),
    __metadata("design:type", Array)
], Question.prototype, "messages", void 0);
__decorate([
    (0, sequelize_typescript_1.BelongsTo)(() => Doctor_1.Doctor),
    __metadata("design:type", Doctor_1.Doctor)
], Question.prototype, "doctors", void 0);
__decorate([
    (0, sequelize_typescript_1.BelongsTo)(() => Parent_1.Parent),
    __metadata("design:type", Parent_1.Parent)
], Question.prototype, "parents", void 0);
__decorate([
    (0, sequelize_typescript_1.BelongsTo)(() => Child_1.Child),
    __metadata("design:type", Child_1.Child)
], Question.prototype, "children", void 0);
__decorate([
    sequelize_typescript_1.PrimaryKey,
    (0, sequelize_typescript_1.Column)({
        type: sequelize_typescript_1.DataType.UUID,
        defaultValue: sequelize_typescript_1.DataType.UUIDV4
    }),
    __metadata("design:type", String)
], Question.prototype, "id", void 0);
__decorate([
    (0, sequelize_typescript_1.ForeignKey)(() => Doctor_1.Doctor),
    (0, sequelize_typescript_1.Column)({
        type: sequelize_typescript_1.DataType.UUID,
        field: "doctor_id",
        allowNull: false
    }),
    __metadata("design:type", String)
], Question.prototype, "doctorId", void 0);
__decorate([
    (0, sequelize_typescript_1.ForeignKey)(() => Child_1.Child),
    (0, sequelize_typescript_1.Column)({
        type: sequelize_typescript_1.DataType.UUID,
        field: "child_id",
        allowNull: false
    }),
    __metadata("design:type", String)
], Question.prototype, "childId", void 0);
__decorate([
    (0, sequelize_typescript_1.ForeignKey)(() => Parent_1.Parent),
    (0, sequelize_typescript_1.Column)({
        type: sequelize_typescript_1.DataType.UUID,
        field: "parent_id",
        allowNull: false
    }),
    __metadata("design:type", String)
], Question.prototype, "parentId", void 0);
__decorate([
    (0, sequelize_typescript_1.Column)({
        field: "created_at",
        type: sequelize_typescript_1.DataType.DATE,
        allowNull: false,
        defaultValue: sequelize_typescript_1.DataType.NOW
    }),
    __metadata("design:type", Date)
], Question.prototype, "createdAt", void 0);
__decorate([
    (0, sequelize_typescript_1.Column)({
        type: sequelize_typescript_1.DataType.STRING(256),
        allowNull: false
    }),
    __metadata("design:type", String)
], Question.prototype, "question", void 0);
Question = __decorate([
    (0, sequelize_typescript_1.Table)({
        tableName: "questions",
        timestamps: false
    })
], Question);
exports.Question = Question;
