"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.User = void 0;
const sequelize_typescript_1 = require("sequelize-typescript");
const ERoles_1 = require("../enums/ERoles");
const Doctor_1 = require("./Doctor");
const Parent_1 = require("./Parent");
const Message_1 = require("./Message");
const Subscription_1 = require("./Subscription");
const Review_1 = require("./Review");
const MessagesStatus_1 = require("./MessagesStatus");
let User = class User extends sequelize_typescript_1.Model {
};
__decorate([
    (0, sequelize_typescript_1.HasMany)(() => Doctor_1.Doctor),
    __metadata("design:type", Array)
], User.prototype, "doctors", void 0);
__decorate([
    (0, sequelize_typescript_1.HasMany)(() => Parent_1.Parent),
    __metadata("design:type", Array)
], User.prototype, "parents", void 0);
__decorate([
    (0, sequelize_typescript_1.HasMany)(() => Message_1.Message),
    __metadata("design:type", Array)
], User.prototype, "messages", void 0);
__decorate([
    (0, sequelize_typescript_1.HasMany)(() => MessagesStatus_1.MessagesStatus),
    __metadata("design:type", Array)
], User.prototype, "messagesStatus", void 0);
__decorate([
    (0, sequelize_typescript_1.HasMany)(() => Review_1.Review),
    __metadata("design:type", Array)
], User.prototype, "reviews", void 0);
__decorate([
    (0, sequelize_typescript_1.HasMany)(() => Subscription_1.Subscription, "userId"),
    __metadata("design:type", Array)
], User.prototype, "subscriptions", void 0);
__decorate([
    (0, sequelize_typescript_1.HasMany)(() => Subscription_1.Subscription, "payedBy"),
    __metadata("design:type", Array)
], User.prototype, "payers", void 0);
__decorate([
    sequelize_typescript_1.PrimaryKey,
    (0, sequelize_typescript_1.Column)({
        type: sequelize_typescript_1.DataType.UUID,
        defaultValue: sequelize_typescript_1.DataType.UUIDV4
    }),
    __metadata("design:type", String)
], User.prototype, "id", void 0);
__decorate([
    (0, sequelize_typescript_1.Column)({
        type: sequelize_typescript_1.DataType.ENUM(...Object.values(ERoles_1.ERoles)),
        allowNull: false
    }),
    __metadata("design:type", String)
], User.prototype, "role", void 0);
__decorate([
    (0, sequelize_typescript_1.Column)({
        type: sequelize_typescript_1.DataType.STRING(256),
        allowNull: false,
        unique: true,
        validate: {
            isEmail: true
        }
    }),
    __metadata("design:type", String)
], User.prototype, "email", void 0);
__decorate([
    (0, sequelize_typescript_1.Column)({
        type: sequelize_typescript_1.DataType.STRING(256),
        allowNull: false
    }),
    __metadata("design:type", String)
], User.prototype, "phone", void 0);
__decorate([
    (0, sequelize_typescript_1.Column)({
        type: sequelize_typescript_1.DataType.STRING(256),
        allowNull: false
    }),
    __metadata("design:type", String)
], User.prototype, "name", void 0);
__decorate([
    (0, sequelize_typescript_1.Column)({
        type: sequelize_typescript_1.DataType.STRING(256),
        allowNull: false
    }),
    __metadata("design:type", String)
], User.prototype, "surname", void 0);
__decorate([
    (0, sequelize_typescript_1.Column)({
        type: sequelize_typescript_1.DataType.STRING(256),
        allowNull: false,
    }),
    __metadata("design:type", String)
], User.prototype, "password", void 0);
__decorate([
    (0, sequelize_typescript_1.Column)({
        type: sequelize_typescript_1.DataType.STRING(256),
        allowNull: true
    }),
    __metadata("design:type", String)
], User.prototype, "patronim", void 0);
__decorate([
    (0, sequelize_typescript_1.Column)({
        field: "is_blocked",
        type: sequelize_typescript_1.DataType.BOOLEAN,
        allowNull: false,
        defaultValue: false
    }),
    __metadata("design:type", Boolean)
], User.prototype, "isBlocked", void 0);
User = __decorate([
    (0, sequelize_typescript_1.Table)({
        tableName: "users",
        timestamps: false
    })
], User);
exports.User = User;
