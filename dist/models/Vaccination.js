"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.Vaccination = void 0;
const sequelize_typescript_1 = require("sequelize-typescript");
const Child_1 = require("./Child");
let Vaccination = class Vaccination extends sequelize_typescript_1.Model {
};
__decorate([
    (0, sequelize_typescript_1.BelongsTo)(() => Child_1.Child),
    __metadata("design:type", Child_1.Child)
], Vaccination.prototype, "children", void 0);
__decorate([
    sequelize_typescript_1.PrimaryKey,
    (0, sequelize_typescript_1.Column)({
        type: sequelize_typescript_1.DataType.UUID,
        defaultValue: sequelize_typescript_1.DataType.UUIDV4
    }),
    __metadata("design:type", String)
], Vaccination.prototype, "id", void 0);
__decorate([
    (0, sequelize_typescript_1.ForeignKey)(() => Child_1.Child),
    (0, sequelize_typescript_1.Column)({
        type: sequelize_typescript_1.DataType.UUID,
        field: "child_id",
        allowNull: false
    }),
    __metadata("design:type", String)
], Vaccination.prototype, "childId", void 0);
__decorate([
    (0, sequelize_typescript_1.Column)({
        type: sequelize_typescript_1.DataType.STRING(256),
        allowNull: false
    }),
    __metadata("design:type", String)
], Vaccination.prototype, "infection", void 0);
__decorate([
    (0, sequelize_typescript_1.Column)({
        type: sequelize_typescript_1.DataType.STRING,
        allowNull: true
    }),
    __metadata("design:type", String)
], Vaccination.prototype, "vaccine", void 0);
__decorate([
    (0, sequelize_typescript_1.Column)({
        type: sequelize_typescript_1.DataType.STRING(256),
        allowNull: true
    }),
    __metadata("design:type", String)
], Vaccination.prototype, "age", void 0);
__decorate([
    (0, sequelize_typescript_1.Column)({
        type: sequelize_typescript_1.DataType.DATEONLY,
        allowNull: true
    }),
    __metadata("design:type", Date)
], Vaccination.prototype, "date", void 0);
__decorate([
    (0, sequelize_typescript_1.Column)({
        type: sequelize_typescript_1.DataType.STRING(256),
        allowNull: true
    }),
    __metadata("design:type", String)
], Vaccination.prototype, "dose", void 0);
__decorate([
    (0, sequelize_typescript_1.Column)({
        type: sequelize_typescript_1.DataType.STRING(256),
        allowNull: true
    }),
    __metadata("design:type", String)
], Vaccination.prototype, "serial", void 0);
__decorate([
    (0, sequelize_typescript_1.Column)({
        type: sequelize_typescript_1.DataType.STRING(256),
        allowNull: true
    }),
    __metadata("design:type", String)
], Vaccination.prototype, "manufacturer", void 0);
__decorate([
    (0, sequelize_typescript_1.Column)({
        type: sequelize_typescript_1.DataType.STRING(256),
        allowNull: true
    }),
    __metadata("design:type", String)
], Vaccination.prototype, "reaction", void 0);
__decorate([
    (0, sequelize_typescript_1.Column)({
        type: sequelize_typescript_1.DataType.STRING(256),
        allowNull: true
    }),
    __metadata("design:type", String)
], Vaccination.prototype, "conterindication", void 0);
__decorate([
    (0, sequelize_typescript_1.Column)({
        type: sequelize_typescript_1.DataType.STRING(256),
        allowNull: true
    }),
    __metadata("design:type", String)
], Vaccination.prototype, "notes", void 0);
Vaccination = __decorate([
    (0, sequelize_typescript_1.Table)({
        tableName: "vaccinations",
        timestamps: false
    })
], Vaccination);
exports.Vaccination = Vaccination;
