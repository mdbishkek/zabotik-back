"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.postgresDB = exports.PostgresDB = void 0;
const dotenv_1 = __importDefault(require("dotenv"));
const path_1 = __importDefault(require("path"));
const sequelize_typescript_1 = require("sequelize-typescript");
const logger_1 = __importDefault(require("../lib/logger"));
dotenv_1.default.config();
class PostgresDB {
    constructor() {
        this.getSequelize = () => {
            return this.sequelize;
        };
        this.close = async () => {
            await this.sequelize.close();
        };
        this.init = async () => {
            try {
                await this.sequelize.authenticate();
                await this.sequelize.sync({
                    alter: true
                });
                logger_1.default.info("DB postgres is connected");
            }
            catch (err) {
                const error = err;
                logger_1.default.error(error.message);
            }
        };
        this.sequelize = new sequelize_typescript_1.Sequelize({
            database: process.env.PG_DB,
            dialect: "postgres",
            host: process.env.PG_HOST,
            username: process.env.PG_USER,
            password: process.env.PG_PASS,
            storage: ":memory",
            models: [path_1.default.join(__dirname, "../models")]
        });
    }
}
exports.PostgresDB = PostgresDB;
exports.postgresDB = new PostgresDB();
