"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.allergiesDb = exports.AllergiesDb = void 0;
const http_status_codes_1 = require("http-status-codes");
const ERoles_1 = require("../../enums/ERoles");
const Doctor_1 = require("../../models/Doctor");
const User_1 = require("../../models/User");
const errorCodeMatcher_1 = require("../../helpers/errorCodeMatcher");
const EErrorMessages_1 = require("../../enums/EErrorMessages");
const Child_1 = require("../../models/Child");
const Parent_1 = require("../../models/Parent");
const Allergy_1 = require("../../models/Allergy");
class AllergiesDb {
    constructor() {
        this.getAllergies = async (userId, childId) => {
            try {
                const foundUser = await User_1.User.findByPk(userId);
                if (!foundUser)
                    throw new Error(EErrorMessages_1.EErrorMessages.NO_ACCESS);
                const foundChild = await Child_1.Child.findByPk(childId);
                if (!foundChild)
                    throw new Error(EErrorMessages_1.EErrorMessages.CHILD_NOT_FOUND);
                if (foundUser.role === ERoles_1.ERoles.DOCTOR) {
                    const foundDoctor = await Doctor_1.Doctor.findOne({ where: { userId } });
                    const foundParent = await Parent_1.Parent.findOne({ where: { id: foundChild.parentId } });
                    if (!foundDoctor || !foundParent || foundDoctor.id !== foundParent.doctorId)
                        throw new Error(EErrorMessages_1.EErrorMessages.NO_ACCESS);
                }
                if (foundUser.role === ERoles_1.ERoles.PARENT) {
                    const foundParentByUser = await Parent_1.Parent.findOne({ where: { userId } });
                    if (!foundParentByUser || foundChild.parentId !== foundParentByUser.id)
                        throw new Error(EErrorMessages_1.EErrorMessages.NO_ACCESS);
                }
                const allergies = await Allergy_1.Allergy.findAll({
                    where: { childId }
                });
                return {
                    status: http_status_codes_1.StatusCodes.OK,
                    result: allergies
                };
            }
            catch (err) {
                const error = err;
                const status = errorCodeMatcher_1.errorCodesMathcher[error.message] || http_status_codes_1.StatusCodes.INTERNAL_SERVER_ERROR;
                return {
                    status,
                    result: {
                        status: "error",
                        message: error.message
                    }
                };
            }
        };
        this.createAllergy = async (userId, allDto) => {
            try {
                const foundUser = await User_1.User.findByPk(userId);
                if (!foundUser || foundUser.isBlocked)
                    throw new Error(EErrorMessages_1.EErrorMessages.NO_ACCESS);
                const foundChild = await Child_1.Child.findByPk(allDto.childId);
                if (!foundChild)
                    throw new Error(EErrorMessages_1.EErrorMessages.CHILD_NOT_FOUND);
                if (foundUser.role === ERoles_1.ERoles.DOCTOR) {
                    const foundDoctor = await Doctor_1.Doctor.findOne({ where: { userId } });
                    const foundParent = await Parent_1.Parent.findOne({ where: { id: foundChild.parentId } });
                    if (!foundDoctor || !foundParent || foundDoctor.id !== foundParent.doctorId)
                        throw new Error(EErrorMessages_1.EErrorMessages.NO_ACCESS);
                }
                if (foundUser.role === ERoles_1.ERoles.PARENT) {
                    const foundParentByUser = await Parent_1.Parent.findOne({ where: { userId } });
                    if (!foundParentByUser || foundChild.parentId !== foundParentByUser.id)
                        throw new Error(EErrorMessages_1.EErrorMessages.NO_ACCESS);
                }
                const newAllergy = await Allergy_1.Allergy.create({ ...allDto });
                return {
                    status: http_status_codes_1.StatusCodes.CREATED,
                    result: newAllergy
                };
            }
            catch (err) {
                const error = err;
                const status = errorCodeMatcher_1.errorCodesMathcher[error.message] || http_status_codes_1.StatusCodes.BAD_REQUEST;
                return {
                    status,
                    result: {
                        status: "error",
                        message: error.message
                    }
                };
            }
        };
        this.deleteAllergy = async (userId, allId) => {
            try {
                const foundUser = await User_1.User.findByPk(userId);
                if (!foundUser || foundUser.isBlocked)
                    throw new Error(EErrorMessages_1.EErrorMessages.NO_ACCESS);
                const allergy = await Allergy_1.Allergy.findByPk(allId);
                if (!allergy)
                    throw new Error(EErrorMessages_1.EErrorMessages.ALLERGY_NOT_FOUND);
                const foundChild = await Child_1.Child.findOne({ where: { id: allergy.childId } });
                if (!foundChild)
                    throw new Error(EErrorMessages_1.EErrorMessages.CHILD_NOT_FOUND);
                if (foundUser.role === ERoles_1.ERoles.DOCTOR) {
                    const foundDoctor = await Doctor_1.Doctor.findOne({ where: { userId } });
                    const foundParent = await Parent_1.Parent.findOne({ where: { id: foundChild.parentId } });
                    if (!foundDoctor || !foundParent || foundDoctor.id !== foundParent.doctorId)
                        throw new Error(EErrorMessages_1.EErrorMessages.NO_ACCESS);
                }
                if (foundUser.role === ERoles_1.ERoles.PARENT) {
                    const foundParentByUser = await Parent_1.Parent.findOne({ where: { userId } });
                    if (!foundParentByUser || foundChild.parentId !== foundParentByUser.id)
                        throw new Error(EErrorMessages_1.EErrorMessages.NO_ACCESS);
                }
                await Allergy_1.Allergy.destroy({ where: { id: allId } });
                return {
                    status: http_status_codes_1.StatusCodes.OK,
                    result: { message: "Запись об аллергии удалена!" }
                };
            }
            catch (err) {
                const error = err;
                const status = errorCodeMatcher_1.errorCodesMathcher[error.message] || http_status_codes_1.StatusCodes.INTERNAL_SERVER_ERROR;
                return {
                    status,
                    result: {
                        status: "error",
                        message: error.message
                    }
                };
            }
        };
    }
}
exports.AllergiesDb = AllergiesDb;
exports.allergiesDb = new AllergiesDb();
