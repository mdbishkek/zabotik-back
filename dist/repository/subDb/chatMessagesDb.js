"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.chatMessagesDb = exports.ChatMessagesDb = void 0;
const http_status_codes_1 = require("http-status-codes");
const errorCodeMatcher_1 = require("../../helpers/errorCodeMatcher");
const User_1 = require("../../models/User");
const EErrorMessages_1 = require("../../enums/EErrorMessages");
const Question_1 = require("../../models/Question");
const ERoles_1 = require("../../enums/ERoles");
const Parent_1 = require("../../models/Parent");
const Doctor_1 = require("../../models/Doctor");
const Message_1 = require("../../models/Message");
const deleteFile_1 = require("../../helpers/deleteFile");
const MessagesStatus_1 = require("../../models/MessagesStatus");
class ChatMessagesDb {
    constructor() {
        this.getMessagesByQuestion = async (userId, questionId) => {
            try {
                const foundUser = await User_1.User.findByPk(userId);
                if (!foundUser)
                    throw new Error(EErrorMessages_1.EErrorMessages.NO_ACCESS);
                const foundQuestion = await Question_1.Question.findByPk(questionId);
                if (!foundQuestion)
                    throw new Error(EErrorMessages_1.EErrorMessages.QUESTION_NOT_FOUND);
                const messages = await Message_1.Message.findAll({
                    where: { questionId },
                    include: {
                        model: User_1.User,
                        as: "users",
                        attributes: ["role", "name", "patronim", "surname"]
                    },
                    order: [
                        ["createdAt", "ASC"]
                    ]
                });
                return {
                    status: http_status_codes_1.StatusCodes.OK,
                    result: messages
                };
            }
            catch (err) {
                const error = err;
                const status = errorCodeMatcher_1.errorCodesMathcher[error.message] || http_status_codes_1.StatusCodes.INTERNAL_SERVER_ERROR;
                return {
                    status,
                    result: {
                        status: "error",
                        message: error.message
                    }
                };
            }
        };
        this.createMessage = async (userId, message) => {
            try {
                const foundUser = await User_1.User.findByPk(userId);
                if (!foundUser)
                    throw new Error(EErrorMessages_1.EErrorMessages.NO_ACCESS);
                const foundQuestion = await Question_1.Question.findByPk(message.questionId);
                if (!foundQuestion)
                    throw new Error(EErrorMessages_1.EErrorMessages.QUESTION_NOT_FOUND);
                const newDate = new Date();
                if (foundUser.role === ERoles_1.ERoles.PARENT) {
                    const foundParent = await Parent_1.Parent.findOne({ where: { userId: foundUser.id } });
                    if (!foundParent || foundParent.subscriptionEndDate.getTime() < newDate.getTime()
                        || foundParent.id !== foundQuestion.parentId)
                        throw new Error(EErrorMessages_1.EErrorMessages.NO_ACCESS);
                }
                if (foundUser.role === ERoles_1.ERoles.DOCTOR) {
                    const foundDoctor = await Doctor_1.Doctor.findOne({ where: { userId: foundUser.id } });
                    if (!foundDoctor || foundDoctor.id !== foundQuestion.doctorId)
                        throw new Error(EErrorMessages_1.EErrorMessages.NO_ACCESS);
                }
                const newMessage = await Message_1.Message.create({ ...message });
                await MessagesStatus_1.MessagesStatus.create({
                    messageId: newMessage.id,
                    userId: foundUser.id
                });
                await MessagesStatus_1.MessagesStatus.create({
                    messageId: newMessage.id,
                    userId: foundUser.id,
                });
                return {
                    status: http_status_codes_1.StatusCodes.CREATED,
                    result: newMessage
                };
            }
            catch (err) {
                const error = err;
                if (message.url) {
                    (0, deleteFile_1.deleteFile)(message.url, "messagesFiles");
                }
                const status = errorCodeMatcher_1.errorCodesMathcher[error.message] || http_status_codes_1.StatusCodes.BAD_REQUEST;
                return {
                    status,
                    result: {
                        status: "error",
                        message: error.message
                    }
                };
            }
        };
        this.deleteMessage = async (userId, messageId) => {
            try {
                const foundUser = await User_1.User.findByPk(userId);
                if (!foundUser)
                    throw new Error(EErrorMessages_1.EErrorMessages.NO_ACCESS);
                const foundMessage = await Message_1.Message.findByPk(messageId);
                if (!foundMessage)
                    throw new Error(EErrorMessages_1.EErrorMessages.MESSAGE_NOT_FOUND);
                const foundQuestion = await Question_1.Question.findByPk(foundMessage.questionId);
                if (!foundQuestion)
                    throw new Error(EErrorMessages_1.EErrorMessages.QUESTION_NOT_FOUND);
                if (foundMessage.authorId !== foundUser.id)
                    throw new Error(EErrorMessages_1.EErrorMessages.NO_ACCESS);
                await Message_1.Message.destroy({ where: { id: messageId } });
                if (foundMessage.url || foundMessage.url !== "" && foundMessage.url !== "default-any-image.svg") {
                    (0, deleteFile_1.deleteFile)(foundMessage.url, "messagesFiles");
                }
                return {
                    status: http_status_codes_1.StatusCodes.OK,
                    result: { message: "Сообщение удалено!" }
                };
            }
            catch (err) {
                const error = err;
                const status = errorCodeMatcher_1.errorCodesMathcher[error.message] || http_status_codes_1.StatusCodes.INTERNAL_SERVER_ERROR;
                return {
                    status,
                    result: {
                        status: "error",
                        message: error.message
                    }
                };
            }
        };
    }
}
exports.ChatMessagesDb = ChatMessagesDb;
exports.chatMessagesDb = new ChatMessagesDb();
