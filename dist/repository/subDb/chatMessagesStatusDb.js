"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.chatMessagesStatusDb = exports.ChatMessagesStatusDb = void 0;
const http_status_codes_1 = require("http-status-codes");
const errorCodeMatcher_1 = require("../../helpers/errorCodeMatcher");
const User_1 = require("../../models/User");
const EErrorMessages_1 = require("../../enums/EErrorMessages");
const Message_1 = require("../../models/Message");
const MessagesStatus_1 = require("../../models/MessagesStatus");
class ChatMessagesStatusDb {
    constructor() {
        this.getMessagesStatusByMessage = async (userId, messageId) => {
            try {
                const foundUser = await User_1.User.findByPk(userId);
                if (!foundUser)
                    throw new Error(EErrorMessages_1.EErrorMessages.NO_ACCESS);
                const foundMessage = await Message_1.Message.findByPk(messageId);
                if (!foundMessage)
                    throw new Error(EErrorMessages_1.EErrorMessages.MESSAGE_NOT_FOUND);
                const messagesStatusArray = await MessagesStatus_1.MessagesStatus.findAll({
                    where: { messageId }
                });
                return {
                    status: http_status_codes_1.StatusCodes.OK,
                    result: messagesStatusArray
                };
            }
            catch (err) {
                const error = err;
                const status = errorCodeMatcher_1.errorCodesMathcher[error.message] || http_status_codes_1.StatusCodes.INTERNAL_SERVER_ERROR;
                return {
                    status,
                    result: {
                        status: "error",
                        message: error.message
                    }
                };
            }
        };
        this.createMessageStatus = async (userId, messageStatus) => {
            try {
                const foundUser = await User_1.User.findByPk(userId);
                if (!foundUser)
                    throw new Error(EErrorMessages_1.EErrorMessages.NO_ACCESS);
                const foundMessage = await Message_1.Message.findByPk(messageStatus.messageId);
                if (!foundMessage)
                    throw new Error(EErrorMessages_1.EErrorMessages.MESSAGE_NOT_FOUND);
                const foundStatus = await MessagesStatus_1.MessagesStatus.findOne({
                    where: {
                        messageId: foundMessage.id,
                        userId: userId,
                        isRead: true
                    }
                });
                if (!foundStatus) {
                    await MessagesStatus_1.MessagesStatus.create({ ...messageStatus });
                    return {
                        status: http_status_codes_1.StatusCodes.CREATED,
                        result: { message: "Статус сообщения создан!" }
                    };
                }
                return {
                    status: http_status_codes_1.StatusCodes.OK,
                    result: { message: "Сообщение прочитано." }
                };
            }
            catch (err) {
                const error = err;
                const status = errorCodeMatcher_1.errorCodesMathcher[error.message] || http_status_codes_1.StatusCodes.BAD_REQUEST;
                return {
                    status,
                    result: {
                        status: "error",
                        message: error.message
                    }
                };
            }
        };
    }
}
exports.ChatMessagesStatusDb = ChatMessagesStatusDb;
exports.chatMessagesStatusDb = new ChatMessagesStatusDb();
