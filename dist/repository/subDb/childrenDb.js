"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.childrenDb = exports.ChildrenDb = void 0;
const http_status_codes_1 = require("http-status-codes");
const Child_1 = require("../../models/Child");
const User_1 = require("../../models/User");
const EErrorMessages_1 = require("../../enums/EErrorMessages");
const ERoles_1 = require("../../enums/ERoles");
const errorCodeMatcher_1 = require("../../helpers/errorCodeMatcher");
const Parent_1 = require("../../models/Parent");
const Doctor_1 = require("../../models/Doctor");
const NewbornData_1 = require("../../models/NewbornData");
const deleteFile_1 = require("../../helpers/deleteFile");
const postgresDb_1 = require("../postgresDb");
class ChildrenDb {
    constructor() {
        this.getChildrenByParentId = async (parentId, userId) => {
            try {
                const foundUser = await User_1.User.findByPk(userId);
                if (!foundUser)
                    throw new Error(EErrorMessages_1.EErrorMessages.NO_ACCESS);
                if (foundUser.role !== ERoles_1.ERoles.PARENT && foundUser.isBlocked)
                    throw new Error(EErrorMessages_1.EErrorMessages.NO_ACCESS);
                const foundParent = await Parent_1.Parent.findByPk(parentId);
                if (!foundParent)
                    throw new Error(EErrorMessages_1.EErrorMessages.PARENT_NOT_FOUND);
                if (foundUser.role === ERoles_1.ERoles.PARENT) {
                    if (foundParent.userId !== userId)
                        throw new Error(EErrorMessages_1.EErrorMessages.NO_ACCESS);
                }
                if (foundUser.role === ERoles_1.ERoles.DOCTOR) {
                    const foundDoctor = await Doctor_1.Doctor.findOne({ where: { userId } });
                    if (!foundDoctor || foundDoctor.id !== foundParent.doctorId)
                        throw new Error(EErrorMessages_1.EErrorMessages.NO_ACCESS);
                }
                const children = await Child_1.Child.findAll({
                    where: { parentId },
                    order: [
                        ["dateOfBirth", "DESC"]
                    ]
                });
                return {
                    status: http_status_codes_1.StatusCodes.OK,
                    result: children,
                };
            }
            catch (err) {
                const error = err;
                const status = errorCodeMatcher_1.errorCodesMathcher[error.message] || http_status_codes_1.StatusCodes.INTERNAL_SERVER_ERROR;
                return {
                    status,
                    result: {
                        status: "error",
                        message: error.message
                    }
                };
            }
        };
        this.getChildrenForDoctor = async (doctorId, userId) => {
            try {
                const foundUser = await User_1.User.findByPk(userId);
                if (!foundUser)
                    throw new Error(EErrorMessages_1.EErrorMessages.NO_ACCESS);
                if (foundUser.role !== ERoles_1.ERoles.DOCTOR && foundUser.isBlocked)
                    throw new Error(EErrorMessages_1.EErrorMessages.NO_ACCESS);
                const foundDoctor = await Doctor_1.Doctor.findByPk(doctorId);
                if (!foundDoctor)
                    throw new Error(EErrorMessages_1.EErrorMessages.DOCTOR_NOT_FOUND);
                const foundChildren = await Child_1.Child.findAll({
                    include: {
                        model: Parent_1.Parent,
                        where: { doctorId },
                        attributes: ["userId"]
                    },
                    order: [
                        ["surname", "ASC"],
                        ["name", "ASC"]
                    ]
                });
                return {
                    status: http_status_codes_1.StatusCodes.OK,
                    result: foundChildren,
                };
            }
            catch (err) {
                const error = err;
                const status = errorCodeMatcher_1.errorCodesMathcher[error.message] || http_status_codes_1.StatusCodes.INTERNAL_SERVER_ERROR;
                return {
                    status,
                    result: {
                        status: "error",
                        message: error.message
                    }
                };
            }
        };
        this.getChildrenByDoctorId = async (userId, offset, limit, doctorId) => {
            try {
                const foundUser = await User_1.User.findByPk(userId);
                if (!foundUser || foundUser.isBlocked)
                    throw new Error(EErrorMessages_1.EErrorMessages.NO_ACCESS);
                const foundDoctor = await Doctor_1.Doctor.findByPk(doctorId);
                if (!foundDoctor)
                    throw new Error(EErrorMessages_1.EErrorMessages.DOCTOR_NOT_FOUND);
                const foundChildren = await Child_1.Child.findAndCountAll({
                    include: {
                        model: Parent_1.Parent,
                        as: "parents",
                        where: { doctorId },
                        attributes: ["userId"]
                    },
                    order: [
                        ["surname", "ASC"],
                        ["name", "ASC"]
                    ],
                    limit: parseInt(limit),
                    offset: parseInt(offset)
                });
                return {
                    status: http_status_codes_1.StatusCodes.OK,
                    result: foundChildren,
                };
            }
            catch (err) {
                const error = err;
                const status = errorCodeMatcher_1.errorCodesMathcher[error.message] || http_status_codes_1.StatusCodes.INTERNAL_SERVER_ERROR;
                return {
                    status,
                    result: {
                        status: "error",
                        message: error.message
                    }
                };
            }
        };
        this.getChildById = async (childId, userId) => {
            try {
                const foundUser = await User_1.User.findByPk(userId);
                if (!foundUser)
                    throw new Error(EErrorMessages_1.EErrorMessages.NO_ACCESS);
                if (foundUser.isBlocked && foundUser.role !== ERoles_1.ERoles.PARENT)
                    throw new Error(EErrorMessages_1.EErrorMessages.NO_ACCESS);
                const child = await Child_1.Child.findByPk(childId);
                if (!child)
                    throw new Error(EErrorMessages_1.EErrorMessages.CHILD_NOT_FOUND);
                if (foundUser.role === ERoles_1.ERoles.PARENT) {
                    const foundParentByUser = await Parent_1.Parent.findOne({ where: { userId } });
                    if (!foundParentByUser || foundParentByUser.id !== child.parentId)
                        throw new Error(EErrorMessages_1.EErrorMessages.NO_ACCESS);
                }
                const foundParent = await Parent_1.Parent.findOne({ where: { id: child.parentId } });
                if (!foundParent)
                    throw new Error(EErrorMessages_1.EErrorMessages.PARENT_NOT_FOUND);
                if (foundUser.role === ERoles_1.ERoles.DOCTOR) {
                    const foundDoctor = await Doctor_1.Doctor.findOne({ where: { userId } });
                    if (!foundDoctor || foundDoctor.id !== foundParent.doctorId)
                        throw new Error(EErrorMessages_1.EErrorMessages.NO_ACCESS);
                }
                return {
                    status: http_status_codes_1.StatusCodes.OK,
                    result: child,
                };
            }
            catch (err) {
                const error = err;
                const status = errorCodeMatcher_1.errorCodesMathcher[error.message] || http_status_codes_1.StatusCodes.INTERNAL_SERVER_ERROR;
                return {
                    status,
                    result: {
                        status: "error",
                        message: error.message
                    }
                };
            }
        };
        this.createChild = async (child, userId) => {
            const transaction = await postgresDb_1.postgresDB.getSequelize().transaction();
            try {
                const foundUser = await User_1.User.findByPk(userId, { transaction });
                if (!foundUser)
                    throw new Error(EErrorMessages_1.EErrorMessages.NO_ACCESS);
                if (foundUser.isBlocked && foundUser.role !== ERoles_1.ERoles.PARENT)
                    throw new Error(EErrorMessages_1.EErrorMessages.NO_ACCESS);
                if (foundUser.role === ERoles_1.ERoles.PARENT) {
                    const foundParentByUser = await Parent_1.Parent.findOne({ where: { userId }, transaction });
                    if (!foundParentByUser || foundParentByUser.id !== child.parentId) {
                        throw new Error(EErrorMessages_1.EErrorMessages.NO_ACCESS);
                    }
                }
                const foundParent = await Parent_1.Parent.findOne({ where: { id: child.parentId }, transaction });
                if (!foundParent)
                    throw new Error(EErrorMessages_1.EErrorMessages.PARENT_NOT_FOUND);
                if (foundUser.role === ERoles_1.ERoles.DOCTOR) {
                    const foundDoctor = await Doctor_1.Doctor.findOne({ where: { userId }, transaction });
                    if (!foundDoctor || foundParent.doctorId !== foundDoctor.id) {
                        throw new Error(EErrorMessages_1.EErrorMessages.NO_ACCESS);
                    }
                }
                if (child.photo === "") {
                    child.photo = "default-child-photo.svg";
                }
                const newChild = await Child_1.Child.create({ ...child }, { transaction });
                const newbornData = {
                    childId: newChild.id
                };
                await NewbornData_1.NewbornData.create({ ...newbornData }, { transaction });
                await transaction.commit();
                return {
                    status: http_status_codes_1.StatusCodes.CREATED,
                    result: newChild,
                };
            }
            catch (err) {
                await transaction.rollback();
                if (child.photo && child.photo !== "default-child-photo.svg") {
                    (0, deleteFile_1.deleteFile)(child.photo, "childrenImgs");
                }
                const error = err;
                const status = errorCodeMatcher_1.errorCodesMathcher[error.message] || http_status_codes_1.StatusCodes.INTERNAL_SERVER_ERROR;
                return {
                    status,
                    result: {
                        status: "error",
                        message: error.message
                    }
                };
            }
        };
        this.editChildById = async (childId, child, userId) => {
            try {
                const foundUser = await User_1.User.findByPk(userId);
                if (!foundUser)
                    throw new Error(EErrorMessages_1.EErrorMessages.NO_ACCESS);
                if (foundUser.isBlocked && foundUser.role !== ERoles_1.ERoles.PARENT)
                    throw new Error(EErrorMessages_1.EErrorMessages.NO_ACCESS);
                const foundChild = await Child_1.Child.findByPk(childId);
                if (!foundChild)
                    throw new Error(EErrorMessages_1.EErrorMessages.CHILD_NOT_FOUND);
                if (foundUser.role === ERoles_1.ERoles.PARENT) {
                    const foundParentByUser = await Parent_1.Parent.findOne({ where: { userId } });
                    if (!foundParentByUser || foundChild.parentId !== foundParentByUser.id)
                        throw new Error(EErrorMessages_1.EErrorMessages.NO_ACCESS);
                }
                const foundParent = await Parent_1.Parent.findOne({ where: { id: foundChild.parentId } });
                if (!foundParent)
                    throw new Error(EErrorMessages_1.EErrorMessages.PARENT_NOT_FOUND);
                if (foundUser.role === ERoles_1.ERoles.DOCTOR) {
                    const foundDoctor = await Doctor_1.Doctor.findOne({ where: { userId } });
                    if (!foundDoctor || foundDoctor.id !== foundParent.doctorId) {
                        throw new Error(EErrorMessages_1.EErrorMessages.NO_ACCESS);
                    }
                }
                const updatedChild = await Child_1.Child.update({ ...child }, { where: { id: foundChild.id }, returning: true }).then((result) => {
                    return result[1][0];
                });
                return {
                    status: http_status_codes_1.StatusCodes.OK,
                    result: updatedChild,
                };
            }
            catch (err) {
                const error = err;
                const status = errorCodeMatcher_1.errorCodesMathcher[error.message] || http_status_codes_1.StatusCodes.BAD_REQUEST;
                return {
                    status,
                    result: {
                        status: "error",
                        message: error.message
                    }
                };
            }
        };
    }
}
exports.ChildrenDb = ChildrenDb;
exports.childrenDb = new ChildrenDb();
