"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.diplomasDb = exports.DiplomasDb = void 0;
const http_status_codes_1 = require("http-status-codes");
const Diploma_1 = require("../../models/Diploma");
const ERoles_1 = require("../../enums/ERoles");
const Doctor_1 = require("../../models/Doctor");
const User_1 = require("../../models/User");
const Parent_1 = require("../../models/Parent");
const errorCodeMatcher_1 = require("../../helpers/errorCodeMatcher");
const EErrorMessages_1 = require("../../enums/EErrorMessages");
const deleteFile_1 = require("../../helpers/deleteFile");
class DiplomasDb {
    constructor() {
        this.getDiplomasByDoctor = async (userId, doctorId) => {
            try {
                const foundUser = await User_1.User.findByPk(userId);
                if (!foundUser)
                    throw new Error(EErrorMessages_1.EErrorMessages.NO_ACCESS);
                const foundDoctor = await Doctor_1.Doctor.findByPk(doctorId);
                if (!foundDoctor)
                    throw new Error(EErrorMessages_1.EErrorMessages.DOCTOR_DIPLOMA_NOT_FOUND);
                if (foundUser.role === ERoles_1.ERoles.DOCTOR && foundUser.id !== foundDoctor.userId)
                    throw new Error(EErrorMessages_1.EErrorMessages.NO_ACCESS);
                if (foundUser.role === ERoles_1.ERoles.PARENT) {
                    const foundParent = await Parent_1.Parent.findOne({
                        where: { userId: foundUser.id, doctorId: doctorId }
                    });
                    if (!foundParent)
                        throw new Error(EErrorMessages_1.EErrorMessages.NO_ACCESS);
                }
                const diplomas = await Diploma_1.Diploma.findAll({
                    where: { doctorId: doctorId }
                });
                return {
                    status: http_status_codes_1.StatusCodes.OK,
                    result: diplomas
                };
            }
            catch (err) {
                const error = err;
                const status = errorCodeMatcher_1.errorCodesMathcher[error.message] || http_status_codes_1.StatusCodes.INTERNAL_SERVER_ERROR;
                return {
                    status,
                    result: {
                        status: "error",
                        message: error.message
                    }
                };
            }
        };
        this.createDiploma = async (userId, diploma) => {
            try {
                const foundUser = await User_1.User.findByPk(userId);
                if (!foundUser || foundUser.isBlocked)
                    throw new Error(EErrorMessages_1.EErrorMessages.NO_ACCESS);
                if (foundUser.role === ERoles_1.ERoles.DOCTOR) {
                    const foundDoctor = await Doctor_1.Doctor.findOne({
                        where: { userId: foundUser.id }
                    });
                    if (!foundDoctor || diploma.doctorId !== foundDoctor.id)
                        throw new Error(EErrorMessages_1.EErrorMessages.NO_ACCESS);
                }
                if (diploma.url === "")
                    throw new Error(EErrorMessages_1.EErrorMessages.IMAGE_REQUIRED);
                const newDiploma = await Diploma_1.Diploma.create({ ...diploma });
                return {
                    status: http_status_codes_1.StatusCodes.CREATED,
                    result: newDiploma
                };
            }
            catch (err) {
                if (diploma.url) {
                    (0, deleteFile_1.deleteFile)(diploma.url, "doctorsDiplomas");
                }
                const error = err;
                const status = errorCodeMatcher_1.errorCodesMathcher[error.message] || http_status_codes_1.StatusCodes.BAD_REQUEST;
                return {
                    status,
                    result: {
                        status: "error",
                        message: error.message
                    }
                };
            }
        };
        this.deleteDiploma = async (userId, diplomaId) => {
            try {
                const foundUser = await User_1.User.findByPk(userId);
                if (!foundUser || foundUser.isBlocked)
                    throw new Error(EErrorMessages_1.EErrorMessages.NO_ACCESS);
                const diploma = await Diploma_1.Diploma.findByPk(diplomaId);
                if (!diploma)
                    throw new Error(EErrorMessages_1.EErrorMessages.DIPLOMA_NOT_FOUND);
                if (foundUser.role === ERoles_1.ERoles.DOCTOR) {
                    const foundDoctor = await Doctor_1.Doctor.findOne({
                        where: { userId: foundUser.id }
                    });
                    if (!foundDoctor || diploma.doctorId !== foundDoctor.id)
                        throw new Error(EErrorMessages_1.EErrorMessages.NO_ACCESS);
                }
                await Diploma_1.Diploma.destroy({ where: { id: diplomaId } });
                if (diploma.url) {
                    (0, deleteFile_1.deleteFile)(diploma.url, "doctorsDiplomas");
                }
                return {
                    status: http_status_codes_1.StatusCodes.OK,
                    result: { message: "Диплом удален!" }
                };
            }
            catch (err) {
                const error = err;
                const status = errorCodeMatcher_1.errorCodesMathcher[error.message] || http_status_codes_1.StatusCodes.INTERNAL_SERVER_ERROR;
                return {
                    status,
                    result: {
                        status: "error",
                        message: error.message
                    }
                };
            }
        };
    }
}
exports.DiplomasDb = DiplomasDb;
exports.diplomasDb = new DiplomasDb();
