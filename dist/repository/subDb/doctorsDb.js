"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.doctorsDb = exports.DoctorsDb = void 0;
const http_status_codes_1 = require("http-status-codes");
const Doctor_1 = require("../../models/Doctor");
const User_1 = require("../../models/User");
const ERoles_1 = require("../../enums/ERoles");
const errorCodeMatcher_1 = require("../../helpers/errorCodeMatcher");
const EErrorMessages_1 = require("../../enums/EErrorMessages");
const Parent_1 = require("../../models/Parent");
const EDoctorLevel_1 = require("../../enums/EDoctorLevel");
class DoctorsDb {
    constructor() {
        this.getDoctors = async (userId, offset, limit) => {
            try {
                const foundUser = await User_1.User.findByPk(userId);
                if (!foundUser || foundUser.isBlocked)
                    throw new Error(EErrorMessages_1.EErrorMessages.NO_ACCESS);
                let foundDoctors = { rows: [], count: 0 };
                if (offset && limit) {
                    foundDoctors = await Doctor_1.Doctor.findAndCountAll({
                        where: foundUser.role === ERoles_1.ERoles.DOCTOR ? { userId: foundUser.id } : {},
                        include: {
                            model: User_1.User,
                            as: "users",
                            attributes: ["id", "name", "patronim", "surname", "email", "phone", "isBlocked"]
                        },
                        order: [
                            [{ model: User_1.User, as: "users" }, "surname", "ASC"],
                            [{ model: User_1.User, as: "users" }, "name", "ASC"]
                        ],
                        limit: parseInt(limit),
                        offset: parseInt(offset)
                    });
                }
                else {
                    foundDoctors = await Doctor_1.Doctor.findAndCountAll({
                        where: foundUser.role === ERoles_1.ERoles.DOCTOR ? { userId: foundUser.id } : {},
                        include: {
                            model: User_1.User,
                            as: "users",
                            attributes: ["id", "name", "patronim", "surname", "email", "phone", "isBlocked"]
                        },
                        order: [
                            [{ model: User_1.User, as: "users" }, "surname", "ASC"],
                            [{ model: User_1.User, as: "users" }, "name", "ASC"]
                        ]
                    });
                }
                return {
                    status: http_status_codes_1.StatusCodes.OK,
                    result: foundDoctors
                };
            }
            catch (err) {
                const error = err;
                const status = errorCodeMatcher_1.errorCodesMathcher[error.message] || http_status_codes_1.StatusCodes.INTERNAL_SERVER_ERROR;
                return {
                    status,
                    result: {
                        status: "error",
                        message: error.message
                    }
                };
            }
        };
        this.getDoctorByUserId = async (userId, doctorUserId) => {
            try {
                const foundUser = await User_1.User.findByPk(userId);
                if (!foundUser)
                    throw new Error(EErrorMessages_1.EErrorMessages.NOT_AUTHORIZED);
                const doctor = await Doctor_1.Doctor.findOne({
                    where: { userId: doctorUserId },
                    include: {
                        model: User_1.User,
                        as: "users",
                        attributes: foundUser.role === ERoles_1.ERoles.PARENT
                            ? ["id", "name", "patronim", "surname"]
                            : ["id", "name", "patronim", "surname", "email", "phone"]
                    }
                });
                if (!doctor)
                    throw new Error(EErrorMessages_1.EErrorMessages.DOCTOR_NOT_FOUND);
                if (foundUser.role === ERoles_1.ERoles.PARENT) {
                    const parent = await Parent_1.Parent.findOne({ where: { doctorId: doctor.id } });
                    if (!parent)
                        new Error(EErrorMessages_1.EErrorMessages.NO_ACCESS);
                }
                if (foundUser.role === ERoles_1.ERoles.DOCTOR && foundUser.id !== doctor.userId)
                    throw new Error(EErrorMessages_1.EErrorMessages.NO_ACCESS);
                return {
                    status: http_status_codes_1.StatusCodes.OK,
                    result: doctor
                };
            }
            catch (err) {
                const error = err;
                const status = errorCodeMatcher_1.errorCodesMathcher[error.message] || http_status_codes_1.StatusCodes.INTERNAL_SERVER_ERROR;
                return {
                    status,
                    result: {
                        status: "error",
                        message: error.message
                    }
                };
            }
        };
        this.getDoctorById = async (userId, doctorId) => {
            try {
                const foundUser = await User_1.User.findByPk(userId);
                if (!foundUser)
                    throw new Error(EErrorMessages_1.EErrorMessages.NOT_AUTHORIZED);
                const doctor = await Doctor_1.Doctor.findOne({
                    where: { id: doctorId },
                    include: {
                        model: User_1.User,
                        as: "users",
                        attributes: ["name", "patronim", "surname"]
                    }
                });
                if (!doctor)
                    throw new Error(EErrorMessages_1.EErrorMessages.DOCTOR_NOT_FOUND);
                return {
                    status: http_status_codes_1.StatusCodes.OK,
                    result: doctor
                };
            }
            catch (err) {
                const error = err;
                const status = errorCodeMatcher_1.errorCodesMathcher[error.message] || http_status_codes_1.StatusCodes.INTERNAL_SERVER_ERROR;
                return {
                    status,
                    result: {
                        status: "error",
                        message: error.message
                    }
                };
            }
        };
        this.editDoctor = async (userId, searchId, doctor) => {
            try {
                const foundUser = await User_1.User.findByPk(userId);
                if (!foundUser || foundUser.isBlocked)
                    throw new Error(EErrorMessages_1.EErrorMessages.NO_ACCESS);
                const foundDoctor = await Doctor_1.Doctor.findByPk(searchId);
                if (!foundDoctor)
                    throw new Error(EErrorMessages_1.EErrorMessages.DOCTOR_NOT_FOUND);
                if (foundUser.role === ERoles_1.ERoles.DOCTOR && String(foundUser.id) !== String(foundDoctor.userId)) {
                    throw new Error(EErrorMessages_1.EErrorMessages.NO_ACCESS);
                }
                if (doctor.photo === "") {
                    doctor.photo = "default-photo.svg";
                }
                const updatedDoctor = await Doctor_1.Doctor.update({ ...doctor }, {
                    where: { id: foundDoctor.id },
                    returning: true
                }).then((result) => {
                    return result[1][0];
                });
                return {
                    status: http_status_codes_1.StatusCodes.OK,
                    result: updatedDoctor
                };
            }
            catch (err) {
                const error = err;
                const status = errorCodeMatcher_1.errorCodesMathcher[error.message] || http_status_codes_1.StatusCodes.BAD_REQUEST;
                return {
                    status,
                    result: {
                        status: "error",
                        message: error.message
                    }
                };
            }
        };
        this.activateDoctor = async (userId, doctorId) => {
            try {
                const foundUser = await User_1.User.findByPk(userId);
                if (!foundUser || foundUser.isBlocked)
                    throw new Error(EErrorMessages_1.EErrorMessages.NO_ACCESS);
                const foundDoctor = await Doctor_1.Doctor.findByPk(doctorId);
                if (!foundDoctor)
                    throw new Error(EErrorMessages_1.EErrorMessages.DOCTOR_NOT_FOUND);
                const updatedDoctor = await Doctor_1.Doctor.update({ isActive: foundDoctor.isActive ? false : true }, {
                    where: { id: foundDoctor.id },
                    returning: true
                }).then((result) => { return result[1][0]; });
                return {
                    status: http_status_codes_1.StatusCodes.OK,
                    result: updatedDoctor
                };
            }
            catch (err) {
                const error = err;
                const status = errorCodeMatcher_1.errorCodesMathcher[error.message] || http_status_codes_1.StatusCodes.BAD_REQUEST;
                return {
                    status,
                    result: {
                        status: "error",
                        message: error.message
                    }
                };
            }
        };
        this.changeDoctorPrice = async (userId, doctorId, obj) => {
            try {
                const foundUser = await User_1.User.findByPk(userId);
                if (!foundUser || foundUser.isBlocked)
                    throw new Error(EErrorMessages_1.EErrorMessages.NO_ACCESS);
                const foundDoctor = await Doctor_1.Doctor.findByPk(doctorId);
                if (!foundDoctor)
                    throw new Error(EErrorMessages_1.EErrorMessages.DOCTOR_NOT_FOUND);
                const newPrice = parseInt(obj.price);
                if (!Number.isInteger(newPrice) || !Object.values(EDoctorLevel_1.EDoctorLevel).includes(newPrice))
                    throw new Error(EErrorMessages_1.EErrorMessages.WRONG_PRICE);
                const updatedDoctor = await Doctor_1.Doctor.update({ price: newPrice }, {
                    where: { id: foundDoctor.id },
                    returning: true
                }).then((result) => { return result[1][0]; });
                return {
                    status: http_status_codes_1.StatusCodes.OK,
                    result: updatedDoctor
                };
            }
            catch (err) {
                const error = err;
                const status = errorCodeMatcher_1.errorCodesMathcher[error.message] || http_status_codes_1.StatusCodes.BAD_REQUEST;
                return {
                    status,
                    result: {
                        status: "error",
                        message: error.message
                    }
                };
            }
        };
    }
}
exports.DoctorsDb = DoctorsDb;
exports.doctorsDb = new DoctorsDb();
