"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.documentsDb = exports.DocumentsDb = void 0;
const http_status_codes_1 = require("http-status-codes");
const errorCodeMatcher_1 = require("../../helpers/errorCodeMatcher");
const EErrorMessages_1 = require("../../enums/EErrorMessages");
const User_1 = require("../../models/User");
const Document_1 = require("../../models/Document");
const ERoles_1 = require("../../enums/ERoles");
const Doctor_1 = require("../../models/Doctor");
const Parent_1 = require("../../models/Parent");
const Child_1 = require("../../models/Child");
const deleteFile_1 = require("../../helpers/deleteFile");
class DocumentsDb {
    constructor() {
        this.createDocument = async (userId, document) => {
            try {
                const foundUser = await User_1.User.findByPk(userId);
                if (!foundUser || foundUser.isBlocked)
                    throw new Error(EErrorMessages_1.EErrorMessages.NO_ACCESS);
                const foundChild = await Child_1.Child.findByPk(document.childId);
                if (!foundChild)
                    throw new Error(EErrorMessages_1.EErrorMessages.CHILD_NOT_FOUND);
                if (foundUser.role === ERoles_1.ERoles.DOCTOR) {
                    const foundDoctor = await Doctor_1.Doctor.findOne({ where: { userId } });
                    const foundParent = await Parent_1.Parent.findOne({ where: { id: foundChild?.parentId } });
                    if (foundDoctor?.id !== foundParent?.doctorId)
                        throw new Error(EErrorMessages_1.EErrorMessages.NO_ACCESS);
                }
                if (foundUser.role === ERoles_1.ERoles.PARENT) {
                    const foundParent = await Parent_1.Parent.findOne({ where: { userId } });
                    if (foundChild?.parentId !== foundParent?.id)
                        throw new Error(EErrorMessages_1.EErrorMessages.NO_ACCESS);
                }
                if (document.url === "")
                    throw new Error(EErrorMessages_1.EErrorMessages.IMAGE_REQUIRED);
                const newDocument = await Document_1.Document.create({ ...document });
                return {
                    status: http_status_codes_1.StatusCodes.CREATED,
                    result: newDocument
                };
            }
            catch (err) {
                const error = err;
                if (document.url) {
                    (0, deleteFile_1.deleteFile)(document.url, "childrenDocuments");
                }
                const status = errorCodeMatcher_1.errorCodesMathcher[error.message] || http_status_codes_1.StatusCodes.BAD_REQUEST;
                return {
                    status,
                    result: {
                        status: "error",
                        message: error.message
                    }
                };
            }
        };
        this.deleteDocument = async (userId, documentId) => {
            try {
                const foundUser = await User_1.User.findByPk(userId);
                if (!foundUser || foundUser.isBlocked)
                    throw new Error(EErrorMessages_1.EErrorMessages.NO_ACCESS);
                const document = await Document_1.Document.findByPk(documentId);
                if (!document)
                    throw new Error(EErrorMessages_1.EErrorMessages.DOCUMENT_NOT_FOUND);
                const foundChild = await Child_1.Child.findOne({ where: { id: document.childId } });
                if (foundUser.role === ERoles_1.ERoles.PARENT) {
                    const foundParent = await Parent_1.Parent.findOne({ where: { userId } });
                    if (foundChild?.parentId !== foundParent?.id)
                        throw new Error(EErrorMessages_1.EErrorMessages.NO_ACCESS);
                }
                if (foundUser.role === ERoles_1.ERoles.DOCTOR) {
                    const foundDoctor = await Doctor_1.Doctor.findOne({ where: { userId } });
                    const foundParent = await Parent_1.Parent.findOne({ where: { id: foundChild?.parentId } });
                    if (foundParent?.doctorId !== foundDoctor?.id)
                        throw new Error(EErrorMessages_1.EErrorMessages.NO_ACCESS);
                }
                await Document_1.Document.destroy({ where: { id: documentId } });
                if (document.url && document.url !== "default-any-image.svg") {
                    (0, deleteFile_1.deleteFile)(document.url, "childrenDocuments");
                }
                return {
                    status: http_status_codes_1.StatusCodes.OK,
                    result: { message: "Документ удален!" }
                };
            }
            catch (err) {
                const error = err;
                const status = errorCodeMatcher_1.errorCodesMathcher[error.message] || http_status_codes_1.StatusCodes.INTERNAL_SERVER_ERROR;
                return {
                    status,
                    result: {
                        status: "error",
                        message: error.message
                    }
                };
            }
        };
        this.getDocumentsByChildId = async (userId, childId) => {
            try {
                const foundUser = await User_1.User.findByPk(userId);
                if (!foundUser)
                    throw new Error(EErrorMessages_1.EErrorMessages.NO_ACCESS);
                const foundChild = await Child_1.Child.findByPk(childId);
                if (!foundChild)
                    throw new Error(EErrorMessages_1.EErrorMessages.CHILD_NOT_FOUND);
                if (foundUser.role === ERoles_1.ERoles.DOCTOR) {
                    const foundDoctor = await Doctor_1.Doctor.findOne({ where: { userId } });
                    const foundParent = await Parent_1.Parent.findOne({ where: { id: foundChild.parentId } });
                    if (foundDoctor?.id !== foundParent?.doctorId)
                        throw new Error(EErrorMessages_1.EErrorMessages.NO_ACCESS);
                }
                if (foundUser.role === ERoles_1.ERoles.PARENT) {
                    const foundParent = await Parent_1.Parent.findOne({ where: { userId } });
                    if (foundChild.parentId !== foundParent?.id)
                        throw new Error(EErrorMessages_1.EErrorMessages.NO_ACCESS);
                }
                const documents = await Document_1.Document.findAll({
                    where: { childId },
                    raw: true
                });
                return {
                    status: http_status_codes_1.StatusCodes.OK,
                    result: documents
                };
            }
            catch (err) {
                const error = err;
                const status = errorCodeMatcher_1.errorCodesMathcher[error.message] || http_status_codes_1.StatusCodes.INTERNAL_SERVER_ERROR;
                return {
                    status,
                    result: {
                        status: "error",
                        message: error.message
                    }
                };
            }
        };
    }
}
exports.DocumentsDb = DocumentsDb;
exports.documentsDb = new DocumentsDb();
