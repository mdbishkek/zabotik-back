"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.newbornDatasDb = exports.NewbornDatasDb = void 0;
const http_status_codes_1 = require("http-status-codes");
const EErrorMessages_1 = require("../../enums/EErrorMessages");
const errorCodeMatcher_1 = require("../../helpers/errorCodeMatcher");
const User_1 = require("../../models/User");
const Child_1 = require("../../models/Child");
const ERoles_1 = require("../../enums/ERoles");
const Doctor_1 = require("../../models/Doctor");
const Parent_1 = require("../../models/Parent");
const NewbornData_1 = require("../../models/NewbornData");
class NewbornDatasDb {
    constructor() {
        this.getNewbornDataByChildId = async (userId, childId) => {
            try {
                const foundUser = await User_1.User.findByPk(userId);
                if (!foundUser)
                    throw new Error(EErrorMessages_1.EErrorMessages.NO_ACCESS);
                const foundChild = await Child_1.Child.findByPk(childId);
                if (!foundChild)
                    throw new Error(EErrorMessages_1.EErrorMessages.CHILD_NOT_FOUND);
                if (foundUser.role === ERoles_1.ERoles.DOCTOR) {
                    const foundDoctor = await Doctor_1.Doctor.findOne({ where: { userId } });
                    const foundParent = await Parent_1.Parent.findOne({ where: { id: foundChild.parentId } });
                    if (foundDoctor?.id !== foundParent?.doctorId)
                        throw new Error(EErrorMessages_1.EErrorMessages.NO_ACCESS);
                }
                if (foundUser.role === ERoles_1.ERoles.PARENT) {
                    const foundParent = await Parent_1.Parent.findOne({ where: { userId } });
                    if (foundChild.parentId !== foundParent?.id)
                        throw new Error(EErrorMessages_1.EErrorMessages.NO_ACCESS);
                }
                const newbornData = await NewbornData_1.NewbornData.findAll({
                    where: { childId },
                });
                return {
                    status: http_status_codes_1.StatusCodes.OK,
                    result: newbornData
                };
            }
            catch (err) {
                const error = err;
                const status = errorCodeMatcher_1.errorCodesMathcher[error.message] || http_status_codes_1.StatusCodes.INTERNAL_SERVER_ERROR;
                return {
                    status,
                    result: {
                        status: "error",
                        message: error.message
                    }
                };
            }
        };
        this.updateNewbornDataByChildId = async (userId, childId, newbornData) => {
            try {
                const foundUser = await User_1.User.findByPk(userId);
                if (!foundUser)
                    throw new Error(EErrorMessages_1.EErrorMessages.NO_ACCESS);
                if (foundUser.isBlocked && foundUser.role !== ERoles_1.ERoles.PARENT)
                    throw new Error(EErrorMessages_1.EErrorMessages.NO_ACCESS);
                const foundChild = await Child_1.Child.findByPk(childId);
                if (!foundChild)
                    throw new Error(EErrorMessages_1.EErrorMessages.CHILD_NOT_FOUND);
                const foundNewbornData = await NewbornData_1.NewbornData.findOne({ where: { childId } });
                if (!foundNewbornData)
                    throw new Error(EErrorMessages_1.EErrorMessages.NEWBORN_DATA_NOT_FOUND);
                if (foundUser.role === ERoles_1.ERoles.PARENT) {
                    const foundParentByUser = await Parent_1.Parent.findOne({ where: { userId } });
                    if (!foundParentByUser || foundChild.parentId !== foundParentByUser.id)
                        throw new Error(EErrorMessages_1.EErrorMessages.NO_ACCESS);
                }
                const foundParent = await Parent_1.Parent.findOne({ where: { id: foundChild.parentId } });
                if (!foundParent)
                    throw new Error(EErrorMessages_1.EErrorMessages.PARENT_NOT_FOUND);
                if (foundUser.role === ERoles_1.ERoles.DOCTOR) {
                    const foundDoctor = await Doctor_1.Doctor.findOne({ where: { userId } });
                    if (!foundDoctor || foundDoctor.id !== foundParent.doctorId) {
                        throw new Error(EErrorMessages_1.EErrorMessages.NO_ACCESS);
                    }
                }
                const updatedNewbornData = await NewbornData_1.NewbornData.update({ ...newbornData }, { where: { id: foundNewbornData.id },
                    returning: true }).then((result) => {
                    return result[1][0];
                });
                return {
                    status: http_status_codes_1.StatusCodes.OK,
                    result: updatedNewbornData
                };
            }
            catch (err) {
                const error = err;
                const status = errorCodeMatcher_1.errorCodesMathcher[error.message] || http_status_codes_1.StatusCodes.BAD_REQUEST;
                return {
                    status,
                    result: {
                        status: "error",
                        message: error.message
                    }
                };
            }
        };
    }
}
exports.NewbornDatasDb = NewbornDatasDb;
exports.newbornDatasDb = new NewbornDatasDb();
