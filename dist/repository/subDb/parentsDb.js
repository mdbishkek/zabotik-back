"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.parentsDb = exports.ParentsDb = void 0;
const http_status_codes_1 = require("http-status-codes");
const Parent_1 = require("../../models/Parent");
const User_1 = require("../../models/User");
const EErrorMessages_1 = require("../../enums/EErrorMessages");
const errorCodeMatcher_1 = require("../../helpers/errorCodeMatcher");
const Doctor_1 = require("../../models/Doctor");
const ERoles_1 = require("../../enums/ERoles");
class ParentsDb {
    constructor() {
        this.getParentsByDoctorId = async (userId, offset, limit, doctorId) => {
            try {
                const foundUser = await User_1.User.findByPk(userId);
                if (!foundUser || foundUser.isBlocked)
                    throw new Error(EErrorMessages_1.EErrorMessages.NO_ACCESS);
                const foundDoctor = await Doctor_1.Doctor.findByPk(doctorId);
                if (!foundDoctor)
                    throw new Error(EErrorMessages_1.EErrorMessages.DOCTOR_NOT_FOUND);
                const foundParents = await Parent_1.Parent.findAndCountAll({
                    where: { doctorId },
                    include: {
                        model: User_1.User,
                        as: "users",
                        attributes: ["name", "patronim", "surname", "email", "phone", "isBlocked"]
                    },
                    order: [
                        [{ model: User_1.User, as: "users" }, "surname", "ASC"],
                        [{ model: User_1.User, as: "users" }, "name", "ASC"]
                    ],
                    limit: parseInt(limit),
                    offset: parseInt(offset)
                });
                return {
                    status: http_status_codes_1.StatusCodes.OK,
                    result: foundParents
                };
            }
            catch (err) {
                const error = err;
                const status = errorCodeMatcher_1.errorCodesMathcher[error.message] || http_status_codes_1.StatusCodes.INTERNAL_SERVER_ERROR;
                return {
                    status,
                    result: {
                        status: "error",
                        message: error.message
                    }
                };
            }
        };
        this.activateParent = async (userId, parentId) => {
            try {
                const foundUser = await User_1.User.findByPk(userId);
                if (!foundUser || foundUser.isBlocked)
                    throw new Error(EErrorMessages_1.EErrorMessages.NO_ACCESS);
                const foundParent = await Parent_1.Parent.findByPk(parentId);
                if (!foundParent)
                    throw new Error(EErrorMessages_1.EErrorMessages.PARENT_NOT_FOUND);
                if (foundUser.role === ERoles_1.ERoles.PARENT && foundParent.userId !== foundUser.id)
                    throw new Error(EErrorMessages_1.EErrorMessages.NO_ACCESS);
                const updatedParent = await Parent_1.Parent.update({ isActive: foundParent.isActive ? false : true }, {
                    where: { id: foundParent.id },
                    returning: true
                }).then((result) => {
                    return result[1][0];
                });
                return {
                    status: http_status_codes_1.StatusCodes.OK,
                    result: updatedParent
                };
            }
            catch (err) {
                const error = err;
                const status = errorCodeMatcher_1.errorCodesMathcher[error.message] || http_status_codes_1.StatusCodes.BAD_REQUEST;
                return {
                    status,
                    result: {
                        status: "error",
                        message: error.message
                    }
                };
            }
        };
        this.getParentByUserId = async (userId, parentUserId) => {
            try {
                const foundUser = await User_1.User.findByPk(userId);
                if (!foundUser || foundUser.role !== ERoles_1.ERoles.PARENT && foundUser.isBlocked)
                    throw new Error(EErrorMessages_1.EErrorMessages.NO_ACCESS);
                const foundParent = await Parent_1.Parent.findOne({
                    where: { userId: parentUserId },
                    include: [
                        {
                            model: User_1.User,
                            as: "users",
                            attributes: ["name", "patronim", "surname", "email", "phone"]
                        },
                        {
                            model: Doctor_1.Doctor,
                            as: "doctors",
                            attributes: ["userId", "photo", "speciality", "experience", "degree"],
                            include: [
                                {
                                    model: User_1.User,
                                    as: "users",
                                    attributes: ["name", "patronim", "surname"]
                                },
                            ],
                        }
                    ]
                });
                if (!foundParent)
                    throw new Error(EErrorMessages_1.EErrorMessages.PARENT_NOT_FOUND);
                return {
                    status: http_status_codes_1.StatusCodes.OK,
                    result: foundParent
                };
            }
            catch (err) {
                const error = err;
                const status = errorCodeMatcher_1.errorCodesMathcher[error.message] || http_status_codes_1.StatusCodes.INTERNAL_SERVER_ERROR;
                return {
                    status,
                    result: {
                        status: "error",
                        message: error.message
                    }
                };
            }
        };
    }
}
exports.ParentsDb = ParentsDb;
exports.parentsDb = new ParentsDb();
