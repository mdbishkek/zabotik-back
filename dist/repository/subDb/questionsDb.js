"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.questionsDb = exports.QuestionsDb = void 0;
const http_status_codes_1 = require("http-status-codes");
const errorCodeMatcher_1 = require("../../helpers/errorCodeMatcher");
const User_1 = require("../../models/User");
const EErrorMessages_1 = require("../../enums/EErrorMessages");
const Question_1 = require("../../models/Question");
const Parent_1 = require("../../models/Parent");
const Child_1 = require("../../models/Child");
const Doctor_1 = require("../../models/Doctor");
const Subscription_1 = require("../../models/Subscription");
const ERoles_1 = require("../../enums/ERoles");
class QuestionsDb {
    constructor() {
        this.getQuestionsByChildId = async (userId, childId) => {
            try {
                const foundUser = await User_1.User.findByPk(userId);
                if (!foundUser)
                    throw new Error(EErrorMessages_1.EErrorMessages.NO_ACCESS);
                const foundChild = await Child_1.Child.findByPk(childId);
                if (!foundChild)
                    throw new Error(EErrorMessages_1.EErrorMessages.CHILD_NOT_FOUND);
                if (foundUser.role === ERoles_1.ERoles.DOCTOR) {
                    const foundDoctor = await Doctor_1.Doctor.findOne({ where: { userId } });
                    const foundParent = await Parent_1.Parent.findOne({ where: { id: foundChild.parentId } });
                    if (!foundDoctor || !foundParent || foundDoctor.id !== foundParent.doctorId)
                        throw new Error(EErrorMessages_1.EErrorMessages.NO_ACCESS);
                }
                if (foundUser.role === ERoles_1.ERoles.PARENT) {
                    const foundParentByUser = await Parent_1.Parent.findOne({ where: { userId } });
                    if (!foundParentByUser || foundChild.parentId !== foundParentByUser.id)
                        throw new Error(EErrorMessages_1.EErrorMessages.NO_ACCESS);
                }
                const questions = await Question_1.Question.findAll({
                    where: { childId },
                    order: [
                        ["createdAt", "DESC"]
                    ]
                });
                return {
                    status: http_status_codes_1.StatusCodes.OK,
                    result: questions
                };
            }
            catch (err) {
                const error = err;
                const status = errorCodeMatcher_1.errorCodesMathcher[error.message] || http_status_codes_1.StatusCodes.INTERNAL_SERVER_ERROR;
                return {
                    status,
                    result: {
                        status: "error",
                        message: error.message
                    }
                };
            }
        };
        this.getQuestionsByDoctorId = async (userId, doctorId) => {
            try {
                const foundUser = await User_1.User.findByPk(userId);
                if (!foundUser || foundUser.isBlocked)
                    throw new Error(EErrorMessages_1.EErrorMessages.NO_ACCESS);
                const foundDoctor = await Doctor_1.Doctor.findByPk(doctorId);
                if (!foundDoctor)
                    throw new Error(EErrorMessages_1.EErrorMessages.DOCTOR_NOT_FOUND);
                if (foundUser.role === ERoles_1.ERoles.DOCTOR) {
                    if (foundDoctor.userId !== foundUser.id)
                        throw new Error(EErrorMessages_1.EErrorMessages.NO_ACCESS);
                }
                const questions = await Question_1.Question.findAll({
                    where: { doctorId },
                    order: [
                        ["createdAt", "DESC"]
                    ]
                });
                return {
                    status: http_status_codes_1.StatusCodes.OK,
                    result: questions
                };
            }
            catch (err) {
                const error = err;
                const status = errorCodeMatcher_1.errorCodesMathcher[error.message] || http_status_codes_1.StatusCodes.INTERNAL_SERVER_ERROR;
                return {
                    status,
                    result: {
                        status: "error",
                        message: error.message
                    }
                };
            }
        };
        this.createQuestion = async (userId, question) => {
            try {
                const foundUser = await User_1.User.findByPk(userId);
                if (!foundUser)
                    throw new Error(EErrorMessages_1.EErrorMessages.NO_ACCESS);
                const newDate = new Date();
                const foundSubscription = await Subscription_1.Subscription.findOne({ where: { userId: foundUser.id } });
                if (!foundSubscription || foundSubscription.endDate.getTime() < newDate.getTime()) {
                    throw new Error(EErrorMessages_1.EErrorMessages.NO_ACCESS);
                }
                const foundParent = await Parent_1.Parent.findOne({
                    where: { userId }
                });
                const foundChild = await Child_1.Child.findByPk(question.childId);
                if (!foundChild)
                    throw new Error(EErrorMessages_1.EErrorMessages.CHILD_NOT_FOUND);
                const foundDoctor = await Doctor_1.Doctor.findByPk(question.doctorId);
                if (!foundDoctor)
                    throw new Error(EErrorMessages_1.EErrorMessages.DOCTOR_NOT_FOUND);
                if (!foundParent || foundDoctor.id !== foundParent.doctorId || foundChild.parentId !== foundParent.id)
                    throw new Error(EErrorMessages_1.EErrorMessages.NO_ACCESS);
                const newQuestion = await Question_1.Question.create({ ...question });
                return {
                    status: http_status_codes_1.StatusCodes.CREATED,
                    result: newQuestion
                };
            }
            catch (err) {
                const error = err;
                const status = errorCodeMatcher_1.errorCodesMathcher[error.message] || http_status_codes_1.StatusCodes.BAD_REQUEST;
                return {
                    status,
                    result: {
                        status: "error",
                        message: error.message
                    }
                };
            }
        };
    }
}
exports.QuestionsDb = QuestionsDb;
exports.questionsDb = new QuestionsDb();
