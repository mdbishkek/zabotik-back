"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.recommendationDb = exports.RecommendationsDb = void 0;
const http_status_codes_1 = require("http-status-codes");
const ERoles_1 = require("../../enums/ERoles");
const Doctor_1 = require("../../models/Doctor");
const User_1 = require("../../models/User");
const errorCodeMatcher_1 = require("../../helpers/errorCodeMatcher");
const EErrorMessages_1 = require("../../enums/EErrorMessages");
const Recommendation_1 = require("../../models/Recommendation");
const deleteFile_1 = require("../../helpers/deleteFile");
class RecommendationsDb {
    constructor() {
        this.getRecommendationsByDoctor = async (doctorId) => {
            try {
                const foundDoctor = await Doctor_1.Doctor.findByPk(doctorId);
                if (!foundDoctor)
                    throw new Error(EErrorMessages_1.EErrorMessages.DOCTOR_NOT_FOUND);
                const recommendations = await Recommendation_1.Recommendation.findAll({
                    where: { doctorId: doctorId },
                    order: [
                        ["createdAt", "DESC"]
                    ]
                });
                return {
                    status: http_status_codes_1.StatusCodes.OK,
                    result: recommendations
                };
            }
            catch (err) {
                const error = err;
                const status = errorCodeMatcher_1.errorCodesMathcher[error.message] || http_status_codes_1.StatusCodes.INTERNAL_SERVER_ERROR;
                return {
                    status,
                    result: {
                        status: "error",
                        message: error.message
                    }
                };
            }
        };
        this.createRecommendation = async (userId, recommendation) => {
            try {
                const foundUser = await User_1.User.findByPk(userId);
                if (!foundUser || foundUser.isBlocked)
                    throw new Error(EErrorMessages_1.EErrorMessages.NO_ACCESS);
                if (foundUser.role === ERoles_1.ERoles.DOCTOR) {
                    const foundDoctor = await Doctor_1.Doctor.findOne({
                        where: { userId: foundUser.id }
                    });
                    if (!foundDoctor || recommendation.doctorId !== foundDoctor.id)
                        throw new Error(EErrorMessages_1.EErrorMessages.NO_ACCESS);
                }
                const newRecommendation = await Recommendation_1.Recommendation.create({ ...recommendation });
                return {
                    status: http_status_codes_1.StatusCodes.CREATED,
                    result: newRecommendation
                };
            }
            catch (err) {
                if (recommendation.url) {
                    (0, deleteFile_1.deleteFile)(recommendation.url, "docRecommends");
                }
                const error = err;
                const status = errorCodeMatcher_1.errorCodesMathcher[error.message] || http_status_codes_1.StatusCodes.BAD_REQUEST;
                return {
                    status,
                    result: {
                        status: "error",
                        message: error.message
                    }
                };
            }
        };
        this.editRecommendation = async (userId, recommendationId, upgradedRecommendation) => {
            try {
                const foundUser = await User_1.User.findByPk(userId);
                if (!foundUser || foundUser.isBlocked)
                    throw new Error(EErrorMessages_1.EErrorMessages.NO_ACCESS);
                if (foundUser.role === ERoles_1.ERoles.DOCTOR) {
                    const foundDoctor = await Doctor_1.Doctor.findOne({
                        where: { userId: foundUser.id }
                    });
                    if (!foundDoctor || upgradedRecommendation.doctorId !== foundDoctor.id)
                        throw new Error(EErrorMessages_1.EErrorMessages.NO_ACCESS);
                }
                const editedRecommendation = await Recommendation_1.Recommendation.update({ ...upgradedRecommendation }, {
                    where: { id: recommendationId },
                    returning: true
                }).then((result) => {
                    return result[1][0];
                });
                return {
                    status: http_status_codes_1.StatusCodes.OK,
                    result: editedRecommendation
                };
            }
            catch (err) {
                const error = err;
                const status = errorCodeMatcher_1.errorCodesMathcher[error.message] || http_status_codes_1.StatusCodes.BAD_REQUEST;
                return {
                    status,
                    result: {
                        status: "error",
                        message: error.message
                    }
                };
            }
        };
        this.deleteRecommendation = async (userId, recommendationId) => {
            try {
                const foundUser = await User_1.User.findByPk(userId);
                if (!foundUser || foundUser.isBlocked)
                    throw new Error(EErrorMessages_1.EErrorMessages.NO_ACCESS);
                const recommendation = await Recommendation_1.Recommendation.findByPk(recommendationId);
                if (!recommendation)
                    throw new Error(EErrorMessages_1.EErrorMessages.NO_RECOMMENDATION_FOUND);
                if (foundUser.role === ERoles_1.ERoles.ADMIN || ERoles_1.ERoles.SUPERADMIN) {
                    await Recommendation_1.Recommendation.destroy({
                        where: { id: recommendationId }
                    });
                    return {
                        status: http_status_codes_1.StatusCodes.OK,
                        result: { message: "Публикация удалена!" }
                    };
                }
                else if (foundUser.role === ERoles_1.ERoles.DOCTOR) {
                    const foundDoctor = await Doctor_1.Doctor.findOne({
                        where: { userId: foundUser.id }
                    });
                    if (!foundDoctor || recommendation.doctorId !== foundDoctor.id)
                        throw new Error(EErrorMessages_1.EErrorMessages.NO_ACCESS);
                    await Recommendation_1.Recommendation.destroy({
                        where: { id: recommendationId }
                    });
                    if (recommendation.url) {
                        (0, deleteFile_1.deleteFile)(recommendation.url, "docRecommends");
                    }
                    return {
                        status: http_status_codes_1.StatusCodes.OK,
                        result: { message: "Публикация удалена!" }
                    };
                }
                else {
                    throw new Error(EErrorMessages_1.EErrorMessages.NO_ACCESS);
                }
            }
            catch (err) {
                const error = err;
                const status = errorCodeMatcher_1.errorCodesMathcher[error.message] || http_status_codes_1.StatusCodes.INTERNAL_SERVER_ERROR;
                return {
                    status,
                    result: {
                        status: "error",
                        message: error.message
                    }
                };
            }
        };
    }
}
exports.RecommendationsDb = RecommendationsDb;
exports.recommendationDb = new RecommendationsDb();
