"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.reviewsDb = exports.ReviewsDb = void 0;
const http_status_codes_1 = require("http-status-codes");
const errorCodeMatcher_1 = require("../../helpers/errorCodeMatcher");
const User_1 = require("../../models/User");
const EErrorMessages_1 = require("../../enums/EErrorMessages");
const Review_1 = require("../../models/Review");
const ERoles_1 = require("../../enums/ERoles");
class ReviewsDb {
    constructor() {
        this.getReviews = async (userId, offset, limit) => {
            try {
                const foundUser = await User_1.User.findByPk(userId);
                if (!foundUser || foundUser.isBlocked)
                    throw new Error(EErrorMessages_1.EErrorMessages.NO_ACCESS);
                const foundReviews = await Review_1.Review.findAndCountAll({
                    include: {
                        model: User_1.User,
                        as: "users",
                        attributes: ["name", "patronim", "surname", "email", "phone"]
                    },
                    order: [
                        ["createdAt", "DESC"]
                    ],
                    limit: parseInt(limit),
                    offset: parseInt(offset)
                });
                return {
                    status: http_status_codes_1.StatusCodes.OK,
                    result: foundReviews
                };
            }
            catch (err) {
                const error = err;
                const status = errorCodeMatcher_1.errorCodesMathcher[error.message] || http_status_codes_1.StatusCodes.INTERNAL_SERVER_ERROR;
                return {
                    status,
                    result: {
                        status: "error",
                        message: error.message
                    }
                };
            }
        };
        this.createReview = async (userId, review) => {
            try {
                const foundUser = await User_1.User.findByPk(userId);
                if (!foundUser)
                    throw new Error(EErrorMessages_1.EErrorMessages.NO_ACCESS);
                const newReview = await Review_1.Review.create({ ...review });
                return {
                    status: http_status_codes_1.StatusCodes.CREATED,
                    result: newReview
                };
            }
            catch (err) {
                const error = err;
                const status = errorCodeMatcher_1.errorCodesMathcher[error.message] || http_status_codes_1.StatusCodes.BAD_REQUEST;
                return {
                    status,
                    result: {
                        status: "error",
                        message: error.message
                    }
                };
            }
        };
        this.deleteReview = async (userId, reviewId) => {
            try {
                const foundUser = await User_1.User.findByPk(userId);
                if (!foundUser || foundUser.isBlocked)
                    throw new Error(EErrorMessages_1.EErrorMessages.NO_ACCESS);
                const review = await Review_1.Review.findByPk(reviewId);
                if (!review)
                    throw new Error(EErrorMessages_1.EErrorMessages.NO_REVIEW_FOUND);
                if (foundUser.role === ERoles_1.ERoles.SUPERADMIN) {
                    await Review_1.Review.destroy({
                        where: { id: reviewId }
                    });
                    return {
                        status: http_status_codes_1.StatusCodes.OK,
                        result: { message: "Отзыв удален!" }
                    };
                }
                else {
                    throw new Error(EErrorMessages_1.EErrorMessages.NO_ACCESS);
                }
            }
            catch (err) {
                const error = err;
                const status = errorCodeMatcher_1.errorCodesMathcher[error.message] || http_status_codes_1.StatusCodes.INTERNAL_SERVER_ERROR;
                return {
                    status,
                    result: {
                        status: "error",
                        message: error.message
                    }
                };
            }
        };
    }
}
exports.ReviewsDb = ReviewsDb;
exports.reviewsDb = new ReviewsDb();
