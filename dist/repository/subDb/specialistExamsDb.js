"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.specialistExamsDb = exports.SpecialistExams = void 0;
const http_status_codes_1 = require("http-status-codes");
const errorCodeMatcher_1 = require("../../helpers/errorCodeMatcher");
const User_1 = require("../../models/User");
const EErrorMessages_1 = require("../../enums/EErrorMessages");
const Child_1 = require("../../models/Child");
const ERoles_1 = require("../../enums/ERoles");
const Doctor_1 = require("../../models/Doctor");
const Parent_1 = require("../../models/Parent");
const SpecialistExam_1 = require("../../models/SpecialistExam");
class SpecialistExams {
    constructor() {
        this.getSpecialistExamsByChildId = async (userId, childId) => {
            try {
                const foundUser = await User_1.User.findByPk(userId);
                if (!foundUser)
                    throw new Error(EErrorMessages_1.EErrorMessages.NO_ACCESS);
                const foundChild = await Child_1.Child.findByPk(childId);
                if (!foundChild)
                    throw new Error(EErrorMessages_1.EErrorMessages.CHILD_NOT_FOUND);
                if (foundUser.role === ERoles_1.ERoles.DOCTOR) {
                    const foundDoctor = await Doctor_1.Doctor.findOne({ where: { userId } });
                    const foundParent = await Parent_1.Parent.findOne({ where: { id: foundChild.parentId } });
                    if (!foundDoctor || !foundParent || foundDoctor.id !== foundParent.doctorId)
                        throw new Error(EErrorMessages_1.EErrorMessages.NO_ACCESS);
                }
                if (foundUser.role === ERoles_1.ERoles.PARENT) {
                    const foundParentByUser = await Parent_1.Parent.findOne({ where: { userId } });
                    if (!foundParentByUser || foundChild.parentId !== foundParentByUser.id)
                        throw new Error(EErrorMessages_1.EErrorMessages.NO_ACCESS);
                }
                const specialistExams = await SpecialistExam_1.SpecialistExam.findAll({
                    where: { childId }
                });
                return {
                    status: http_status_codes_1.StatusCodes.OK,
                    result: specialistExams
                };
            }
            catch (err) {
                const error = err;
                const status = errorCodeMatcher_1.errorCodesMathcher[error.message] || http_status_codes_1.StatusCodes.INTERNAL_SERVER_ERROR;
                return {
                    status,
                    result: {
                        status: "error",
                        message: error.message
                    }
                };
            }
        };
        this.createSpecialistExam = async (userId, exams) => {
            try {
                const foundUser = await User_1.User.findByPk(userId);
                if (!foundUser || foundUser.isBlocked)
                    throw new Error(EErrorMessages_1.EErrorMessages.NO_ACCESS);
                const foundChild = await Child_1.Child.findByPk(exams.childId);
                if (!foundChild)
                    throw new Error(EErrorMessages_1.EErrorMessages.CHILD_NOT_FOUND);
                if (foundUser.role === ERoles_1.ERoles.DOCTOR) {
                    const foundDoctor = await Doctor_1.Doctor.findOne({ where: { userId } });
                    const foundParent = await Parent_1.Parent.findOne({ where: { id: foundChild.parentId } });
                    if (!foundDoctor || !foundParent || foundDoctor.id !== foundParent.doctorId)
                        throw new Error(EErrorMessages_1.EErrorMessages.NO_ACCESS);
                }
                if (foundUser.role === ERoles_1.ERoles.PARENT) {
                    const foundParentByUser = await Parent_1.Parent.findOne({ where: { userId } });
                    if (!foundParentByUser || foundChild.parentId !== foundParentByUser.id)
                        throw new Error(EErrorMessages_1.EErrorMessages.NO_ACCESS);
                }
                const newExams = await SpecialistExam_1.SpecialistExam.create({ ...exams });
                return {
                    status: http_status_codes_1.StatusCodes.OK,
                    result: newExams
                };
            }
            catch (err) {
                const error = err;
                const status = errorCodeMatcher_1.errorCodesMathcher[error.message] || http_status_codes_1.StatusCodes.BAD_REQUEST;
                return {
                    status,
                    result: {
                        status: "error",
                        message: error.message
                    }
                };
            }
        };
        this.deleteSpecialistExam = async (userId, examinationId) => {
            try {
                const foundUser = await User_1.User.findByPk(userId);
                if (!foundUser || foundUser.isBlocked)
                    throw new Error(EErrorMessages_1.EErrorMessages.NO_ACCESS);
                const foundExamination = await SpecialistExam_1.SpecialistExam.findByPk(examinationId);
                if (!foundExamination)
                    throw new Error(EErrorMessages_1.EErrorMessages.SPECIALIST_EXAM_NOT_FOUND);
                const foundChild = await Child_1.Child.findOne({ where: { id: foundExamination.childId } });
                if (!foundChild)
                    throw new Error(EErrorMessages_1.EErrorMessages.CHILD_NOT_FOUND);
                if (foundUser.role === ERoles_1.ERoles.DOCTOR) {
                    const foundDoctor = await Doctor_1.Doctor.findOne({ where: { userId } });
                    const foundParent = await Parent_1.Parent.findOne({ where: { id: foundChild.parentId } });
                    if (!foundDoctor || !foundParent || foundDoctor.id !== foundParent.doctorId)
                        throw new Error(EErrorMessages_1.EErrorMessages.NO_ACCESS);
                }
                if (foundUser.role === ERoles_1.ERoles.PARENT) {
                    const foundParentByUser = await Parent_1.Parent.findOne({ where: { userId } });
                    if (!foundParentByUser || foundChild.parentId !== foundParentByUser.id)
                        throw new Error(EErrorMessages_1.EErrorMessages.NO_ACCESS);
                }
                await SpecialistExam_1.SpecialistExam.destroy({ where: { id: examinationId } });
                return {
                    status: http_status_codes_1.StatusCodes.OK,
                    result: { message: "Запись об осмотре врача удалена!" }
                };
            }
            catch (err) {
                const error = err;
                const status = errorCodeMatcher_1.errorCodesMathcher[error.message] || http_status_codes_1.StatusCodes.INTERNAL_SERVER_ERROR;
                return {
                    status,
                    result: {
                        status: "error",
                        message: error.message
                    }
                };
            }
        };
    }
}
exports.SpecialistExams = SpecialistExams;
exports.specialistExamsDb = new SpecialistExams();
