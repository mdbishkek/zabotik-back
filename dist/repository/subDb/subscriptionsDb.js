"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.subscriptionsDb = exports.SubscriptionsDb = void 0;
const errorCodeMatcher_1 = require("../../helpers/errorCodeMatcher");
const http_status_codes_1 = require("http-status-codes");
const User_1 = require("../../models/User");
const EErrorMessages_1 = require("../../enums/EErrorMessages");
const Parent_1 = require("../../models/Parent");
const Subscription_1 = require("../../models/Subscription");
const Doctor_1 = require("../../models/Doctor");
const ERoles_1 = require("../../enums/ERoles");
const EPaymentType_1 = require("../../enums/EPaymentType");
const calcSumAndDate_1 = require("../../helpers/calcSumAndDate");
const postgresDb_1 = require("../postgresDb");
class SubscriptionsDb {
    constructor() {
        this.renewSubscription = async (userId, subscriptionDto) => {
            const transaction = await postgresDb_1.postgresDB.getSequelize().transaction();
            try {
                const foundUser = await User_1.User.findByPk(userId);
                if (!foundUser || foundUser.isBlocked)
                    throw new Error(EErrorMessages_1.EErrorMessages.NO_ACCESS);
                const foundParent = await Parent_1.Parent.findOne({ where: { userId: subscriptionDto.userId } });
                if (!foundParent)
                    throw new Error(EErrorMessages_1.EErrorMessages.PARENT_NOT_FOUND);
                if (foundUser.role === ERoles_1.ERoles.PARENT && foundUser.id !== subscriptionDto.userId)
                    throw new Error(EErrorMessages_1.EErrorMessages.NO_ACCESS);
                const foundDoctor = await Doctor_1.Doctor.findByPk(foundParent.doctorId);
                if (!foundDoctor)
                    throw new Error(EErrorMessages_1.EErrorMessages.DOCTOR_NOT_FOUND);
                if (foundUser.role === ERoles_1.ERoles.DOCTOR) {
                    const foundDoctorByUser = await Doctor_1.Doctor.findOne({ where: { userId: subscriptionDto.payedBy } });
                    if (!foundDoctorByUser || foundParent.doctorId !== foundDoctorByUser.id)
                        throw new Error(EErrorMessages_1.EErrorMessages.NO_ACCESS);
                }
                if (typeof subscriptionDto.type !== "string")
                    throw new Error(EErrorMessages_1.EErrorMessages.WRONG_SUB_TYPE);
                const dataToAdd = (0, calcSumAndDate_1.calcSumAndDate)(subscriptionDto.type, foundParent.subscriptionEndDate, foundDoctor.price);
                await Subscription_1.Subscription.create({
                    userId: subscriptionDto.userId,
                    payedBy: subscriptionDto.paymentType === EPaymentType_1.EPaymentType.CASH ? foundDoctor.userId : subscriptionDto.userId,
                    type: subscriptionDto.type,
                    paymentType: subscriptionDto.paymentType,
                    endDate: dataToAdd?.newSubDate,
                    sum: dataToAdd?.sum
                }, { transaction });
                await Parent_1.Parent.update({ subscriptionEndDate: dataToAdd?.newSubDate }, { where: { id: foundParent.id }, transaction });
                await transaction.commit();
                return {
                    status: http_status_codes_1.StatusCodes.OK, result: { message: "Subscription is renewed" }
                };
            }
            catch (err) {
                const error = err;
                await transaction.rollback();
                const status = errorCodeMatcher_1.errorCodesMathcher[error.message] || http_status_codes_1.StatusCodes.BAD_REQUEST;
                return {
                    status, result: {
                        status: "error", message: error.message
                    }
                };
            }
        };
    }
}
exports.SubscriptionsDb = SubscriptionsDb;
exports.subscriptionsDb = new SubscriptionsDb();
