"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.usersDb = exports.UsersDb = void 0;
const http_status_codes_1 = require("http-status-codes");
const User_1 = require("../../models/User");
const ERoles_1 = require("../../enums/ERoles");
const Parent_1 = require("../../models/Parent");
const shortid_1 = __importDefault(require("shortid"));
const generateHash_1 = require("../../helpers/generateHash");
const jsonwebtoken_1 = __importDefault(require("jsonwebtoken"));
const mailer_1 = __importDefault(require("../../lib/mailer"));
const Doctor_1 = require("../../models/Doctor");
const generateJWT_1 = require("../../helpers/generateJWT");
const checkPassword_1 = require("../../helpers/checkPassword");
const passwordValidation_1 = require("../../helpers/passwordValidation");
const EErrorMessages_1 = require("../../enums/EErrorMessages");
const errorCodeMatcher_1 = require("../../helpers/errorCodeMatcher");
const sequelize_1 = require("sequelize");
const Child_1 = require("../../models/Child");
const NewbornData_1 = require("../../models/NewbornData");
const Subscription_1 = require("../../models/Subscription");
const EPaymentType_1 = require("../../enums/EPaymentType");
const postgresDb_1 = require("../postgresDb");
class UsersDb {
    constructor() {
        this.getUsers = async (userId, offset, limit, filter) => {
            try {
                const foundUser = await User_1.User.findByPk(userId);
                if (!foundUser || foundUser.isBlocked)
                    throw new Error(EErrorMessages_1.EErrorMessages.NO_ACCESS);
                let foundUsers = {
                    rows: [],
                    count: 0
                };
                if (filter && filter === "admins") {
                    foundUsers = await User_1.User.findAndCountAll({
                        where: {
                            role: {
                                [sequelize_1.Op.or]: [ERoles_1.ERoles.ADMIN, ERoles_1.ERoles.SUPERADMIN]
                            }
                        },
                        order: [
                            ["surname", "ASC"],
                            ["name", "ASC"]
                        ],
                        limit: parseInt(limit),
                        offset: parseInt(offset)
                    });
                }
                else {
                    foundUsers = await User_1.User.findAndCountAll({
                        order: [
                            ["surname", "ASC"],
                            ["name", "ASC"]
                        ],
                        limit: parseInt(limit),
                        offset: parseInt(offset)
                    });
                }
                return {
                    status: http_status_codes_1.StatusCodes.OK,
                    result: foundUsers
                };
            }
            catch (err) {
                const error = err;
                const status = errorCodeMatcher_1.errorCodesMathcher[error.message] || http_status_codes_1.StatusCodes.BAD_REQUEST;
                return {
                    status,
                    result: {
                        status: "error",
                        message: error.message
                    }
                };
            }
        };
        this.register = async (userId, userDto) => {
            const transaction = await postgresDb_1.postgresDB.getSequelize().transaction();
            try {
                const foundUser = await User_1.User.findByPk(userId);
                if (!foundUser || foundUser.isBlocked)
                    throw new Error(EErrorMessages_1.EErrorMessages.NO_ACCESS);
                const userExists = await User_1.User.findOne({
                    where: {
                        email: userDto.email
                    },
                    transaction
                });
                if (userExists)
                    throw new Error(EErrorMessages_1.EErrorMessages.USER_ALREADY_EXISTS);
                const emailRegex = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
                const email = { email: userDto.email };
                if (!emailRegex.test(userDto.email)) {
                    throw new Error(EErrorMessages_1.EErrorMessages.WRONG_MAIL_FORMAT);
                }
                const primaryPassword = shortid_1.default.generate();
                const user = await User_1.User.create({ ...userDto, password: primaryPassword }, { transaction });
                const token = jsonwebtoken_1.default.sign(email, `${process.env.MAIL_KEY}`, { expiresIn: "24h" });
                const url = `${process.env.REG_LINK}?token=${token}`;
                await (0, mailer_1.default)({ link: url, recipient: email.email, theme: "Регистрация" });
                if (user.role === ERoles_1.ERoles.DOCTOR) {
                    const newDoctor = {
                        userId: user.id,
                        speciality: "-",
                        experience: 0,
                        placeOfWork: "-",
                        photo: "default-photo.svg",
                        price: userDto.price ? userDto.price : 5000
                    };
                    await Doctor_1.Doctor.create({ ...newDoctor }, { transaction });
                }
                await transaction.commit();
                return {
                    status: http_status_codes_1.StatusCodes.CREATED,
                    result: user
                };
            }
            catch (err) {
                const error = err;
                await transaction.rollback();
                return {
                    status: http_status_codes_1.StatusCodes.BAD_REQUEST,
                    result: {
                        status: "error",
                        message: error.message
                    }
                };
            }
        };
        this.registerParent = async (userDto, userId) => {
            const transaction = await postgresDb_1.postgresDB.getSequelize().transaction();
            try {
                if (userDto.role !== ERoles_1.ERoles.PARENT)
                    throw new Error(EErrorMessages_1.EErrorMessages.NO_ACCESS);
                const foundUser = await User_1.User.findByPk(userId, { transaction });
                if (!foundUser || foundUser.isBlocked)
                    throw new Error(EErrorMessages_1.EErrorMessages.NO_ACCESS);
                const doctor = await Doctor_1.Doctor.findOne({
                    where: { userId },
                    transaction
                });
                if (doctor && foundUser.role === ERoles_1.ERoles.DOCTOR && doctor.id !== userDto.doctorId)
                    throw new Error(EErrorMessages_1.EErrorMessages.NO_ACCESS);
                const foundDoctor = await Doctor_1.Doctor.findByPk(userDto.doctorId, { transaction });
                if (!foundDoctor)
                    throw new Error(EErrorMessages_1.EErrorMessages.DOCTOR_NOT_FOUND);
                const userExists = await User_1.User.findOne({
                    where: {
                        email: userDto.email
                    },
                    transaction
                });
                if (userExists)
                    throw new Error(EErrorMessages_1.EErrorMessages.USER_ALREADY_EXISTS);
                const emailRegex = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
                const email = { email: userDto.email };
                if (!emailRegex.test(userDto.email))
                    throw new Error(EErrorMessages_1.EErrorMessages.WRONG_MAIL_FORMAT);
                const primaryPassword = shortid_1.default.generate();
                const user = await User_1.User.create({ ...userDto, password: await (0, generateHash_1.generateHash)(primaryPassword) }, { transaction });
                const token = jsonwebtoken_1.default.sign(email, `${process.env.MAIL_KEY}`, { expiresIn: "24h" });
                const url = `${process.env.REG_LINK}?token=${token}`;
                await (0, mailer_1.default)({ link: url, recipient: email.email, theme: "Регистрация" });
                const now = new Date();
                const newParent = {
                    userId: user.id,
                    doctorId: userDto.doctorId,
                    subscriptionEndDate: new Date(now.setMonth(now.getMonth() + userDto.subscrType))
                };
                const parentUser = await Parent_1.Parent.create({ ...newParent }, { transaction });
                let sum;
                switch (userDto.subscrType) {
                    case 1:
                        sum = foundDoctor.price;
                        break;
                    case 6:
                        sum = Math.floor((foundDoctor.price * 6 - (foundDoctor.price * 6) * 15 / 100) / 1000) * 1000;
                        break;
                    case 12:
                        sum = Math.floor((foundDoctor.price * 12 - (foundDoctor.price * 12) * 35 / 100) / 1000) * 1000;
                        break;
                }
                await Subscription_1.Subscription.create({
                    userId: user.id,
                    payedBy: userDto.paymentType === EPaymentType_1.EPaymentType.CASH ? foundDoctor.userId : parentUser.userId,
                    type: userDto.subscrType,
                    paymentType: userDto.paymentType,
                    endDate: new Date(now.setMonth(now.getMonth() + userDto.subscrType)),
                    sum: sum
                }, { transaction });
                const child = {
                    parentId: parentUser.id,
                    photo: "default-child-photo.svg",
                    name: userDto.child.name,
                    surname: userDto.child.surname,
                    patronim: userDto.child.patronim ? userDto.child.patronim : "",
                    dateOfBirth: userDto.child.dateOfBirth,
                    sex: userDto.child.sex,
                    height: userDto.child.height,
                    weight: userDto.child.weight
                };
                const createdChild = await Child_1.Child.create({ ...child }, { transaction });
                const newbornData = {
                    childId: createdChild.id
                };
                await NewbornData_1.NewbornData.create({ ...newbornData }, { transaction });
                delete user.dataValues.password;
                await transaction.commit();
                return {
                    status: http_status_codes_1.StatusCodes.CREATED,
                    result: user
                };
            }
            catch (err) {
                const error = err;
                await transaction.rollback();
                return {
                    status: http_status_codes_1.StatusCodes.BAD_REQUEST,
                    result: {
                        status: "error",
                        message: error.message
                    }
                };
            }
        };
        this.login = async (userDto) => {
            try {
                const foundUser = await User_1.User.findOne({ where: { email: userDto.email } });
                if (!foundUser)
                    throw new Error(EErrorMessages_1.EErrorMessages.WRONG_PASS_OR_EMAIL);
                const isMatch = await (0, checkPassword_1.checkPassword)(userDto.password, foundUser);
                if (!isMatch)
                    throw new Error(EErrorMessages_1.EErrorMessages.WRONG_PASS_OR_EMAIL);
                const user = foundUser.dataValues;
                delete user.password;
                const userWithToken = { ...user, token: (0, generateJWT_1.generateJWT)({ id: user.id, role: user.role, email: user.email, phone: user.phone, name: user.name, surname: user.surname, patronim: user.patronim, isBlocked: user.isBlocked }) };
                return {
                    status: http_status_codes_1.StatusCodes.OK,
                    result: userWithToken,
                };
            }
            catch (err) {
                const error = err;
                const status = errorCodeMatcher_1.errorCodesMathcher[error.message] || http_status_codes_1.StatusCodes.BAD_REQUEST;
                return {
                    status,
                    result: {
                        status: "error",
                        message: error.message
                    }
                };
            }
        };
        this.editUser = async (editorId, userId, userDto) => {
            try {
                const foundUser = await User_1.User.findByPk(editorId);
                const editingUser = await User_1.User.findByPk(userId);
                if (!editingUser)
                    throw new Error(EErrorMessages_1.EErrorMessages.USER_NOT_FOUND);
                if (!foundUser || foundUser.isBlocked)
                    throw new Error(EErrorMessages_1.EErrorMessages.NO_ACCESS);
                if (foundUser.role !== ERoles_1.ERoles.SUPERADMIN &&
                    foundUser.role !== ERoles_1.ERoles.ADMIN && foundUser.id !== userId)
                    throw new Error(EErrorMessages_1.EErrorMessages.NO_ACCESS);
                if (foundUser.role === ERoles_1.ERoles.ADMIN && editingUser.role === ERoles_1.ERoles.ADMIN && foundUser.id !== editingUser.id)
                    throw new Error(EErrorMessages_1.EErrorMessages.NO_ACCESS);
                if (foundUser.role === ERoles_1.ERoles.ADMIN && editingUser.role === ERoles_1.ERoles.SUPERADMIN)
                    throw new Error(EErrorMessages_1.EErrorMessages.NO_ACCESS);
                const fieldsToExclude = ["id", "password", "email", "role", "isBlocked"];
                const myFields = Object.keys(userDto).filter(field => !fieldsToExclude.includes(field));
                const user = await User_1.User.update(userDto, { where: { id: userId }, fields: myFields, returning: true }).then((result) => {
                    return result[1][0];
                });
                const updatedUser = user.dataValues;
                delete updatedUser.password;
                const userWithToken = { ...updatedUser, token: (0, generateJWT_1.generateJWT)({ id: user.id, role: user.role, email: user.email, phone: user.phone, name: user.name, surname: user.surname, patronim: user.patronim, isBlocked: user.isBlocked }) };
                return {
                    status: http_status_codes_1.StatusCodes.OK,
                    result: userWithToken
                };
            }
            catch (err) {
                const error = err;
                return {
                    status: http_status_codes_1.StatusCodes.BAD_REQUEST,
                    result: {
                        status: "error",
                        message: error.message
                    }
                };
            }
        };
        this.setPassword = async (data) => {
            try {
                const dataFromToken = jsonwebtoken_1.default.verify(data.token, `${process.env.MAIL_KEY}`);
                if (!dataFromToken)
                    throw new Error(http_status_codes_1.ReasonPhrases.UNAUTHORIZED);
                (0, passwordValidation_1.passwordValidation)(data.password);
                const foundUser = await User_1.User.findOne({ where: { email: dataFromToken.email } });
                const newPassword = await (0, generateHash_1.generateHash)(data.password);
                await User_1.User.update({ password: newPassword }, { where: { id: foundUser?.dataValues.id }, returning: true });
                return {
                    status: http_status_codes_1.StatusCodes.OK,
                    result: { message: "Пароль изменён." }
                };
            }
            catch (err) {
                const error = err;
                return {
                    status: http_status_codes_1.StatusCodes.BAD_REQUEST,
                    result: { message: error.message }
                };
            }
        };
        this.blockUser = async (adminId, userId) => {
            try {
                const foundAdmin = await User_1.User.findByPk(adminId);
                if (!foundAdmin || foundAdmin.isBlocked)
                    throw new Error(EErrorMessages_1.EErrorMessages.NO_ACCESS);
                const foundUser = await User_1.User.findByPk(userId);
                if (!foundUser)
                    throw new Error(EErrorMessages_1.EErrorMessages.USER_NOT_FOUND_BY_ID);
                if (foundUser.role === ERoles_1.ERoles.SUPERADMIN)
                    throw new Error(EErrorMessages_1.EErrorMessages.SUPERADMIN_CANT_BE_BLOCKED);
                const updatedUser = await User_1.User.update({ isBlocked: foundUser.isBlocked ? false : true }, {
                    where: { id: foundUser.id },
                    returning: true
                }).then((result) => { return result[1][0]; });
                return {
                    status: http_status_codes_1.StatusCodes.OK,
                    result: updatedUser
                };
            }
            catch (err) {
                const error = err;
                const status = errorCodeMatcher_1.errorCodesMathcher[error.message] || http_status_codes_1.StatusCodes.BAD_REQUEST;
                return {
                    status,
                    result: {
                        status: "error",
                        message: error.message
                    }
                };
            }
        };
        this.checkToken = async (id) => {
            try {
                const foundUser = await User_1.User.findByPk(id);
                if (!foundUser)
                    throw new Error(EErrorMessages_1.EErrorMessages.NO_ACCESS);
                const returningUser = foundUser.dataValues;
                delete returningUser.password;
                const userWithToken = { ...returningUser, token: (0, generateJWT_1.generateJWT)({ id: foundUser.id, role: foundUser.role, email: foundUser.email, phone: foundUser.phone, name: foundUser.name, surname: foundUser.surname, patronim: foundUser.patronim, isBlocked: foundUser.isBlocked }) };
                return {
                    status: http_status_codes_1.StatusCodes.OK,
                    result: userWithToken
                };
            }
            catch (err) {
                const error = err;
                return {
                    status: http_status_codes_1.StatusCodes.UNAUTHORIZED,
                    result: {
                        status: "error",
                        message: error.message
                    }
                };
            }
        };
    }
}
exports.UsersDb = UsersDb;
exports.usersDb = new UsersDb();
