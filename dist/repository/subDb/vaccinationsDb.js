"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.vaccinationsDb = exports.VaccinationsDb = void 0;
const http_status_codes_1 = require("http-status-codes");
const ERoles_1 = require("../../enums/ERoles");
const Doctor_1 = require("../../models/Doctor");
const User_1 = require("../../models/User");
const errorCodeMatcher_1 = require("../../helpers/errorCodeMatcher");
const EErrorMessages_1 = require("../../enums/EErrorMessages");
const Child_1 = require("../../models/Child");
const Parent_1 = require("../../models/Parent");
const Vaccination_1 = require("../../models/Vaccination");
class VaccinationsDb {
    constructor() {
        this.getVaccinations = async (userId, childId) => {
            try {
                const foundUser = await User_1.User.findByPk(userId);
                if (!foundUser)
                    throw new Error(EErrorMessages_1.EErrorMessages.NO_ACCESS);
                const foundChild = await Child_1.Child.findByPk(childId);
                if (!foundChild)
                    throw new Error(EErrorMessages_1.EErrorMessages.CHILD_NOT_FOUND);
                if (foundUser.role === ERoles_1.ERoles.DOCTOR) {
                    const foundDoctor = await Doctor_1.Doctor.findOne({ where: { userId } });
                    const foundParent = await Parent_1.Parent.findOne({ where: { id: foundChild.parentId } });
                    if (!foundDoctor || !foundParent || foundDoctor.id !== foundParent.doctorId)
                        throw new Error(EErrorMessages_1.EErrorMessages.NO_ACCESS);
                }
                if (foundUser.role === ERoles_1.ERoles.PARENT) {
                    const foundParentByUser = await Parent_1.Parent.findOne({ where: { userId } });
                    if (!foundParentByUser || foundChild.parentId !== foundParentByUser.id)
                        throw new Error(EErrorMessages_1.EErrorMessages.NO_ACCESS);
                }
                const vaccinations = await Vaccination_1.Vaccination.findAll({
                    where: { childId }
                });
                return {
                    status: http_status_codes_1.StatusCodes.OK,
                    result: vaccinations
                };
            }
            catch (err) {
                const error = err;
                const status = errorCodeMatcher_1.errorCodesMathcher[error.message] || http_status_codes_1.StatusCodes.INTERNAL_SERVER_ERROR;
                return {
                    status,
                    result: {
                        status: "error",
                        message: error.message
                    }
                };
            }
        };
        this.createVaccination = async (userId, vacDto) => {
            try {
                const foundUser = await User_1.User.findByPk(userId);
                if (!foundUser || foundUser.isBlocked)
                    throw new Error(EErrorMessages_1.EErrorMessages.NO_ACCESS);
                const foundChild = await Child_1.Child.findByPk(vacDto.childId);
                if (!foundChild)
                    throw new Error(EErrorMessages_1.EErrorMessages.CHILD_NOT_FOUND);
                if (foundUser.role === ERoles_1.ERoles.DOCTOR) {
                    const foundDoctor = await Doctor_1.Doctor.findOne({ where: { userId } });
                    const foundParent = await Parent_1.Parent.findOne({ where: { id: foundChild.parentId } });
                    if (!foundDoctor || !foundParent || foundDoctor.id !== foundParent.doctorId)
                        throw new Error(EErrorMessages_1.EErrorMessages.NO_ACCESS);
                }
                if (foundUser.role === ERoles_1.ERoles.PARENT) {
                    const foundParentByUser = await Parent_1.Parent.findOne({ where: { userId } });
                    if (!foundParentByUser || foundChild.parentId !== foundParentByUser.id)
                        throw new Error(EErrorMessages_1.EErrorMessages.NO_ACCESS);
                }
                const newVaccination = await Vaccination_1.Vaccination.create({ ...vacDto });
                return {
                    status: http_status_codes_1.StatusCodes.CREATED,
                    result: newVaccination
                };
            }
            catch (err) {
                const error = err;
                const status = errorCodeMatcher_1.errorCodesMathcher[error.message] || http_status_codes_1.StatusCodes.BAD_REQUEST;
                return {
                    status,
                    result: {
                        status: "error",
                        message: error.message
                    }
                };
            }
        };
        this.deleteVaccination = async (userId, vacId) => {
            try {
                const foundUser = await User_1.User.findByPk(userId);
                if (!foundUser || foundUser.isBlocked)
                    throw new Error(EErrorMessages_1.EErrorMessages.NO_ACCESS);
                const vaccination = await Vaccination_1.Vaccination.findByPk(vacId);
                if (!vaccination)
                    throw new Error(EErrorMessages_1.EErrorMessages.VACCINATION_NOT_FOUND);
                const foundChild = await Child_1.Child.findOne({ where: { id: vaccination.childId } });
                if (!foundChild)
                    throw new Error(EErrorMessages_1.EErrorMessages.CHILD_NOT_FOUND);
                if (foundUser.role === ERoles_1.ERoles.DOCTOR) {
                    const foundDoctor = await Doctor_1.Doctor.findOne({ where: { userId } });
                    const foundParent = await Parent_1.Parent.findOne({ where: { id: foundChild.parentId } });
                    if (!foundDoctor || !foundParent || foundDoctor.id !== foundParent.doctorId)
                        throw new Error(EErrorMessages_1.EErrorMessages.NO_ACCESS);
                }
                if (foundUser.role === ERoles_1.ERoles.PARENT) {
                    const foundParentByUser = await Parent_1.Parent.findOne({ where: { userId } });
                    if (!foundParentByUser || foundChild.parentId !== foundParentByUser.id)
                        throw new Error(EErrorMessages_1.EErrorMessages.NO_ACCESS);
                }
                await Vaccination_1.Vaccination.destroy({ where: { id: vacId } });
                return {
                    status: http_status_codes_1.StatusCodes.OK,
                    result: { message: "Запись о вакцинации удалена!" }
                };
            }
            catch (err) {
                const error = err;
                const status = errorCodeMatcher_1.errorCodesMathcher[error.message] || http_status_codes_1.StatusCodes.INTERNAL_SERVER_ERROR;
                return {
                    status,
                    result: {
                        status: "error",
                        message: error.message
                    }
                };
            }
        };
    }
}
exports.VaccinationsDb = VaccinationsDb;
exports.vaccinationsDb = new VaccinationsDb();
