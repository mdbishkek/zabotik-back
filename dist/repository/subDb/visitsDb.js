"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.visitDb = exports.VisitsDb = void 0;
const http_status_codes_1 = require("http-status-codes");
const Doctor_1 = require("../../models/Doctor");
const User_1 = require("../../models/User");
const errorCodeMatcher_1 = require("../../helpers/errorCodeMatcher");
const EErrorMessages_1 = require("../../enums/EErrorMessages");
const Visit_1 = require("../../models/Visit");
const Child_1 = require("../../models/Child");
const Parent_1 = require("../../models/Parent");
const ERoles_1 = require("../../enums/ERoles");
class VisitsDb {
    constructor() {
        this.getVisitsByChildId = async (userId, childId) => {
            try {
                const foundUser = await User_1.User.findByPk(userId);
                if (!foundUser)
                    throw new Error(EErrorMessages_1.EErrorMessages.NO_ACCESS);
                const foundChild = await Child_1.Child.findByPk(childId);
                if (!foundChild)
                    throw new Error(EErrorMessages_1.EErrorMessages.CHILD_NOT_FOUND);
                const foundParent = await Parent_1.Parent.findByPk(foundChild.parentId);
                if (foundUser.role === ERoles_1.ERoles.DOCTOR) {
                    const foundDoctor = await Doctor_1.Doctor.findOne({ where: { userId: foundUser.id } });
                    if (!foundDoctor)
                        throw new Error(EErrorMessages_1.EErrorMessages.NO_ACCESS);
                    if (!foundParent || foundDoctor.id !== foundParent.doctorId || foundUser.isBlocked) {
                        throw new Error(EErrorMessages_1.EErrorMessages.NO_ACCESS);
                    }
                }
                if (foundUser.role === ERoles_1.ERoles.PARENT) {
                    const foundParentByUserId = await Parent_1.Parent.findOne({ where: { userId: foundUser.id } });
                    if (!foundParentByUserId || foundParentByUserId.id !== foundChild.parentId) {
                        throw new Error(EErrorMessages_1.EErrorMessages.NO_ACCESS);
                    }
                }
                const visits = await Visit_1.Visit.findAll({
                    where: { childId: childId },
                    order: [["date", "ASC"]]
                });
                return {
                    status: http_status_codes_1.StatusCodes.OK,
                    result: visits
                };
            }
            catch (err) {
                const error = err;
                const status = errorCodeMatcher_1.errorCodesMathcher[error.message] || http_status_codes_1.StatusCodes.INTERNAL_SERVER_ERROR;
                return {
                    status,
                    result: {
                        status: "error",
                        message: error.message
                    }
                };
            }
        };
        this.createVisit = async (userId, visit) => {
            try {
                const foundUser = await User_1.User.findByPk(userId);
                if (!foundUser || foundUser.isBlocked)
                    throw new Error(EErrorMessages_1.EErrorMessages.NO_ACCESS);
                const foundDoctor = await Doctor_1.Doctor.findOne({ where: { userId: foundUser.id } });
                if (!foundDoctor)
                    throw new Error(EErrorMessages_1.EErrorMessages.NO_ACCESS);
                const foundChild = await Child_1.Child.findByPk(visit.childId);
                if (!foundChild)
                    throw new Error(EErrorMessages_1.EErrorMessages.CHILD_NOT_FOUND);
                const foundParent = await Parent_1.Parent.findByPk(foundChild.parentId);
                if (!foundParent)
                    throw new Error(EErrorMessages_1.EErrorMessages.NO_ACCESS);
                if (foundParent.doctorId !== foundDoctor.id)
                    throw new Error(EErrorMessages_1.EErrorMessages.NO_ACCESS);
                const newVisit = await Visit_1.Visit.create({ ...visit });
                return {
                    status: http_status_codes_1.StatusCodes.OK,
                    result: newVisit
                };
            }
            catch (err) {
                const error = err;
                const status = errorCodeMatcher_1.errorCodesMathcher[error.message] || http_status_codes_1.StatusCodes.BAD_REQUEST;
                return {
                    status,
                    result: {
                        status: "error",
                        message: error.message
                    }
                };
            }
        };
        this.deleteVisit = async (userId, visitId) => {
            try {
                const foundUser = await User_1.User.findByPk(userId);
                if (!foundUser || foundUser.isBlocked)
                    throw new Error(EErrorMessages_1.EErrorMessages.NO_ACCESS);
                const foundDoctor = await Doctor_1.Doctor.findOne({ where: { userId: foundUser.id } });
                if (!foundDoctor)
                    throw new Error(EErrorMessages_1.EErrorMessages.NO_ACCESS);
                const visit = await Visit_1.Visit.findByPk(visitId, { include: {
                        model: Child_1.Child,
                        as: "children",
                        attributes: ["parentId"]
                    } });
                if (!visit)
                    throw new Error(EErrorMessages_1.EErrorMessages.VISIT_NOT_FOUND);
                const foundParent = await Parent_1.Parent.findByPk(visit.children.parentId);
                if (!foundParent || foundParent.doctorId !== foundDoctor.id)
                    throw new Error(EErrorMessages_1.EErrorMessages.NO_ACCESS);
                await Visit_1.Visit.destroy({ where: { id: visitId } });
                return {
                    status: http_status_codes_1.StatusCodes.OK,
                    result: { message: "Посещение удалено!" }
                };
            }
            catch (err) {
                const error = err;
                const status = errorCodeMatcher_1.errorCodesMathcher[error.message] || http_status_codes_1.StatusCodes.INTERNAL_SERVER_ERROR;
                return {
                    status,
                    result: {
                        status: "error",
                        message: error.message
                    }
                };
            }
        };
    }
}
exports.VisitsDb = VisitsDb;
exports.visitDb = new VisitsDb();
