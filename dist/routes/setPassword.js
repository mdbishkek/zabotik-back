"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const morganMiddleware_1 = __importDefault(require("../config/morganMiddleware"));
const express_1 = __importDefault(require("express"));
const mailer_1 = __importDefault(require("../lib/mailer"));
const jsonwebtoken_1 = __importDefault(require("jsonwebtoken"));
const User_1 = require("../models/User");
const router = express_1.default.Router();
router.use(morganMiddleware_1.default);
router.post("/send-set-password-link", async (req, res) => {
    try {
        const emailRegex = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
        const email = { email: req.body.email };
        if (!emailRegex.test(email.email)) {
            throw new Error("Неправильный формат email-адреса");
        }
        const foundUser = await User_1.User.findOne({ where: { email: email.email } });
        if (!foundUser)
            throw new Error("Пользователь не найден!");
        const token = jsonwebtoken_1.default.sign(email, `${process.env.MAIL_KEY}`, { expiresIn: "24h" });
        const url = `${process.env.REG_LINK}?token=${token}`;
        await (0, mailer_1.default)({ link: url, recipient: email.email, theme: "Восстановление пароля" });
        res.status(200).send(email);
    }
    catch (err) {
        const error = err;
        const result = {
            status: "error",
            message: error.message
        };
        res.status(500).send(result);
    }
});
router.get("/send-set-password-link", (req, res) => {
    try {
        const token = req.query.token;
        jsonwebtoken_1.default.verify(token, `${process.env.MAIL_KEY}`);
        res.status(200).send("Доступ разрешен");
    }
    catch (err) {
        res.status(500).send(err);
    }
});
exports.default = router;
