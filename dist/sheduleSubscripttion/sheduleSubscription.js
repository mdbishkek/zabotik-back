"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    var desc = Object.getOwnPropertyDescriptor(m, k);
    if (!desc || ("get" in desc ? !m.__esModule : desc.writable || desc.configurable)) {
      desc = { enumerable: true, get: function() { return m[k]; } };
    }
    Object.defineProperty(o, k2, desc);
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
const schedule = __importStar(require("node-schedule"));
const Parent_1 = require("../models/Parent");
const rule = new schedule.RecurrenceRule();
rule.hour = 0;
rule.minute = 0;
rule.tz = "Etc/UTC";
const checkSubDate = async () => {
    try {
        const AllParent = await Parent_1.Parent.findAll({
            where: { isActive: true },
        });
        AllParent.forEach(async (parent) => {
            const date = new Date(Date.now());
            if (parent.subscriptionEndDate < date) {
                await Parent_1.Parent.update({ isActive: false }, {
                    where: { id: parent.id },
                    returning: true
                }).then((result) => {
                    return result[1][0];
                });
            }
        });
    }
    catch (error) {
        console.log(error);
    }
};
const job = schedule.scheduleJob(rule, function () {
    checkSubDate();
});
exports.default = job;
