import { ERoles } from "./enums/ERoles";
import { User } from "./models/User";
import Logger from "./lib/logger";
import uuid from "react-uuid";
import { PostgresDB } from "./repository/postgresDb";


const db = PostgresDB;
const date: Date = new Date();

export const createUserFixtures = async (): Promise<void> => {
    try {
        await User.create({
            id: uuid(),
            role: ERoles.ADMIN,
            email: "zabotik.help@yandex.ru",
            phone: "+7(707)177-48-74",
            name: "Алина",
            surname: "Ардамина",
            password: "$2b$08$oG.sWyvFWciU9P8gnjQz3.HhNt5V6v4yoLXaDlSO1a75hKtfXf61i",
            isBlocked: false,
        });
        Logger.info("Фикстуры созданы");
    } catch (error) {
        Logger.error(error);
    }
};

createUserFixtures();